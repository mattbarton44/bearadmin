<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GameRoster extends Model
{
    use SoftDeletes;
	protected $table = 'gamerosters';
    protected $dates = ['deleted_at'];

    public function game()
    {
        return $this->belongsTo('Game','game_id');
    }

    public function user()
    {
        return $this->belongsTo('User','user_id');
    }
}
