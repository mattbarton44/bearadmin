<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArticleComment extends Model
{
    use SoftDeletes;
	
	protected $table = 'articlecomments';
    protected $dates = ['deleted_at'];

	public function article()
    {
        return $this->belongsTo('App\Article','article_id');
    }

	public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
