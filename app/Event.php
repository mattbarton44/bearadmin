<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;
	protected $table = 'events';
    protected $dates = ['deleted_at'];

	public function game()
    {
        return $this->hasOne('App\Game','event_id');
    }

    public function eventgroups()
    {
        return $this->hasMany('App\EventGroup','event_id');
    }

    public function groups()
    {
        return $this->belongsToMany('App\Group', 'eventgroups', 'event_id', 'group_id');
    }

    public function training()
    {
        return $this->hasOne('App\Training', 'event_id');
    }
}
