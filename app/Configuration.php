<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Configuration extends Model
{
    use SoftDeletes;
	protected $table = 'configuration';
    protected $dates = ['deleted_at'];

	
}
