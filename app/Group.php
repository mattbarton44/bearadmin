<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
    use SoftDeletes;

    protected $table = 'groups';

    protected $dates = ['deleted_at'];
    protected $hidden = array('pivot');

	// relationships
	public function groupMembers()
    {
        return $this->hasMany('App\GroupMember','group_id');
    }

    public function articleGroups()
    {
        return $this->hasMany('App\ArticleGroup','group_id');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'groupmembers', 'group_id', 'user_id');
    }

    public function articles()
    {
        return $this->belongsToMany('App\Article', 'articlegroups', 'group_id', 'article_id');
    }
	
	// scopes
	public function scopeGetId($query,$name)
    {
        return $query->where('name','=',$name)->pluck('id');
    }
}
