<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends Model
{
    use SoftDeletes;
	protected $table = 'teams';
    protected $dates = ['deleted_at'];

	public function games()
    {
        return $this->hasMany('App\Game','team_id');
    }

    public function trainings()
    {
        return $this->hasMany('App\Training','team_id');
    }
    
	public function group()
    {
        return $this->belongsTo('App\Group','group_id');
    }
}
