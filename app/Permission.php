<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
	protected $table = 'permissions';

	public function rolePermissions()
    {
        return $this->hasMany('App\RolePermission','permission_id');
    }
}