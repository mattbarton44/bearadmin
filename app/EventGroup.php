<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventGroup extends Model
{    

    protected $table = 'eventgroups';

    protected $fillable = array('event_id', 'group_id');
    
    public function event()
    {
        return $this->belongsTo('App\Event','event_id');
    }

    public function group()
    {
        return $this->belongsTo('App\Group','group_id');
    }
}
