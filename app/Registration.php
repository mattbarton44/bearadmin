<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Registration extends Model
{	
    use SoftDeletes;
	protected $table = 'registrations';
    protected $dates = ['deleted_at'];

	public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
