<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolePermission extends Model
{
    protected $table = 'rolepermissions';
    protected $fillable = array('role_id', 'permission_id');

	public function permission()
    {
        return $this->belongsTo('App\Permission','permission_id');
    }
}
