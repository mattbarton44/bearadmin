<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{

    use SoftDeletes;
    
    protected $table = 'roles';
    protected $dates = ['deleted_at'];

	// relationships
	public function rolePermissions()
    {
        return $this->hasMany('App\RolePermission','role_id');
    }

    public function users()
    {
        return $this->hasMany('App\User','role_id');
    }   

    public function permissions()
    {
        return $this->belongsToMany('App\Permission', 'rolepermissions', 'role_id', 'permission_id');
    }
    
}