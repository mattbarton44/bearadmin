<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'role_id'
    ];

    protected $hidden = [
        'password', 'remember_token', 'pivot',
    ];

    // relationships
    public function passwordresets()
    {
        return $this->hasMany('App\PasswordReset','user_id');
    }

    public function groups()
    {
        return $this->belongsToMany('App\Group', 'groupmembers', 'user_id', 'group_id');
    }

    public function groupmembers()
    {
        return $this->hasMany('App\GroupMember','user_id');
    }

     public function articles()
    {
        return $this->hasMany('App\Article','user_id');
    }

    public function articlecomments()
    {
        return $this->hasMany('App\ArticleComment','user_id');
    }   

    public function registrations()
    {
        return $this->hasMany('App\Registration','user_id');
    }

    public function orders()
    {
        return $this->hasMany('App\Order','user_id');
    }

    public function jobs()
    {
        return $this->hasMany('App\GameJob', 'user_id');
    }

    public function role()
    {
        return $this->belongsTo('App\Role', 'role_id', 'id');
    }


    public function has($permissionName)
    {
        $role = $this->role()->with('rolePermissions.permission')->first();
        foreach ($role->rolePermissions as $rolepermission)
        {
            if ($rolepermission->permission->name == $permissionName)
            {
                return true;
            }
        }
        return false;
    }
}
