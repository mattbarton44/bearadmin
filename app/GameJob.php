<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GameJob extends Model
{
    use SoftDeletes;
	protected $table = 'gamejobs';
    protected $dates = ['deleted_at'];

    public function game()
    {
        return $this->belongsTo('App\Game','game_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
