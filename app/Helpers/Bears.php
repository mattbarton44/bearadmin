<?php

namespace App\Helpers;

use DB;
use Auth;
use Redirect;
use Route;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Stripe\{Charge, customer};
use App\Product;
use App\Order;
use App\User;
use domDocument;

class Bears
{
    public static function isActiveRoute($route, $output = 'active')
    {
        if (Route::getFacadeRoot()->current()->uri() == $route) {
            return $output;
        }
    }

    public static function processExposedProductOrder($data)
    {
    	$product = Product::find($data["product_id"]);

        if($product == null)
        {
        	return 'Error, invalid product';
        }

		$form = [];
		$product_form = json_decode($product->form);

		for($i=0; $i<sizeof($product_form); $i++)
		{
			$key = str_replace(' ', '_', $product_form[$i]->label);

			switch($product_form[$i]->type)
			{
				case "input":
				case "select":
					$form[$product_form[$i]->label] = $data[$key];
					break;
			}
		}

		if(Order::where('product_id', '=', $product->id)->count() >= 80)
		{
			return 'Error, no tickets left for this event';
		}

        $order = new Order;
        $order->form = json_encode($form);
        $order->user_id = User::where('email','=','info@sheffieldbears.com')->first()->id;
        $order->product_id = $product->id;
        $order->status = 'Awaiting Payment';
       	$order->save();

        $charge = Charge::create([
        	'source' => $data['stripeToken'],
        	'description' => $product->name,
        	'amount' => $product->price,
  			"receipt_email" => $data['stripeEmail'],
        	'currency' => 'gbp',
        ]);

        if($charge->outcome->type == 'authorized')
        {
        	$order->status = 'Complete';
        	$order->save();
        }
        else
        {
        	$order->status = 'Failed';
        	$order->save();
        	return 'Purchase Failed!';
        }
 

        return 'Purchase successful!';

    }

    public static function getBuihaCupTableData($div_id)
    {
        $data = file_get_contents("http://www.buiha.org.uk/ajax-cup-table-body.php?ajax='ajax'&div=".$div_id);
        $dom = new domDocument;
        @$dom->loadHTML($data);
        $dom->preserveWhiteSpace = false;
        $tables = $dom->getElementsByTagName('table');
        $rows = $tables->item(0)->getElementsByTagName('tr');
        $datas = [];

        $index = 0;
        $col_heads = ['#','Team','GP','W','D','L','Pts','GF','GA','GD','PP%','PK%'];
        foreach ($rows as $row)
        {
            if($index != 0)
            {
                $row_data = [];
                $cols = $row->getElementsByTagName('td');
                $col_index = 0;
                foreach ($cols as $col) 
                {
                    if($col_index == 0)
                    {
                        $row_data[$col_heads[$col_index]] = '' . $index;
                    }
                    else
                    {
                        $row_data[$col_heads[$col_index]] = $col->nodeValue;
                    }
                    $col_index = $col_index + 1;
                }
                $datas[] = $row_data;
            }
            $index = $index + 1;
        }
        return $datas;
    }

    public static function storeScheduleInfo()
    {
        // add in year detection from reg year setting
        $data = file_get_contents("https://www.buiha.org.uk/club-schedule.php?club=18&season=17-18");
        $dom = new domDocument;
        @$dom->loadHTML($data);
        $dom->preserveWhiteSpace = false;
        $body = $dom->getElementById('body_text');

        $rows = $body->getElementsByTagName('tr');
        $datas = [];
        foreach ($rows as $row)
        {
            $cols = $row->getElementsByTagName('td');
            if($cols[3] != null && $cols[4] != null && $cols[5] != null)
            {
                $row_data = [];
                $row_data['Date'] = preg_replace('!\s+!', ' ', rtrim(trim($cols[1]->nodeValue)));
                $row_data['Score'] = $cols[5]->nodeValue;
                if($cols[5]->nodeValue == "PROVISIONAL")
                {
                    $row_data['Score'] = $cols[5]->nodeValue;
                }
                else
                {
                    $row_data['Score'] = $cols[5]->nodeValue;
                }

                $details = preg_replace('!\s+!', ' ', rtrim(trim($cols[3]->nodeValue)));

                $row_data['Home'] = explode('A: ', explode('H: ', $details)[1])[0];
                $row_data['Away'] = explode('Location: ', explode('A: ', $details)[1])[0];

                $row_data['Location'] = explode('Face Off: ', explode('Location: ', $details)[1])[0];
                $row_data['Time'] = explode('Face Off: ', $details)[1];

                return $row_data;
                $datas[] = $row_data;
            }
        }
        return $datas;
    }
}