<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrainingAttendance extends Model
{
    use SoftDeletes;
	protected $table = 'trainingattendances';
    protected $dates = ['deleted_at'];

    public function training()
    {
        return $this->belongsTo('Training','training_id');
    }
}
