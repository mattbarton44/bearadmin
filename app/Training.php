<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Training extends Model
{
    use SoftDeletes;
	protected $table = 'trainings';
    protected $dates = ['deleted_at'];

    public function event()
    {
        return $this->belongsTo('Event','event_id');
    }

    public function team()
    {
        return $this->belongsTo('Team','team_id');
    }

    public function attendances()
    {
        return $this->hasMany('App\TrainingAttendance','training_id');
    }
}
