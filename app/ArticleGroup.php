<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleGroup extends Model
{    
    protected $table = 'articlegroups';

    protected $fillable = array('article_id', 'group_id');
    
	public function article()
    {
        return $this->belongsTo('Article','article_id');
    }

    public function group()
    {
        return $this->belongsTo('Group','group_id');
    }
}
