<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupMember extends Model
{
    protected $table = 'groupmembers';
    protected $fillable = array('group_id', 'user_id');

	public function group()
    {
        return $this->belongsTo('App\Group','group_id');
    }
	public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
