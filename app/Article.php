<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use SoftDeletes;
	protected $table = 'articles';
    protected $dates = ['deleted_at'];

	public function comments()
    {
        return $this->hasMany('App\ArticleComment','article_id');
    }

    public function articlegroups()
    {
        return $this->hasMany('App\ArticleGroup','article_id');
    }

    public function groups()
    {
        return $this->belongsToMany('App\Group', 'articlegroups', 'article_id', 'group_id');
    }

	public function type()
    {
        return $this->belongsTo('App\ArticleType','articleType_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
