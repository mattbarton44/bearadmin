<?php

namespace App\Http\Middleware;

use Closure;
use Redirect;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    { 
        if ($request->user()->has($permission) == false) 
        {
            if($request->ajax())
            {
                return response()->json( ['error'=>'Permission denied'], 401);
            }
            else
            {
                return Redirect::to('club/');
            }
        }

        return $next($request);
    }
}
