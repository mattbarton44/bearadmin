<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreArticle extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {
        return [
            'subject' => 'required|unique:articles,subject',
            'content' => 'required',
            'public' => 'required',
            'type_id' => 'required',
        ];
    }
}
