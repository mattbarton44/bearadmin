<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateGame extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {
        return [
            'id' => 'required',
            'team' => 'required',
            'homeTeam' => 'required',
            'awayTeam' => 'required',
        ];
    }
}