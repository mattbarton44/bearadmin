<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEvent extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {
        return [
            'name' => 'required',
            'description' => 'required',
            'start' => 'required',
            'end' => 'required',
            'type' => 'required',
        ];
    }
}