<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateArticle extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {
        return [
            'subject' => 'required',
            'content' => 'required',
            'public' => 'required',
            'type_id' => 'required',
        ];
    }
}