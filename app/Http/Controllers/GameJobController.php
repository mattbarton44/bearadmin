<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Game;
use App\GameJob;
use App\GameRoster;
use App\Team;
use App\Event;
use App\EventGroup;
use App\Helpers\Bears;
use DB;
use Auth;
use App\Http\Requests\StoreGameJob;
use App\Http\Requests\UpdateGameJob;

class GameJobController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('perm:gamejobs')->only('index');
        $this->middleware('perm:gamejobs_create')->only('create');
        $this->middleware('perm:gamejobs_show')->only('show');
        $this->middleware('perm:gamejobs_edit')->only('edit');
        $this->middleware('perm:gamejobs_create')->only('store');
        $this->middleware('perm:gamejobs_edit')->only('update');
        $this->middleware('perm:gamejobs_destroy')->only('destroy');
    }

    public function index()
    {
        return GameJob::with('game')->with('user')->get();
    }

    public function create()
    {
        return;
    }

    public function show($id)
    {
        return;
    }

    public function edit($id)
    {
        return;
    }

    public function store(StoreGameJob $request)
    {
        $gamejob = new GameJob;
        $gamejob->game_id = $request->game["id"];
        $gamejob->user_id = Auth::user()->id;
        $gamejob->role = $request->role;
        $gamejob->prevExp = $request->prevExp;
        $gamejob->attended = false;

        try
        {
            DB::transaction(function() use($gamejob)
            {
                $gamejob->save();
            });
        }
        catch (\Exception $e)
        {    
            return response()->json( ['error'=>$e], 500);
        }
        return response()->json( ['success'=>"Game Job created"], 200);
    }

    public function update(UpdateGameJob $request, $id)
    {
        return;
    }

    public function destroy($id)
    {        
        $job = GameJob::find($id);
        try
        {
            DB::transaction(function() use($job)
            {
                $job->delete();
            });
        }
        catch (\Exception $e)
        {                   
            return response()->json( ['error'=>'Database transaction failed'], 500);
        }
        return response()->json( ['success'=>"Job deleted"], 200);
    }
    


    public function my_jobs()
    {
        return GameJob::with('game.event')->where('user_id', '=', Auth::id())->get();
    }
}