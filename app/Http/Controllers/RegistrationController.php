<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Registration;
use App\Product;
use App\Group;
use App\GroupMember;
use App\Order;
use App\User;
use App\Configuration;
use App\Helpers\Bears;
use DB;
use Auth;

class RegistrationController extends Controller
{

    public function index()
    {
        return Registration::orderBy('season', 'ASC')->with('user')->get();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        return Registration::with('user')->find($id);
    }


    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $reg = Registration::find($id);
        $reg->status = $request->input('status');
        $reg->save();

        $group_id = Group::where('name','=','Registered Players')->pluck('id')->first();
        $user_id = $reg->user->id;

        $groupmember = GroupMember::where('group_id','=', $group_id)->where('user_id','=', $user_id)->first();

        if($reg->status == 'Complete' && $groupmember == null)
        {
            $groupmember = new GroupMember(array('user_id' => $user_id,'group_id' => $group_id));
        }

        try
        {
            DB::transaction(function() use($reg, $groupmember)
            {        
                $reg->save();

                if($reg->status == 'Complete' && $groupmember == null)
                {
                    $user->groupmembers()->save($groupmember);
                }
                else if($reg->status != 'Complete' && $groupmember != null)
                {
                    $groupmember->delete();
                }

            });
        }
        catch (\Exception $e)
        {                   
            return response()->json( ['error'=>$e->getMessage()], 500);
        }
        return response()->json( ['success'=>"Registration updated"], 200);
    }

    public function destroy($id)
    {
        //
    }

    public function my_registration()
    {
        // Pending / Rejected / Complete
        $config = Configuration::where('setting','=','reg_year')->first();
        $next_year = ((int)substr($config->value, -2)+1);
        $reg = Registration::where('user_id','=',Auth::id())->where('season','=',$config->value . "/" . $next_year)->first();
        if($reg != null)
        {
            return 'Your registration for the ' . $reg->season . ' season is ' . $reg->status;
        }
        else
        {
            return 'You have not submitted your registration for the current season';
        }
    }
}
