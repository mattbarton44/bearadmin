<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Group;
use App\GroupMember;
use App\Helpers\Bears;
use DB;
use Auth;
use App\Http\Requests\StoreUser;
use App\Http\Requests\UpdateUser;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('perm:users')->only('index');
        $this->middleware('perm:users_create')->only('create');
        $this->middleware('perm:users_show')->only('show');
        $this->middleware('perm:users_edit')->only('edit');
        $this->middleware('perm:users_create')->only('store');
        $this->middleware('perm:users_edit')->only('update');
        $this->middleware('perm:users_destroy')->only('destroy');
    }

    public function index()
    {
        return User::orderBy('last_name', 'ASC')->with('role')->with('groups')->get();
    }

    public function create()
    {
        // return data needed to create user
        return;
    }

    public function show($id)
    {
        // return data for requested user
        return;
    }

    public function edit($id)
    {
        return;
    }

    public function store(StoreUser $request)
    {
        $user = new User;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->role_id = $request->role_id;
        $user->avatarPath = '/images/avatars/default.jpg';

        try
        {
            DB::transaction(function() use($user)
            {
                $user->save();
            });
        }
        catch (\Exception $e)
        {                   
            return response()->json( ['error'=>$e->getMessage()], 500);
        }
        return response()->json( ['success'=>"User created"], 200);
    }

    public function update(UpdateUser $request, $id)
    {
        $user = User::find($id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->role_id = $request->role_id;

        $groups = [];
        for($i = 0; $i<sizeof($request->groups);$i++)
        {           
            $groups[] = new GroupMember(array('user_id' => $user->id,'group_id' => $request->groups[$i]["id"]));
        }

        try
        {
            DB::transaction(function() use($user,$groups)
            {
                $user->save();
                $user->groupmembers()->delete();
                if(!empty($groups))
                {
                    $user->groupmembers()->saveMany($groups);    
                }
            });
        }
        catch (\Exception $e)
        {                   
            return response()->json( ['error'=>$e->getMessage()], 500);
        }
        return response()->json( ['success'=>"User updated"], 200);
    }

    public function destroy(Request $request, $id)
    {
        $user = User::find($id);
        try
        {
            DB::transaction(function() use($user)
            {
                $user->delete();
            });
        }
        catch (\Exception $e)
        {                   
            return response()->json( ['error'=>$e->getMessage()], 500);
        }
        return response()->json( ['success'=>"User deleted"], 200);
    }


    public function auth_data()
    {
        return User::with('role.permissions')->get()->find(Auth::id());
    }

}
