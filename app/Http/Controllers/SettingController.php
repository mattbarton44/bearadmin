<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Socialite;
use Auth;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('perm:settings');
    }

    public function index() 
    {

    }

    public function edit($id)
    {
        return;
    }

    public function update(Request $request, $id)
    {
        return;
    }

    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallback(Request $request)
    {

        if (!$request->has('code') || $request->has('denied')) 
        {
            // cancelled or unsuccessful
            return redirect('/club/settings');
        }

        $fb_user = Socialite::driver('facebook')->user();

        $user = Auth::user();

        $user->facebook_id = $fb_user->id;
        $user->avatarPath = $fb_user->avatar;

        $user->save();

        return redirect('/club/settings');
    }

}


