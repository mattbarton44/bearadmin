<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\ArticleComment;
use App\Helpers\Bears;
use DB;
use Auth;
use App\Http\Requests\StoreComment;

class ArticleCommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('perm:articlecomments_create')->only('store');
        $this->middleware('perm:articlecomments_edit')->only('update');
        $this->middleware('perm:articlecomments_destroy')->only('destroy');
    }

    public function index() { return; }
    public function create() { return; }
    public function show($id) { return; }
    public function edit($id) { return; }

    public function store(StoreComment $request)
    {
        $comment = new ArticleComment;
        $comment->article_id = $request->article_id;
        $comment->user_id = Auth::id();
        $comment->comment = $request->comment;

        try
        {
            DB::transaction(function() use($comment)
            {
                $comment->save();
            });
        }
        catch (\Exception $e)
        {                   
            return response()->json( ['error'=>'Database transaction failed'], 500);
        }
        return response()->json( ['success'=>"Comment created"], 200);
    }

    public function update(Request $request, $id)
    {
        return;
    }

    public function destroy($id)
    {
        $comment = ArticleComment::find($id);
        try
        {
            DB::transaction(function() use($comment)
            {
                $comment->delete();
            });
        }
        catch (\Exception $e)
        {                   
            return response()->json( ['error'=>'Database transaction failed'], 500);
        }
        return response()->json( ['success'=>"Comment deleted"], 200);
    }
}