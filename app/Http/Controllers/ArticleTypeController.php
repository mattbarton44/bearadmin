<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\ArticleType;
use App\Article;
use App\Helpers\Bears;
use DB;

class ArticleTypeController extends Controller
{
	public function __construct()
    {
        $this->middleware('perm:articles')->only('index');
    }


    public function index()
    {
        return ArticleType::orderBy('name', 'ASC')->get();
    }

    public function create() { return; }
    public function show($id) { return; }
    public function edit($id) { return; }

    public function store(Request $request)
    {
        return;
    }

    public function update(Request $request, $id)
    {
        return;
    }

    public function destroy($id)
    {
        return;
    }
}
