<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Training;
use App\TrainingAttendance;
use App\Team;
use App\Event;
use App\EventGroup;
use App\Helpers\Bears;
use DB;
use Auth;

class TrainingController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('perm:trainings')->only('index');
        $this->middleware('perm:trainings_create')->only('create');
        $this->middleware('perm:trainings_show')->only('show');
        $this->middleware('perm:trainings_edit')->only('edit');
        $this->middleware('perm:trainings_create')->only('store');
        $this->middleware('perm:trainings_edit')->only('update');
        $this->middleware('perm:trainings_destroy')->only('destroy');
    }

    public function index()
    {
        return;
    }

    public function create()
    {
        return;
    }

    public function show($id)
    {
        return;
    }

    public function edit($id)
    {
        return;
    }

    public function store($request)
    {
        return;
    }

    public function update($request, $id)
    {
        return;
    }

    public function destroy($id)
    {
        return;
    }

}