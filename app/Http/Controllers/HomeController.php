<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Bears;
use App\Configuration;
use App\Article;

class HomeController extends Controller
{
    public function getBuihaCupTables()
    {
        $all_config = Configuration::all();
        $config = ['a'=>[],'b'=>[],'c'=>[],'d'=>[],'e'=>[]];

        for($i =0; $i<sizeof($all_config); $i++)
        {
            switch($all_config[$i]->setting)
            {
                case 'a_div_id':
                    $config['a']['id'] = $all_config[$i]->value;
                    break;
                case 'b_div_id':
                    $config['b']['id'] = $all_config[$i]->value;
                    break;
                case 'c_div_id':
                    $config['c']['id'] = $all_config[$i]->value;
                    break;
                case 'd_div_id':
                    $config['d']['id'] = $all_config[$i]->value;
                    break;
                case 'e_div_id':
                    $config['e']['id'] = $all_config[$i]->value;
                    break;
                case 'a_div_name':
                    $config['a']['name'] = $all_config[$i]->value;
                    break;
                case 'b_div_name':
                    $config['b']['name'] = $all_config[$i]->value;
                    break;
                case 'c_div_name':
                    $config['c']['name'] = $all_config[$i]->value;
                    break;
                case 'd_div_name':
                    $config['d']['name'] = $all_config[$i]->value;
                    break;
                case 'e_div_name':
                    $config['e']['name'] = $all_config[$i]->value;
                    break;
            }
        }

        $data = [];

        if($config['a']['id'] != '0')
        {
            $data[$config["a"]["name"]] = Bears::getBuihaCupTableData($config["a"]["id"]);
        }

        if($config['b']['id'] != '0')
        {
            $data[$config["b"]["name"]] = Bears::getBuihaCupTableData($config["b"]["id"]);
        }

        if($config['c']['id'] != '0')
        {
            $data[$config["c"]["name"]] = Bears::getBuihaCupTableData($config["c"]["id"]);
        }

        if($config['d']['id'] != '0')
        {
            $data[$config["d"]["name"]] = Bears::getBuihaCupTableData($config["d"]["id"]);
        }

        if($config['e']['id'] != '0')
        {
            $data[$config["e"]["name"]] = Bears::getBuihaCupTableData($config["e"]["id"]);
        }
        return $data;
    }

    public function getFeaturedNews()
    {
        $data = Article::with('user')->with('type')->find(Configuration::where('setting','=','featured_article_id')->pluck('value'))->first();
        return $data;
    }

    public function getLatestNews()
    {
        $data = Article::with('user')->with('type')->where('public','=',true)->where('id','!=',Configuration::where('setting','=','featured_article_id')->pluck('value'))->take(4)->get();
        return $data;
    }

    public function getAllNews()
    {
        $data = Article::with('user')->with('type')->where('public','=',true)->get();
        return $data;
    }

    public function getNewsArticle($id)
    {
        $data = Article::with('user')->with('type')->where('public','=',true)->find($id);


        return $data;
    }
}
