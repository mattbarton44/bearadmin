<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Helpers\Bears;
use App\Permission;

class PermissionController extends Controller
{
    public function __construct()
    {
        //$this->middleware('perm:permissions')->only('index');
    }

    public function index()
    {
        return Permission::orderBy('name', 'ASC')->get();
    }

}
