<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Training;
use App\Game;
use App\Event;
use App\EventGroup;
use App\Helpers\Bears;
use DB;
use Auth;
use App\Http\Requests\StoreEvent;
use App\Http\Requests\UpdateEvent;

class EventController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('perm:events')->only('index');
        $this->middleware('perm:events')->only('games');
        $this->middleware('perm:events_create')->only('create');
        $this->middleware('perm:events_show')->only('show');
        $this->middleware('perm:events_edit')->only('edit');
        $this->middleware('perm:events_create')->only('store');
        $this->middleware('perm:events_edit')->only('update');
        $this->middleware('perm:events_destroy')->only('destroy');
    }

    public function index()
    {
        if(Auth::user()->has('administrator'))
        {
            return Event::orderBy('start', 'ASC')->with('groups')->get();
        }
        else
        {
            $groups = Auth::user()->groups->pluck('id');
            $events = Event::orderBy('start', 'ASC')->with('groups')->get();

            $filtered = $events->reject(function ($event) use ($groups) {

                if(sizeof($event->groups) == 0)
                {
                    return false;
                }

                foreach($event->groups as $group)
                {
                    if(in_array($group->id, $groups->toArray()) == true)
                    {
                        return false;
                    }
                }
                return true;
            });
            return $filtered->flatten();
        }
    }

    public function games()
    {
        return $events = Event::orderBy('start', 'ASC')->where('type', '=', 'Game')->whereDoesntHave('game')->get();
    }

    public function create()
    {
        return;
    }

    public function show($id)
    {
        return;
    }

    public function edit($id)
    {
        return;
    }

    public function store(StoreEvent $request)
    {
        $event = new Event;
        $event->name = $request->name;
        $event->description = $request->description;
        $event->start = $request->start;
        $event->end = $request->end;
        $event->location = $request->location;
        $event->type = $request->type;

        $eventgroups = [];

        for($i = 0; $i<sizeof($request->groups);$i++)
        {           
            $eventgroups[] = new EventGroup(array('event_id' => $event->id,'group_id' => $request->groups[$i]["id"]));
        }

        try
        {
            DB::transaction(function() use($event,$eventgroups)
            {
                $event->save();
                if(!empty($eventgroups))
                {
                    $event->eventgroups()->saveMany($eventgroups);    
                }
            });
        }
        catch (\Exception $e)
        {                   

            return response()->json( ['error'=>$e], 500);
            return response()->json( ['error'=>'Database transaction failed'], 500);
        }
        return response()->json( ['success'=>"Event created"], 200);
    }




    public function update(UpdateEvent $request, $id)
    {
        $event = Event::find($id);
        $event->name = $request->name;
        $event->description = $request->description;
        $event->start = $request->start;
        $event->end = $request->end;
        $event->location = $request->location;
        $event->type = $request->type;

        $eventgroups = [];
        for($i = 0; $i<sizeof($request->groups);$i++)
        {           
            $eventgroups[] = new EventGroup(array('event_id' => $event->id,'group_id' => $request->groups[$i]["id"]));
        }

        try
        {
            DB::transaction(function() use($event,$eventgroups)
            {
                $event->save();
                $event->eventgroups()->delete();

                if(!empty($eventgroups))
                {
                    $event->eventgroups()->saveMany($eventgroups);    
                }
            });
        }
        catch (\Exception $e)
        {                   
            return response()->json( ['error'=>'Database transaction failed'], 500);
        }
        return response()->json( ['success'=>"Event updated"], 200);
    }



    public function destroy($id)
    {
        $event = Event::find($id);
        try
        {
            DB::transaction(function() use($event)
            {
                $event->eventgroups()->delete();

                if($event->game()->exists() == true)
                {
                    foreach($event->game()->jobs() as $job) 
                    { 
                        $job->delete(); 
                    }
                    foreach($event->game()->rosters() as $roster) 
                    { 
                        $roster->delete(); 
                    }
                    $event->game()->delete();
                }

                if($event->training()->exists() == true)
                {
                    foreach($event->training()->attendances() as $attendance)
                    { 
                        $attendance->delete(); 
                    }
                    $event->training()->delete();
                }

                $event->delete();
            });
        }
        catch (\Exception $e)
        {                   
            return response()->json( ['error'=>'Database transaction failed'], 500);
        }
        return response()->json( ['success'=>"Event deleted"], 200);
    }
    
}