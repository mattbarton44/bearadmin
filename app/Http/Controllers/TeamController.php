<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Training;
use App\Team;
use App\Game;
use App\Helpers\Bears;
use DB;
use Auth;
use App\Http\Requests\StoreTeam;
use App\Http\Requests\UpdateTeam;

class TeamController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('perm:teams')->only('index');
        $this->middleware('perm:teams_create')->only('create');
        $this->middleware('perm:teams_show')->only('show');
        $this->middleware('perm:teams_edit')->only('edit');
        $this->middleware('perm:teams_create')->only('store');
        $this->middleware('perm:teams_edit')->only('update');
        $this->middleware('perm:teams_destroy')->only('destroy');
    }

    public function index()
    {
        return Team::with('group.users')->get();
    }

    public function create()
    {
        return;
    }

    public function show($id)
    {
        return Team::with('group.users')->with('games')->with('trainings')->find($id);
    }

    public function edit($id)
    {
        return;
    }

    public function store(StoreTeam $request)
    {
        $team = new Team;
        $team->name = $request->name;
        $team->group_id = $request->group_id;

        try
        {
            DB::transaction(function() use($team)
            {
                $team->save();
            });
        }
        catch (\Exception $e)
        {                   
            return response()->json( ['error'=>$e->getMessage()], 500);
        }
        return response()->json( ['success'=>"Team created"], 200);
    }

    public function update(UpdateTeam $request, $id)
    {
        $team = Team::find($id);
        $team->name = $request->name;
        $team->group_id = $request->group_id;

        try
        {
            DB::transaction(function() use($team)
            {
                $team->save();
            });
        }
        catch (\Exception $e)
        {                   

            return response()->json( ['error'=>$e->getMessage()], 500);
        }
        return response()->json( ['success'=>"Team updated"], 200);
    }

    public function destroy($id)
    {
        $team = Team::find($id);
        try
        {
            DB::transaction(function() use($team)
            {
                $team->delete();
            });
        }
        catch (\Exception $e)
        {                   
            return response()->json( ['error'=>$e->getMessage()], 500);
        }
        return response()->json( ['success'=>"Team deleted"], 200);
    }
    
}