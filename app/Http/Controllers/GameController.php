<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Game;
use App\GameJob;
use App\GameRoster;
use App\Team;
use App\Event;
use App\EventGroup;
use App\Helpers\Bears;
use DB;
use Auth;
use App\Http\Requests\StoreGame;
use App\Http\Requests\UpdateGame;

class GameController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('perm:games')->only('index');
        $this->middleware('perm:games_create')->only('create');
        $this->middleware('perm:games_show')->only('show');
        $this->middleware('perm:games_edit')->only('edit');
        $this->middleware('perm:games_create')->only('store');
        $this->middleware('perm:games_edit')->only('update');
        $this->middleware('perm:games_destroy')->only('destroy');
    }

    public function index()
    {
        return Game::with('team')->with('event')->with('jobs.user')->with('rosters')->get();
    }

    public function create()
    {
        return;
    }

    public function show($id)
    {
        return;
    }

    public function edit($id)
    {
        return;
    }

    public function store(StoreGame $request)
    {
        $game = new Game;
        $game->event_id = $request->id;
        $game->team_id = $request->team;
        $game->homeTeam = $request->homeTeam;
        $game->awayTeam = $request->awayTeam;
        $game->homeTeamGoals = 0;
        $game->awayTeamGoals = 0;
        $game->complete = false;

        try
        {
            DB::transaction(function() use($game)
            {
                $game->save();
            });
        }
        catch (\Exception $e)
        {    
            return response()->json( ['error'=>$e], 500);
        }
        return response()->json( ['success'=>"Game created"], 200);
    }

    public function update(UpdateGame $request, $id)
    {
        $game = Game::find($id);
        $game->homeTeam = $request->homeTeam;
        $game->awayTeam = $request->awayTeam;
        $game->homeTeamGoals = $request->homeTeamGoals;
        $game->awayTeamGoals = $request->awayTeamGoals;
        $game->complete = $request->complete;
        $game->team_id = $request->team_id;

        $gameJobs = [];
        for($i = 0; $i<sizeof($request->jobs);$i++)
        {    
            $job = GameJob::find($request->jobs[$i]["id"]); 
            $job->attended = $request->jobs[$i]["attended"];
            $gameJobs[] = $job;
        }

        try
        {
            DB::transaction(function() use($game,$gameJobs)
            {
                $game->save();
                if(!empty($gameJobs))
                {
                    $game->jobs()->saveMany($gameJobs);    
                }
            });
        }
        catch (\Exception $e)
        {                   
            return response()->json( ['error'=>'Database transaction failed'], 500);
        }
        return response()->json( ['success'=>"Game updated"], 200);
        return;
    }

    public function destroy($id)
    {
        return;
    }
    
}