<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Configuration;
use DB;

class ConfigurationController extends Controller
{

    public function index()
    {
        return Configuration::all();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        for($i = 0; $i<sizeof($request->data);$i++)
        {           
            $config = Configuration::find($request->data[$i]['id']);
            if($request->data[$i]['value'] == null)
            {
                $config->value = '';
            }
            else
            {                
                $config->value = $request->data[$i]['value'];
            }
            $configs[] = $config;
        }

        try
        {
            DB::transaction(function() use($configs)
            {
                for($i = 0; $i<sizeof($configs);$i++)
                {           
                    $configs[$i]->save();
                }
            });
        }
        catch (\Exception $e)
        {                   

            return response()->json( ['error'=>$e->getMessage()], 500);
        }
        return response()->json( ['success'=>"Configuration updated"], 200);
    }

    public function destroy($id)
    {
        //
    }
}
