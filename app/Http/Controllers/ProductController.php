<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Product;
use App\Order;
use App\User;
use App\Helpers\Bears;
use DB;
use Auth;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('perm:member')->only('index');
        $this->middleware('perm:member')->only('shop');
        $this->middleware('perm:member')->only('show');
        $this->middleware('perm:shop_admin')->only('store');
        $this->middleware('perm:shop_admin')->only('update');
        $this->middleware('perm:shop_admin')->only('destroy');
    }

    public function index()
    {
        return Product::orderBy('updated_at', 'DESC')->with('orders')->get();
    }

    public function shop()
    {
        $products = Product::where('available','=',true)->orderBy('updated_at', 'DESC')->with('orders')->get();
        
        $filteredProducts = [];
        foreach($products as $product)
        {
            $count = Auth::user()->orders->where('product_id','=',$product->id)->where('status','=','Complete')->count();
            if($count < $product->max_count)
            {
                $filteredProducts[] = $product;
            }
        }
        return $filteredProducts;
    }


    public function create()
    {
        return;
    }

    public function show($id)
    {
        return Product::with('orders')->find($id);
    }

    public function edit($id)
    {
        return;
    }

    public function store(Request $request)
    {
        // add request validation in at some point
        $product = new Product;
        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->available = $request->available;
        $product->max_count = $request->max_count;
        $product->category = $request->category;

        $form_data = [];
        for($i = 0; $i<sizeof($request->form);$i++)
        {           
            $form_data[] = array(
                'label' => $request->form[$i]['label'],
                'type' => $request->form[$i]['type'],
                'options' => explode('/', $request->form[$i]['options']),
                'placeholder' => $request->form[$i]['placeholder'],
                'help_text' => $request->form[$i]['help_text']
            );
        }
        $product->form = json_encode($form_data);

        try
        {
            DB::transaction(function() use($product)
            {
                $product->save();
            });
        }
        catch (\Exception $e)
        {                   
            return response()->json( ['error'=>'Database transaction failed'], 500);
        }
        return response()->json( ['success'=>"Product created"], 200);

    }

    public function update(Request $request, $id)
    {
        // add request validation in at some point
        $product = Product::find($id);
        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->available = $request->available;
        $product->max_count = $request->max_count;
        $product->category = $request->category;

        $form_data = [];
        for($i = 0; $i<sizeof($request->form);$i++)
        {           
            $options = $request->form[$i]['options'];
            if(is_array($options) != true)
            {
                $options = explode('/', $request->form[$i]['options']);
            }
            $form_data[] = array(
                'label' => $request->form[$i]['label'],
                'type' => $request->form[$i]['type'],
                'options' => $options,
                'placeholder' => $request->form[$i]['placeholder'],
                'help_text' => $request->form[$i]['help_text']
            );
        }
        $product->form = json_encode($form_data);

        try
        {
            DB::transaction(function() use($product)
            {
                $product->save();
            });
        }
        catch (\Exception $e)
        {                   
            return response()->json( ['error'=>'Database transaction failed'], 500);
        }
        return response()->json( ['success'=>"Product updated"], 200);

    }

    public function destroy(Request $request, $id)
	{

    }
}