<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Article;
use App\User;
use App\Group;
use App\ArticleGroup;
use App\Helpers\Bears;
use DB;
use Auth;
use App\Http\Requests\StoreArticle;
use App\Http\Requests\UpdateArticle;


class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware('perm:articles')->only('index');
        $this->middleware('perm:articles_create')->only('create');
        $this->middleware('perm:articles_show')->only('show');
        $this->middleware('perm:articles_edit')->only('edit');
        $this->middleware('perm:articles_create')->only('store');
        $this->middleware('perm:articles_edit')->only('update');
        $this->middleware('perm:articles_destroy')->only('destroy');
    }

    public function index()
    {
        if(Auth::user()->has('articles_administrate'))
        {
            return Article::orderBy('updated_at', 'DESC')->with('user')->with('groups')->with('type')->with('comments')->get();
        }
        else
        {
            $groups = Auth::user()->groups->pluck('id');
            $articles = Article::orderBy('updated_at', 'DESC')->with('user')->with('groups')->with('type')->with('comments')->get();

            $filtered = $articles->reject(function ($article) use ($groups) {
                if($article->public == true || sizeof($article->groups) == 0)
                {
                    return false;
                }
                foreach($article->groups as $group)
                {
                    if(in_array($group->id, $groups->toArray()) == true)
                    {
                        return false;
                    }
                }
                return true;
            });
            return $filtered->flatten();
        }
    }

    public function create()
    {
        return;
    }

    public function show($id)
    {
        $article = Article::with('user')->with('groups.users')->with('type')->with('comments.user')->get()->find($id);
        $ingroup = false;

        $groups = Auth::user()->groups->pluck('id');

        if($article->public == true || sizeof($article->groups) == 0)
        {
            $ingroup = true;
        }
        foreach($article->groups as $group)
        {
            if(in_array($group->id, $groups->toArray()) == true)
            {
                $ingroup = true;
            }
        }

        if(Auth::user()->has('articles_administrate') || $ingroup == true)
        {
            return $article;
        }
        return "permission denied";
    }

    public function edit($id)
    {
        return;
    }

    public function store(StoreArticle $request)
    {
        $article = new Article;
        $article->subject = $request->subject;
        $article->body = $request->content;
        $article->public = $request->public;
        $article->articleType_id = $request->type_id;
        $article->user_id = Auth::user()->id;

        $articlegroups = [];
        for($i = 0; $i<sizeof($request->groups);$i++)
        {           
            $articlegroups[] = new ArticleGroup(array('article_id' => $article->id,'group_id' => $request->groups[$i]["id"]));
        }

        try
        {
            DB::transaction(function() use($article,$articlegroups)
            {
                $article->save();
                if(!empty($articlegroups))
                {
                    $article->articlegroups()->saveMany($articlegroups);    
                }
            });
        }
        catch (\Exception $e)
        {                   
            return response()->json( ['error'=>'Database transaction failed'], 500);
        }
        return response()->json( ['success'=>"Article created"], 200);
    }

    public function update(UpdateArticle $request, $id)
    {
        $article = Article::find($id);
        $article->subject = $request->subject;
        $article->body = $request->content;
        $article->public = $request->public;
        $article->articleType_id = $request->type_id;
        $article->user_id = Auth::user()->id;

        $articlegroups = [];
        for($i = 0; $i<sizeof($request->groups);$i++)
        {           
            $articlegroups[] = new ArticleGroup(array('article_id' => $article->id,'group_id' => $request->groups[$i]["id"]));
        }

        try
        {
            DB::transaction(function() use($article,$articlegroups)
            {
                $article->save();
                $article->articlegroups()->delete();

                if(!empty($articlegroups))
                {
                    $article->articlegroups()->saveMany($articlegroups);    
                }
            });
        }
        catch (\Exception $e)
        {                   
            return response()->json( ['error'=>'Database transaction failed'], 500);
        }
        return response()->json( ['success'=>"Article updated"], 200);
    }

    public function destroy($id)
    {
        $article = Article::find($id);
        try
        {
            DB::transaction(function() use($article)
            {
                $article->articlegroups()->delete();
                $article->comments()->delete();
                $article->delete();
            });
        }
        catch (\Exception $e)
        {                   
            return response()->json( ['error'=>'Database transaction failed'], 500);
        }
        return response()->json( ['success'=>"Article deleted"], 200);
    }
}