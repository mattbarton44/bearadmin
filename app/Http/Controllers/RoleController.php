<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Helpers\Bears;
use App\Role;
use App\Permission;
use App\RolePermission;
use DB;
use App\Http\Requests\StoreRole;
use App\Http\Requests\UpdateRole;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('perm:roles')->only('index');
        $this->middleware('perm:roles_edit')->only('edit');
        $this->middleware('perm:roles_create')->only('store');
        $this->middleware('perm:roles_edit')->only('update');
        $this->middleware('perm:roles_destroy')->only('destroy');
    }

    public function index()
    {
        return Role::orderBy('name', 'ASC')->with('rolepermissions.permission')->with('users')->get();
    }

    public function edit($id)
    {
        return Role::find($id)->with('permissions', Permission::orderBy('name', 'ASC'))->get();
    }

    public function store(StoreRole $request)
    {
        $role = new Role;
        $role->name = $request->name;
        $role->description = $request->description;

        $rolepermissions = [];
        for($i = 0; $i<sizeof($request->permissions);$i++)
        {           
            $rolepermissions[] = new RolePermission(array('role_id' => $role->id,'permission_id' => $request->permissions[$i]["id"]));
        }

        try
        {
            DB::transaction(function() use($role,$rolepermissions)
            {
                $role->save();
                if(!empty($rolepermissions))
                {
                    $role->rolePermissions()->saveMany($rolepermissions);    
                }
            });
        }
        catch (\Exception $e)
        {                   
            return response()->json( ['error'=>$e->getMessage()], 500);
        }
        return response()->json( ['success'=>"Role created"], 200);
    }

    public function update(UpdateRole $request, $id)
    {
        $role = Role::find($id);
        $role->name = $request->name;
        $role->description = $request->description;
        
        $rolepermissions = [];
        for($i = 0; $i<sizeof($request->permissions);$i++)
        {           
            $rolepermissions[] = new RolePermission(array('role_id' => $role->id,'permission_id' => $request->permissions[$i]["id"]));
        }

        try
        {
            DB::transaction(function() use($role,$rolepermissions)
            {
                $role->save();
                $role->rolePermissions()->delete();
                if(!empty($rolepermissions))
                {
                    $role->rolePermissions()->saveMany($rolepermissions);    
                }
            });
        }
        catch (\Exception $e)
        {                   

            return response()->json( ['error'=>$e->getMessage()], 500);
        }
        return response()->json( ['success'=>"Role updated"], 200);
    }

    public function destroy(Request $request, $id)
    {
        $role = Role::find($id);
        try
        {
            DB::transaction(function() use($role)
            {
                $role->rolePermissions()->delete();
                $role->delete();
            });
        }
        catch (\Exception $e)
        {                   
            return response()->json( ['error'=>$e->getMessage()], 500);
        }
        return response()->json( ['success'=>"Role deleted"], 200);
    }

}
