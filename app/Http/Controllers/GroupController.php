<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Group;
use App\GroupMember;
use App\Helpers\Bears;
use DB;
use App\Http\Requests\StoreGroup;
use App\Http\Requests\UpdateGroup;

class GroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('perm:groups')->only('index');
        $this->middleware('perm:groups')->only('solo');
        $this->middleware('perm:groups_create')->only('create');
        $this->middleware('perm:groups_show')->only('show');
        $this->middleware('perm:groups_edit')->only('edit');
        $this->middleware('perm:groups_create')->only('store');
        $this->middleware('perm:groups_edit')->only('update');
        $this->middleware('perm:groups_destroy')->only('destroy');
    }

    public function index()
    {
        return Group::orderBy('name', 'ASC')->with('users')->get();
    }

    public function solo()
    {
        return Group::orderBy('name', 'ASC')->get();
    }

    public function create()
    {
        return;
    }

    public function show($id)
    {
        return;
    }

    public function edit($id)
    {
        return;
    }

    public function store(StoreGroup $request)
    {
        $group = new Group;
        $group->name = $request->name;

        $groupmembers = [];
        for($i = 0; $i<sizeof($request->users);$i++)
        {           
            $groupmembers[] = new GroupMember(array('group_id' => $group->id,'user_id' => $request->users[$i]["id"]));
        }

        try
        {
            DB::transaction(function() use($group,$groupmembers)
            {
                $group->save();
                if(!empty($groupmembers))
                {
                    $group->groupMembers()->saveMany($groupmembers);    
                }
            });
        }
        catch (\Exception $e)
        {                   
            return response()->json( ['error'=>$e->getMessage()], 500);
        }
        return response()->json( ['success'=>"Group created"], 200);
    }


    public function update(UpdateGroup $request, $id)
    {
        $group = Group::find($id);
        $group->name = $request->name;

        $groupmembers = [];
        for($i = 0; $i<sizeof($request->users);$i++)
        {           
            $groupmembers[] = new GroupMember(array('group_id' => $group->id,'user_id' => $request->users[$i]["id"]));
        }

        try
        {
            DB::transaction(function() use($group,$groupmembers)
            {
                $group->save();
                $group->groupMembers()->delete();
                if(!empty($groupmembers))
                {
                    $group->groupMembers()->saveMany($groupmembers);    
                }
            });
        }
        catch (\Exception $e)
        {                   

            return response()->json( ['error'=>$e->getMessage()], 500);
        }
        return response()->json( ['success'=>"Group updated"], 200);
    }

    public function destroy(Request $request, $id)
    {
        $group = Group::find($id);
        try
        {
            DB::transaction(function() use($group)
            {
                $group->groupMembers()->delete();
                $group->delete();
            });
        }
        catch (\Exception $e)
        {                   
            return response()->json( ['error'=>$e->getMessage()], 500);
        }
        return response()->json( ['success'=>"Group deleted"], 200);
    }
}