<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Product;
use App\Order;
use App\User;
use App\Registration;
use App\Helpers\Bears;
use DB;
use Auth;
use Stripe\{Charge, customer};

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('perm:member')->only('store');
    }

    public function index()
    {
        return Order::orderBy('created_at', 'DESC')->with('user')->with('product')->get();
    }
    
    public function show($id)
    {
        return Order::with('user')->with('product')->find($id);
    }


    public function store(Request $request)
    {
        $product = Product::find($request->input('product_id'));

        if($product == null)
        {
        	return 'Error, invalid product';
        }

		$form = [];
		$product_form = json_decode($product->form);

		for($i=0; $i<sizeof($product_form); $i++)
		{
			$key = str_replace(' ', '_', $product_form[$i]->label);

			switch($product_form[$i]->type)
			{
				case "input":
				case "select":
					$form[$product_form[$i]->label] = $request->input($key);
					break;
				case "image":
					$image = $request->file($key);
					if($request->hasFile($key))
					{
						if($request->file($key)->isValid() == true)
						{
							$path = $request->file($key)->store('public/uploads');
							$form[$product_form[$i]->label] = $path;
						}
					}
					break;
			}
		}

        $order = new Order;
        $order->form = json_encode($form);
        $order->user_id = Auth::id();
        $order->product_id = $product->id;
        $order->status = 'Complete';
       	$order->save();

		if($product->category == 'BUIHA Registration')
		{
			$pieces = explode(" ", $product->name);
	        $reg = new Registration;
	        $reg->user_id = Auth::id();
	        $reg->season = $pieces[3];
	        $reg->type = $pieces[1];
	        $reg->status = 'Pending';
	        $reg->form = json_encode($form);
	       	$reg->save();
		}

        return 'Order successful!';


    }

}