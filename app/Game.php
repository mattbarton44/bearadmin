<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Game extends Model
{
    use SoftDeletes;
	protected $table = 'games';
    protected $dates = ['deleted_at'];

    public function event()
    {
        return $this->belongsTo('App\Event','event_id');
    }

    public function team()
    {
        return $this->belongsTo('App\Team','team_id');
    }

    public function jobs()
    {
        return $this->hasMany('App\GameJob','game_id');
    }

    public function rosters()
    {
        return $this->hasMany('App\GameRoster','game_id');
    }
}
