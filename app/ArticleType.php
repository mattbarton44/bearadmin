<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleType extends Model
{
	protected $table = 'articletypes';
	
	public function articles()
    {
        return $this->hasMany('Article','article_id');
    }
}
