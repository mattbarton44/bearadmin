<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTables extends Migration
{
    public function up()
    { 
        Schema::create('configuration', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('setting');
            $table->string('value');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('roles', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
         
        Schema::create('permissions', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
         
        Schema::create('rolepermissions', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id')->unsigned();
            $table->foreign('role_id')->references('id')->on('roles');
            $table->integer('permission_id')->unsigned();
            $table->foreign('permission_id')->references('id')->on('permissions');
            $table->softDeletes();
            $table->timestamps();
        });
         
        Schema::create('users', function(Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email', 100);
            $table->string('password', 100);
            $table->timestamp('last_login');
            $table->string('avatarPath')->nullable();
            $table->string('facebook_id')->nullable();
            $table->integer('role_id')->unsigned()->nullable();
            $table->foreign('role_id')->references('id')->on('roles');
            $table->string('remember_token', 64)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
         
        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });
         
        Schema::create('groups', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->unique();
            $table->softDeletes();
            $table->timestamps();
        });
         
        Schema::create('groupmembers', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('group_id')->unsigned();
            $table->foreign('group_id')->references('id')->on('groups');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('articletypes', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('colour', 255);
            $table->softDeletes();
            $table->timestamps();
        });     
                           
        Schema::create('articles', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('articleType_id')->unsigned();
            $table->foreign('articleType_id')->references('id')->on('articletypes');
            $table->boolean('public');
            $table->string('subject', 255);
            $table->mediumText('body');
            $table->softDeletes();
            $table->timestamps();
        });
         
        Schema::create('articlecomments', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('article_id')->unsigned();
            $table->foreign('article_id')->references('id')->on('articles');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->text('comment');
            $table->softDeletes();
            $table->timestamps();
        });
                  
        Schema::create('articlegroups', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('article_id')->unsigned();
            $table->foreign('article_id')->references('id')->on('articles');
            $table->integer('group_id')->unsigned();
            $table->foreign('group_id')->references('id')->on('groups');
            $table->softDeletes();
            $table->timestamps();
        });
         
        Schema::create('teams', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->integer('group_id')->unsigned();
            $table->foreign('group_id')->references('id')->on('groups');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('events', function(Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->string('name');
            $table->text('description');
            $table->string('location');
            $table->timestamp('start')->nullable();
            $table->timestamp('end')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('eventgroups', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id')->unsigned();
            $table->foreign('event_id')->references('id')->on('events');
            $table->integer('group_id')->unsigned();
            $table->foreign('group_id')->references('id')->on('groups');
            $table->softDeletes();
            $table->timestamps();
        });
         
        Schema::create('trainings', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id')->unsigned();
            $table->foreign('event_id')->references('id')->on('events');
            $table->integer('team_id')->unsigned();
            $table->foreign('team_id')->references('id')->on('teams');
            $table->softDeletes();
            $table->timestamps();
        });
         
        Schema::create('trainingattendances', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('training_id')->unsigned();
            $table->foreign('training_id')->references('id')->on('trainings');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->boolean('attended');
            $table->softDeletes();
            $table->timestamps();
        });
        
        Schema::create('games', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id')->unsigned();
            $table->foreign('event_id')->references('id')->on('events');
            $table->integer('team_id')->unsigned();
            $table->foreign('team_id')->references('id')->on('teams');
            $table->string('homeTeam');
            $table->string('awayTeam');
            $table->string('homeTeamGoals');
            $table->string('awayTeamGoals');
            $table->boolean('complete');
            $table->softDeletes();
            $table->timestamps();
        });
            
        Schema::create('gamerosters', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('game_id')->unsigned();
            $table->foreign('game_id')->references('id')->on('games');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->boolean('attended');
            $table->softDeletes();
            $table->timestamps();
        }); 
         
        Schema::create('gamejobs', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('game_id')->unsigned();
            $table->foreign('game_id')->references('id')->on('games');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('role');
            $table->boolean('prevExp');
            $table->boolean('attended');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('products', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->integer('price');
            $table->boolean('available');
            $table->integer('max_count');
            $table->string('category');
            $table->json('form');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('orders', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('products');
            $table->string('status');
            $table->json('form');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('registrations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('season');
            $table->string('type');
            $table->string('status');
            $table->json('form');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('registrations');
        Schema::dropIfExists('orders');
        Schema::dropIfExists('products');
        Schema::dropIfExists('gamejobs');
        Schema::dropIfExists('gamerosters');
        Schema::dropIfExists('games');
        Schema::dropIfExists('trainingattendances');
        Schema::dropIfExists('trainings');
        Schema::dropIfExists('events');
        Schema::dropIfExists('eventgroups');
        Schema::dropIfExists('teams');
        Schema::dropIfExists('articlegroups');
        Schema::dropIfExists('articlecomments');
        Schema::dropIfExists('articles');
        Schema::dropIfExists('articletypes');
        Schema::dropIfExists('groupmembers');
        Schema::dropIfExists('groups');
        Schema::dropIfExists('passwordResets');
        Schema::dropIfExists('users');
        Schema::dropIfExists('rolepermissions');
        Schema::dropIfExists('permissions');
        Schema::dropIfExists('roles');
        Schema::dropIfExists('configuration');
        Schema::dropIfExists('password_resets');
    }
}