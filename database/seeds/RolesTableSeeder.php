<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    public function run()
    {
        Role::create(array(
			'name' => 'Administrator',
			'description' => ''
		));

		Role::create(array(
			'name' => 'User',
			'description' => ''
		));
    }
}
