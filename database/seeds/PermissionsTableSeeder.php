<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        Permission::create(array('name' => 'administrator',			'description' => 'Administrator'));
        Permission::create(array('name' => 'member',				'description' => 'Member'));
        Permission::create(array('name' => 'coach',					'description' => 'Coach'));
        Permission::create(array('name' => 'committee',				'description' => 'Committee member'));

		Permission::create(array('name' => 'permissions',			'description' => 'View all permissions'));

		Permission::create(array('name' => 'settings',				'description' => 'Edit your settings'));
		Permission::create(array('name' => 'configuration',			'description' => 'Configure app settings'));

		Permission::create(array('name' => 'users',					'description' => 'View all users'));
		Permission::create(array('name' => 'users_show',			'description' => 'View details for a user'));
		Permission::create(array('name' => 'users_create',			'description' => 'Create a new user'));
		Permission::create(array('name' => 'users_edit',			'description' => 'Edit a users details'));
		Permission::create(array('name' => 'users_destroy',			'description' => 'Delete a user'));

		Permission::create(array('name' => 'groups',				'description' => 'View all groups'));
		Permission::create(array('name' => 'groups_show',			'description' => 'View details for a group'));
		Permission::create(array('name' => 'groups_create',			'description' => 'Create a new group'));
		Permission::create(array('name' => 'groups_edit',			'description' => 'Edit a groups details'));
		Permission::create(array('name' => 'groups_destroy',		'description' => 'Delete a group'));

		Permission::create(array('name' => 'roles',					'description' => 'View all roles'));
		Permission::create(array('name' => 'roles_show',			'description' => 'View details for a role'));
		Permission::create(array('name' => 'roles_create',			'description' => 'Create a new role'));
		Permission::create(array('name' => 'roles_edit',			'description' => 'Edit a roles details'));
		Permission::create(array('name' => 'roles_destroy',			'description' => 'Delete a role'));

		Permission::create(array('name' => 'articles',				'description' => 'View all articles'));
		Permission::create(array('name' => 'articles_show',			'description' => 'View details for an article'));
		Permission::create(array('name' => 'articles_create',		'description' => 'Create a new article'));
		Permission::create(array('name' => 'articles_edit',			'description' => 'Edit an articles details'));
		Permission::create(array('name' => 'articles_destroy',		'description' => 'Delete an article'));
		Permission::create(array('name' => 'articles_administrate',	'description' => 'Modify any article'));
		
		Permission::create(array('name' => 'articlecomments_admin',	'description' => 'Perform CRUD on other users comments'));
		Permission::create(array('name' => 'articlecomments_create','description' => 'Comment on an article'));
		Permission::create(array('name' => 'articlecomments_edit',	'description' => 'Edit your comment'));
		Permission::create(array('name' => 'articlecomments_destroy','description' => 'Remove your comment'));

		Permission::create(array('name' => 'teams',					'description' => 'View all teams'));
		Permission::create(array('name' => 'teams_show',			'description' => 'View details for a team'));
		Permission::create(array('name' => 'teams_create',			'description' => 'Create a new team'));
		Permission::create(array('name' => 'teams_edit',			'description' => 'Edit a teams details'));
		Permission::create(array('name' => 'teams_destroy',			'description' => 'Delete a team'));

		Permission::create(array('name' => 'games',					'description' => 'View all games'));
		Permission::create(array('name' => 'games_show',			'description' => 'View details for a game'));
		Permission::create(array('name' => 'games_create',			'description' => 'Create a new game'));
		Permission::create(array('name' => 'games_edit',			'description' => 'Edit a games details'));
		Permission::create(array('name' => 'games_destroy',			'description' => 'Delete a game'));

		Permission::create(array('name' => 'gamejobs',				'description' => 'View all gamejobs'));
		Permission::create(array('name' => 'gamejobs_show',			'description' => 'View details for a gamejob'));
		Permission::create(array('name' => 'gamejobs_create',		'description' => 'Create a new gamejob'));
		Permission::create(array('name' => 'gamejobs_edit',			'description' => 'Edit a gamejobs details'));
		Permission::create(array('name' => 'gamejobs_destroy',		'description' => 'Delete a gamejob'));

		Permission::create(array('name' => 'trainings',				'description' => 'View all trainings'));
		Permission::create(array('name' => 'trainings_show',		'description' => 'View details for a training'));
		Permission::create(array('name' => 'trainings_create',		'description' => 'Create a new training'));
		Permission::create(array('name' => 'trainings_edit',		'description' => 'Edit a trainings details'));
		Permission::create(array('name' => 'trainings_destroy',		'description' => 'Delete a training'));

		Permission::create(array('name' => 'events',				'description' => 'View all events'));
		Permission::create(array('name' => 'events_show',			'description' => 'View details for an event'));
		Permission::create(array('name' => 'events_create',			'description' => 'Create a new event'));
		Permission::create(array('name' => 'events_edit',			'description' => 'Edit an events details'));
		Permission::create(array('name' => 'events_destroy',		'description' => 'Delete an event'));

		Permission::create(array('name' => 'shop',					'description' => 'User is able to make purchases from the shop'));
		Permission::create(array('name' => 'shop_admin',			'description' => 'Manage products in the shop'));

		Permission::create(array('name' => 'registrations',			'description' => 'View all registrations'));
		Permission::create(array('name' => 'registrations_admin',	'description' => 'Manage the status of registrations'));
    }
}
