<?php

use Illuminate\Database\Seeder;
use App\Configuration;

class SettingsTableSeeder extends Seeder
{
    public function run()
    {
        Configuration::create(array(
			'name' => 'Registration Year',
			'setting' => 'reg_year',
			'value' => '2017'
		));

		Configuration::create(array(
			'name' => 'Student Registration Product Id',
			'setting' => 'reg_student_product_id',
			'value' => 0
		));

		Configuration::create(array(
			'name' => 'Staff Registration Product Id',
			'setting' => 'reg_alumni_product_id',
			'value' => 0
		));

		Configuration::create(array(
			'name' => 'Alumni Registration Product Id',
			'setting' => 'reg_staff_product_id',
			'value' => 0
		));

		Configuration::create(array(
			'name' => 'Featured Article Id',
			'setting' => 'featured_article_id',
			'value' => '0'
		));

		Configuration::create(array(
			'name' => 'A Team BUIHA Division Id',
			'setting' => 'a_div_id',
			'value' => '0'
		));

		Configuration::create(array(
			'name' => 'A Team BUIHA Division Name',
			'setting' => 'a_div_name',
			'value' => ''
		));

		Configuration::create(array(
			'name' => 'B Team BUIHA Division Id',
			'setting' => 'b_div_id',
			'value' => '0'
		));

		Configuration::create(array(
			'name' => 'B Team BUIHA Division Name',
			'setting' => 'b_div_name',
			'value' => ''
		));
		
		Configuration::create(array(
			'name' => 'C Team BUIHA Division Id',
			'setting' => 'c_div_id',
			'value' => '0'
		));

		Configuration::create(array(
			'name' => 'C Team BUIHA Division Name',
			'setting' => 'c_div_name',
			'value' => ''
		));
		
		Configuration::create(array(
			'name' => 'D Team BUIHA Division Id',
			'setting' => 'd_div_id',
			'value' => '0'
		));

		Configuration::create(array(
			'name' => 'D Team BUIHA Division Name',
			'setting' => 'd_div_name',
			'value' => ''
		));
		
		Configuration::create(array(
			'name' => 'E Team BUIHA Division Id',
			'setting' => 'e_div_id',
			'value' => '0'
		));

		Configuration::create(array(
			'name' => 'E Team BUIHA Division Name',
			'setting' => 'e_div_name',
			'value' => ''
		));


    }
}
