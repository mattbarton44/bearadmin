<?php

use Illuminate\Database\Seeder;
use App\ArticleType;

class ArticleTypesTableSeeder extends Seeder
{
    public function run()
    {
    	ArticleType::create(array(
			'name' => 'General Information',
			'colour' => 'general'
		));

    	ArticleType::create(array(
			'name' => 'Socials',
			'colour' => 'socials'
		));

    	ArticleType::create(array(
			'name' => 'Fixtures',
			'colour' => 'fixtures'
		));

    	ArticleType::create(array(
			'name' => 'Off Ice',
			'colour' => 'off_ice'
		));

    	ArticleType::create(array(
			'name' => 'Varsity',
			'colour' => 'varsity'
		));

    	ArticleType::create(array(
			'name' => 'Other',
			'colour' => 'other'
		));
    }
}