<?php

use Illuminate\Database\Seeder;
use App\Group;

class GroupsTableSeeder extends Seeder
{
    public function run()
    {
		Group::create(array(
			'name' => 'Registered Players'
		));

		Group::create(array(
			'name' => 'Committee Members'
		));

		Group::create(array(
			'name' => 'Administrators'
		));
    }
}
