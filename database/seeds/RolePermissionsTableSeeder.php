<?php

use Illuminate\Database\Seeder;
use App\RolePermission;
use App\Role;
use App\Permission;

class RolePermissionsTableSeeder extends Seeder
{
    public function run()
    {
    	$role_id = Role::where('name','=','Administrator')->first()->id;
    	$permissions = Permission::all();
    	foreach($permissions as $permission)
    	{
    		RolePermission::create(array(
				'role_id' => $role_id,
				'permission_id' => $permission->id
			));
    	}
    }
}