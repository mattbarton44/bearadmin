<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
    	User::create(array(
			'first_name' => 'Sheffield',
			'last_name' => 'Bears',
			'email' => 'info@sheffieldbears.com',
			'password' => Hash::make('password'),
			'remember_token' => '',
			'role_id' => Role::where('name','=','Administrator')->first()->id
		));

    	//factory('App\User', 10)->create();
    }
}
