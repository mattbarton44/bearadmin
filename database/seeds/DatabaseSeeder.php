<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
    	$this->call(RolesTableSeeder::class);
    	$this->call(PermissionsTableSeeder::class);
    	$this->call(RolePermissionsTableSeeder::class);
    	$this->call(SettingsTableSeeder::class);
    	$this->call(GroupsTableSeeder::class);
    	$this->call(UsersTableSeeder::class);
    	$this->call(GroupMembersTableSeeder::class);
        $this->call(ArticleTypesTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
    }
}
