<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder
{
    public function run()
    {        

    	Product::create(array(
			'name' => 'BUIHA Student Registration 2017/18',
			'description' => 'If you are a registered full or part-time student at either The University of Sheffield or Sheffield Hallam University',
			'price' => 6000,
			'available' => true,
			'max_count' => 1,
			'category' => 'BUIHA Registration',
			'form' => '[{"label":"Telephone Number", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"Address Line 1", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"Address Line 2", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"Address Line 3", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"Postcode", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"Nationality", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"Date Of Birth", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"University Name", "type":"input", "options":[], "placeholder":"", "help_text":"e.g. Sheffield Hallam University or University of Sheffield" },{"label":"Course Title", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"Year Of Graduation", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"Student Number", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"Image Requirements", "type":"label", "options":[], "placeholder":"All images must be smaller than 500kb and either .jpg .png or .gif.", "help_text":"" },{"label":"Personal Identification", "type":"image", "options":[], "placeholder":"", "help_text":"A photocopy or clear cropped photograph of ID e.g. Driving Licence or Passport." },{"label":"Student Card", "type":"image", "options":[], "placeholder":"", "help_text":"A photocopy or clear, cropped photograph of your current university student card." },{"label":"Proof of Enrolement", "type":"image", "options":[], "placeholder":"", "help_text":"University of Sheffield students can find it online at MUSE > My services > View all services > Certificate of Student Status.<br>Sheffield Hallam students can find it online at Blackboard > My Student Record > Confirmation of Enrolement.<br>A letter from either a university or UCAS confirming an offer or acceptance to study is not acceptable." }]',
		));

		Product::create(array(
			'name' => 'BUIHA Alumni Registration 2017/18',
			'description' => 'If you graduated from either The University of Sheffield or Sheffield Hallam University at the end of the previous academic year',
			'price' => 6000,
			'available' => true,
			'max_count' => 1,
			'category' => 'BUIHA Registration',
			'form' => '[{"label":"Telephone Number", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"Address Line 1", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"Address Line 2", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"Address Line 3", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"Postcode", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"Nationality", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"Date Of Birth", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"University Name", "type":"input", "options":[], "placeholder":"", "help_text":"e.g. Sheffield Hallam University or University of Sheffield" },{"label":"Course Title", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"Year Of Graduation", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"Student Number", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"Image Requirements", "type":"label", "options":[], "placeholder":"All images must be smaller than 500kb and either .jpg .png or .gif.", "help_text":"" },{"label":"Personal Identification", "type":"image", "options":[], "placeholder":"", "help_text":"A photocopy or clear cropped photograph of ID e.g. Driving Licence or Passport." },{"label":"Proof of Graduation", "type":"image", "options":[], "placeholder":"", "help_text":"A photocopy, screenshot or clear, cropped photograph of your graduation certificate or other proof of graduation." }]',
		));

		Product::create(array(
			'name' => 'BUIHA Staff Registration 2017/18',
			'description' => 'If you work for either The University of Sheffield or Sheffield Hallam University',
			'price' => 6000,
			'available' => true,
			'max_count' => 1,
			'category' => 'BUIHA Registration',
			'form' => '[{"label":"Telephone Number", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"Address Line 1", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"Address Line 2", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"Address Line 3", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"Postcode", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"Nationality", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"Date Of Birth", "type":"input", "options":[], "placeholder":"", "help_text":"" },{"label":"University Name", "type":"input", "options":[], "placeholder":"", "help_text":"e.g. Sheffield Hallam University or University of Sheffield" },{"label":"Image Requirements", "type":"label", "options":[], "placeholder":"All images must be smaller than 500kb and either .jpg .png or .gif.", "help_text":"" },{"label":"Staff Card", "type":"image", "options":[], "placeholder":"", "help_text":"A photocopy, screenshot or clear, cropped photograph of your staff card." },{"label":"Payslip", "type":"image", "options":[], "placeholder":"", "help_text":"A photocopy, screenshot or clear, cropped photograph of your most recent pay slip." }]',
		));

		Product::create(array(
			'name' => 'Give It A Go Ticket - 11th October',
			'description' => '',
			'price' => 750,
			'available' => true,
			'max_count' => 30,
			'category' => 'Tickets',
			'form' => '[{"label":"Information", "type":"label", "options":[], "placeholder":"The name and email address supplied below will be used to grant admission on the day of the event.", "help_text":"" },
			{"label":"Full Name", "type":"input", "options":[], "placeholder":"", "help_text":"" },
			{"label":"University", "type":"input", "options":[], "placeholder":"", "help_text":"" },
			{"label":"Email Address", "type":"input", "options":[], "placeholder":"", "help_text":"" },
			{"label":"Height", "type":"input", "options":[], "placeholder":"", "help_text":"Enter your approximate height in feet and inches" }]',
		));

		Product::create(array(
			'name' => 'Give It A Go Ticket - 13th October',
			'description' => '',
			'price' => 750,
			'available' => true,
			'max_count' => 30,
			'category' => 'Tickets',
			'form' => '[
			{"label":"Information", "type":"label", "options":[], "placeholder":"The name and email address supplied below will be used to grant admission on the day of the event.", "help_text":"" },
			{"label":"Full Name", "type":"input", "options":[], "placeholder":"", "help_text":"" },
			{"label":"University", "type":"input", "options":[], "placeholder":"", "help_text":"" },
			{"label":"Email Address", "type":"input", "options":[], "placeholder":"", "help_text":"" },
			{"label":"Height", "type":"input", "options":[], "placeholder":"", "help_text":"Enter your approximate height in feet and inches" }]',
		));

    	/*

		{label:"", type:"", "options":[], "placeholder":"", "help_text":"" }

			types
				input
				select
				image
				label
    	*/
    }
}
