const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.js('resources/assets/js/app.js', 'public/js').version();

mix.js('resources/assets/front/js/app.js', 'public/front/js').version();

mix.sass('resources/assets/front/sass/components.scss', 'public/front/css').version();
mix.sass('resources/assets/front/sass/content.scss', 'public/front/css').version();
mix.sass('resources/assets/front/sass/custom.scss', 'public/front/css').version();
mix.sass('resources/assets/front/sass/preloader.scss', 'public/front/css').version();
mix.sass('resources/assets/front/sass/style.scss', 'public/front/css').version();


mix.sass('resources/assets/app/sass/app.scss', 'public/app/css').version();

mix.options({
  processCssUrls: true,
  imgLoaderOptions: {
    enabled: false,
  }
});