<?php

use Illuminate\Http\Request;



Route::get('users/auth_data', array('uses' => 'UserController@auth_data'));
Route::get('products/shop', array('uses' => 'ProductController@shop'));
Route::get('groups/solo', 	array('uses' => 'GroupController@solo'));
Route::get('events/games', array('uses' => 'EventController@games'));
Route::get('gamejobs/my_jobs', array('uses' => 'GameJobController@my_jobs'));
Route::get('registrations/my_registration', array('uses' => 'RegistrationController@my_registration'));


Route::resource('users', 'UserController');
Route::resource('groups', 'GroupController');
Route::resource('roles', 'RoleController');
Route::resource('permissions', 'PermissionController');
Route::resource('articles', 'ArticleController');
Route::resource('articletypes', 'ArticleTypeController');
Route::resource('articlecomments', 'ArticleCommentController');
Route::resource('products', 'ProductController');
Route::resource('orders', 'OrderController');
Route::resource('registrations', 'RegistrationController');
Route::resource('configuration', 'ConfigurationController');

Route::resource('events', 'EventController');
Route::resource('teams', 'TeamController');
Route::resource('games', 'GameController');
Route::resource('gamejobs', 'GameJobController');
Route::resource('trainings', 'TrainingController');




/*
Route::resource('teams', 'TeamController');
Route::resource('games', 'GameController');
Route::resource('trainings', 'TrainingController');
Route::resource('events', 'EventController');
Route::put('registrations/manage', 		array('before' => 'permissions:registrations_manage','uses' => 'RegistrationController@manage'));
Route::post('register/processStudent', 	array('before' => 'permissions:register', 			'uses' => 'RegistrationController@storeStudent'));
Route::post('register/processStaff', 	array('before' => 'permissions:register', 			'uses' => 'RegistrationController@storeStaff'));
Route::post('register/processAlumni', 	array('before' => 'permissions:register', 			'uses' => 'RegistrationController@storeAlumni'));
Route::put('settings/edit', 			array('before' => 'permissions:settings_edit', 			'uses' => 'SettingController@update'));
Route::put('configuration/edit', 		array('before' => 'permissions:configuration_edit', 		'uses' => 'ConfigurationController@update'));
*/