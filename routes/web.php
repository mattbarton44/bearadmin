<?php

use Illuminate\Http\Request;


Route::group(['prefix' => 'dashboard'], function ()
{
	Route::get('/cup_tables_data',  array('uses' => 'HomeController@getBuihaCupTables'));
	Route::get('/featured_news',  array('uses' => 'HomeController@getFeaturedNews'));
	Route::get('/latest_news',  array('uses' => 'HomeController@getLatestNews'));
	Route::get('/all_news',  array('uses' => 'HomeController@getAllNews'));
	Route::get('/news_article/{id}',  array('uses' => 'HomeController@getNewsArticle'));

	Route::get('test', function () { return App\Helpers\Bears::storeScheduleInfo(); });
});


Auth::routes();

Route::get('/', function () { return view('front/content/index'); });
Route::get('news/{id}', function () { return view('front/content/news_article'); });
Route::get('news', function () { return view('front/content/news'); });
Route::get('unsupported', function () { return view('front/content/unsupported'); });

//Route::get('fixtures', function () { return view('front/content/fixtures'); });
//Route::get('statistics', function () { return view('front/content/statistics'); });


// TEMPORARY ROUTES 	
/*
Route::get('giveitago', function () { return view('exposed_product'); });
Route::get('exposed_product_data', function () { 
	$data = [];
	$data["first"] = App\Product::where('name', '=', 'Give It A Go Ticket - 11th October')->first();
	$data["first"]["tickets_left"] = (25 - App\Order::where('product_id', '=', $data["first"]->id)->count());
	$data["second"] = App\Product::where('name', '=', 'Give It A Go Ticket - 13th October')->first();
	$data["second"]["tickets_left"] = (25 - App\Order::where('product_id', '=', $data["second"]->id)->count());
	return $data;
});
*/

Route::post('exposed_product', function (Request $request) {App\Helpers\Bears::processExposedProductOrder($request->all());});


Route::get('alumni', function () { 	return view('alumni'); });
Route::get('alumni_product', function () { 	
	$data = App\Product::withCount('orders')->where('name', '=', 'Alumni Tournament 2019 Ticket')->first();
	return $data;
});



Route::group(['prefix' => 'club', 'middleware' => 'auth' ], function ()
{
	/* DASHBOARD */	Route::get('/', 			function ()			{ return view('club/content/index'); 									});

	Route::group(['prefix' => 'news'], function () {				/* === ARTICLES === */
	/* SHOW */		Route::get('/{id}', 		function ($id) 		{ return view('club/content/articles/show')->with('id',$id); 			})->middleware('perm:articles_show');
	/* INDEX */		Route::get('/', 			function () 		{ return view('club.content/articles.index'); 							})->middleware('perm:articles');
	});

	Route::group(['prefix' => 'teams'], function () {				/* === TEAMS === */
	/* SHOW */		Route::get('/{id}', 		function ($id) 		{ return view('club/content/teams/show')->with('id',$id); 				})->middleware('perm:teams_show');
	/* INDEX */		Route::get('/', 			function () 		{ return view('club/content/teams/index'); 								})->middleware('perm:teams');
	});

	Route::group(['prefix' => 'games'], function () {				/* === GAMES === */	
	/* SHOW */		Route::get('/{id}', 		function ($id) 		{ return view('club/content/games/show')->with('id',$id); 				})->middleware('perm:games_show');
	/* INDEX */		Route::get('/', 			function () 		{ return view('club/content/games/index'); 								})->middleware('perm:games');
	});

	Route::group(['prefix' => 'trainings'], function () {			/* === TRAININGS === */	
	/* SHOW */		Route::get('/{id}', 		function ($id) 		{ return view('club/content/trainings/show')->with('id',$id); 			})->middleware('perm:trainings_show');
	/* INDEX */		Route::get('/', 			function () 		{ return view('club/content/trainings/index'); 							})->middleware('perm:trainings');
	});

	Route::group(['prefix' => 'events'], function () {				/* === EVENTS === */
	/* SHOW */		Route::get('/{id}', 		function ($id) 		{ return view('club/content/events/show')->with('id',$id); 				})->middleware('perm:events_show');
	/* INDEX */		Route::get('/', 			function () 		{ return view('club/content/events/index'); 							})->middleware('perm:events');
	});

	Route::group(['prefix' => 'registrations'], function () {		/* === REGISTRATIONS === */
	/* SHOW */		Route::get('/{id}', 		function ($id) 		{ return view('club/content/registrations/show')->with('id',$id); 	})->middleware('perm:registrations_admin');
	/* INDEX */		Route::get('/', 			function () 		{ return view('club/content/registrations/index'); 					})->middleware('perm:registrations');
	});

	Route::group(['prefix' => 'shop'], function () {				/* === SHOP === */
	/* SHOW */		Route::get('/{id}', 		function ($id) 		{ return view('club/content/shop/show')->with('id',$id); 			})->middleware('perm:member');
	/* INDEX */		Route::get('/', 			function () 		{ return view('club/content/shop/index'); 							})->middleware('perm:member');
	});

	Route::group(['prefix' => 'products'], function () {			/* === PRODUCTS === */
	/* SHOW */		Route::get('/{id}', 		function ($id) 		{ return view('club/content/products/show')->with('id',$id); 			})->middleware('perm:shop_admin');
	/* INDEX */		Route::get('/', 			function () 		{ return view('club/content/products/index'); 							})->middleware('perm:shop_admin');
	});

	Route::group(['prefix' => 'orders'], function () {				/* === ORDERS === */
	/* SHOW */		Route::get('/{id}', 		function ($id) 		{ return view('club/content/orders/show')->with('id',$id); 			})->middleware('perm:shop_admin');
	/* INDEX */		Route::get('/', 			function () 		{ return view('club/content/orders/index'); 						})->middleware('perm:shop_admin');
	});

	Route::group(['prefix' => 'settings'], function () {			/* === SETTINGS === */
					Route::get('/facebook/auth', 			'SettingController@redirectToProvider');
					Route::get('/facebook/auth/callback', 	'SettingController@handleProviderCallback');
	/* EDIT */		Route::get('/', 			function () 		{ return view('club/content/settings/edit'); 							})->middleware('perm:settings');
	});

	Route::group(['prefix' => 'configuration'], function () {		/* === CONFIGURATION === */
	/* EDIT */		Route::get('/', 			function () 		{ return view('club/content/configuration/edit'); 						})->middleware('perm:configuration');
	});
	
	Route::group(['prefix' => 'users'], function () {				/* === USERS === */	
	/* SHOW */		Route::get('/{id}', 		function ($id) 		{ return view('club/content/users/show')->with('id',$id); 				})->middleware('perm:users_show');
	/* INDEX */		Route::get('/', 			function () 		{ return view('club/content/users/index'); 								})->middleware('perm:users');
	});

	Route::group(['prefix' => 'groups'], function () {				/* === GROUPS === */	
	/* SHOW */		Route::get('/{id}', 		function ($id) 		{ return view('club/content/groups/show')->with('id',$id); 				})->middleware('perm:groups_show');
	/* INDEX */		Route::get('/', 			function () 		{ return view('club/content/groups/index'); 							})->middleware('perm:groups');
	});

	Route::group(['prefix' => 'roles'], function () {				/* === ROLES === */	
	/* SHOW */		Route::get('/{id}', 		function ($id) 		{ return view('club/content/roles/show')->with('id',$id); 				})->middleware('perm:roles_show');
	/* INDEX */		Route::get('/', 			function () 		{ return view('club/content/roles/index'); 								})->middleware('perm:roles');
	});

	Route::group(['prefix' => 'permissions'], function () {			/* === PERMISSIONS === */
	/* INDEX */		Route::get('/', 			function () 		{ return view('club/content/permissions/index'); 						})->middleware('perm:permissions');
	});

	// CATCH OTHER
	Route::any('{all}', function()
	{
		return Redirect::to('club/');
	})->where('all', '.*');
});
Route::any('{all}', function()
{
	return Redirect::to('/');
})->where('all', '.*');





