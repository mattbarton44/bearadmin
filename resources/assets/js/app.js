
import DatatableFactory from './components/vuejs-datatable/index.js';
import _ from 'lodash';
import moment from 'moment';
import FullCalendar from "vue-full-calendar";
import Datepicker from "vuejs-datepicker";

const VueInputMask = require('vue-inputmask').default
import "fullcalendar/dist/fullcalendar.min.css";

Vue.component('passport-clients', require('./components/passport/Clients.vue'));
Vue.component('passport-authorized-clients', require('./components/passport/AuthorizedClients.vue'));
Vue.component('passport-personal-access-tokens', require('./components/passport/PersonalAccessTokens.vue'));
Vue.component('datepicker', Datepicker);

Vue.component('featurednews', require('./components/widgets/featurednews.vue'));
Vue.component('latestnews', require('./components/widgets/latestnews.vue'));
Vue.component('leaguetable', require('./components/widgets/leaguetable.vue'));
Vue.component('matchreport', require('./components/widgets/matchreport.vue'));
Vue.component('twitter', require('./components/widgets/twitter.vue'));
Vue.component('upcomingfixture', require('./components/widgets/upcomingfixture.vue'));

Vue.use(DatatableFactory);
Vue.use(FullCalendar);
Vue.use(VueInputMask)


Vue.prototype.moment = moment;

Vue.component('summernote', {
	template: '<textarea :name="name"></textarea>',
    props: {
        model: {
            required: true,
        },
        name: {
            type: String,
            required: true,
        },
        height: {
            type: String,
            default: '150'
        }
    },
    methods: {
        run (code, value) {
            if (typeof value === undefined) {
                $(this.$el).summernote(code);
            } else {
                $(this.$el).summernote(code, value);
            }
        }
    },
    mounted() {
        let config = {
            height: this.height
        };
        let vm = this;
        config.callbacks = {
            onInit: function () {
                $(vm.$el).summernote("code", vm.model);
            },
            onChange: function () {
                vm.$emit('change', $(vm.$el).summernote('code'));
            },
            onBlur: function () {
                vm.$emit('change', $(vm.$el).summernote('code'));
            }
        };
        $(this.$el).summernote(config);
    },
})

