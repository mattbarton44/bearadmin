
<script>
window.Laravel =  <?php echo json_encode([
    'csrfToken' => csrf_token(),
]); ?>

var BearAdmin = {
	csrfToken: "{{ csrf_token() }}",
	stripeKey: "{{ config('services.stripe.key') }}",
}


function check() 
{
    "use strict";

    try { eval("var foo = (x)=>x+1"); }
    catch (e) { return false; }
    return true;
}

if (check() == false) 
{
    window.location.href = window.location.origin + "/unsupported";
}



</script>

<script src="{{ mix('/front/js/app.js') }}"></script>
<script src="{{ mix('/js/app.js') }}"></script>

<!-- Mainly scripts -->
<script src="/app/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="/app/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="/app/js/inspinia.js"></script>
<script src="/app/js/plugins/pace/pace.min.js"></script>

<script src="/app/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="/app/plugins/sweetalert/dist/sweetalert.min.js"></script>

<script src="/app/js/plugins/vuejs-datatable/vuejs-datatable.js"></script>
