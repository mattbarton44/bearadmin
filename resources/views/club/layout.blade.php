<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Bears - @yield('title') </title>
        @include('club.scripts_css')
    </head>
    <body class="md-skin fixed-nav">
        <div id="wrapper">
            @include('club.navigation')
            <div id="page-wrapper" class="gray-bg">
                @include('club.header')
                <div class="wrapper wrapper-content animated fadeInRight">
                    @yield('content')
                </div>
                @include('club.footer')
            </div>
        </div>
        @include('club.scripts_js')
        @yield('scripts')
    </body>
</html>
