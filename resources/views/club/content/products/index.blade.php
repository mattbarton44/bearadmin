@extends('club.layout')

@section('title', 'Products')

@section('content')
<div class="m-b-lg row" id="contentDiv" v-cloak v-show="initialised">
	<div class="col-lg-12">

        <div class="ibox float-e-margins">
            <div class="ibox-content">
				<div class="row">
					<div class="col-xs-8">
		                <h2>Products</h2>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-success btn-circle btn-lg pull-right m-l" type="button" @click="openCreateBox"><i class="fa fa-plus"></i></button>
		                <button class="btn btn-default btn-circle btn-lg pull-right" type="button" @click="toggleFilters"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>


        <div class="ibox float-e-margins" v-if="createBoxOpen == true">
            <div class="ibox-content">
				<div class="row m-b-lg">
					<div class="col-xs-8">
		                <h2>Create Product</h2>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-danger btn-circle btn-lg pull-right" type="button" @click="closeCreateBox"><i class="fa fa-times"></i></button>
                    </div>
                </div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
	                    	<label>Name</label>
	                    	<input type="text" placeholder="" class="form-control" v-model="createData.name">
	                    </div>
                    </div>
                </div>
				<div class="row m-b">
					<div class="col-md-12">
	                    <label>Description</label>
	                    <input type="text" placeholder="" class="form-control" v-model="createData.description">
                    </div>
                </div>
                <div class="row m-b">
					<div class="col-md-6">
						<div class="form-group">
	                    	<label>Status</label>
							<select v-model="createData.available" class="form-control">
								<option :value="true">Available</option>
								<option :value="false">Unavailable</option>
							</select>
	                    </div>
                    </div>
					<div class="col-md-6">
						<div class="form-group">
	                    	<label>Type</label>
							<select v-model="createData.category" class="form-control">
								<option :value="'BUIHA Registration'">BUIHA Registration</option>
								<option :value="'Ice Fees'">Ice Fees</option>
								<option :value="'Kit'">Kit</option>
								<option :value="'Tickets'">Tickets</option>
								<option :value="'Other'">Other</option>
							</select>
	                    </div>
                    </div>
                </div>
                <div class="row m-b">
					<div class="col-md-6">
						<div class="form-group">
	                    	<label>Price (in pence)</label>
	                    	<input type="text" placeholder="" class="form-control" v-model="createData.price">
	                    </div>
                    </div>
					<div class="col-md-6">
						<div class="form-group">
	                    	<label>Max orders per user</label>
	                    	<input type="text" placeholder="" class="form-control" v-model="createData.max_count">
	                    </div>
                    </div>
                </div>


                <div class="row m-b">
					<div class="col-md-12">
						<div class="form-group">
	                    	<label>Form</label>
	                    	<button class="btn btn-default form-control" @click='createFormAddElement'>Add form element</button>
	                    </div>
                    </div>
                </div>

                <template v-for="(row, index) in createData.form">
					<div class="ibox">
				        <div class="ibox-content">
					        <div class="row m-b">
								<div class="col-xs-8">
					                <h2>Form element @{{index}}</h2>
			                    </div>
								<div class="col-xs-4">
					                <button class="btn btn-danger btn-circle btn-lg pull-right" type="button" @click="createFormRemoveElement(index)"><i class="fa fa-times"></i></button>
			                    </div>
			                </div>
			                <div class="row m-b">
								<div class="col-md-3">
									<div class="form-group">
				                    	<label>Type</label>
										<select v-model="row.type" class="form-control">
											<option :value="'input'">Input</option>
											<option :value="'select'">Select</option>
											<option :value="'image'">Image</option>
											<option :value="'label'">Label</option>
										</select>
				                    </div>
			                    </div>
								<div class="col-md-3">
									<div class="form-group">
				                    	<label>Label</label>
				                    	<input type="text" placeholder="" class="form-control" v-model="row.label">
				                    </div>
			                    </div>
								<div class="col-md-3" v-if="row.type == 'select'">
									<div class="form-group">
				                    	<label>Options</label>
				                    	<input type="text" placeholder="List options seperated by '/'" class="form-control" v-model="row.options">
				                    </div>
			                    </div>
								<div class="col-md-3" v-if="row.type == 'input' || row.type == 'label'">
									<div class="form-group">
				                    	<label>Placeholder</label>
				                    	<input type="text" placeholder="" class="form-control" v-model="row.placeholder">
				                    </div>
			                    </div>
								<div class="col-md-3">
									<div class="form-group">
				                    	<label>Help Text</label>
				                    	<input type="text" placeholder="" class="form-control" v-model="row.help_text">
				                    </div>
			                    </div>
			                </div>

			            </div>
	                </div>
                </template>

				<div class="row m-b" v-show="createValidationMessage != ''">
					<div class="col-xs-12">
	                	<div class="form-group">
	                    	<h3 class="text-danger text-center">@{{createValidationMessage}}</h3>
	                    </div>
                    </div>
                </div>

				<div class="row m-t-lg">
					<div class="col-xs-12">
                        <button class="btn btn-block btn-primary" @click="submitCreate">Submit</button>
                    </div>
                </div>
            </div>
        </div>




        <div class="ibox float-e-margins" v-if="editBoxOpen == true">
            <div class="ibox-content">
				<div class="row m-b-lg">
					<div class="col-xs-8">
		                <h2>Edit Product @{{ editData.id }}</h2>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-danger btn-circle btn-lg pull-right" type="button" @click="closeEditBox"><i class="fa fa-times"></i></button>
                    </div>
                </div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
	                    	<label>Name</label>
	                    	<input type="text" placeholder="" class="form-control" v-model="editData.name">
	                    </div>
                    </div>
                </div>
				<div class="row m-b">
					<div class="col-md-12">
	                    <label>Description</label>
	                    <input type="text" placeholder="" class="form-control" v-model="editData.description">
                    </div>
                </div>
                <div class="row m-b">
					<div class="col-md-6">
						<div class="form-group">
	                    	<label>Status</label>
							<select v-model="editData.available" class="form-control">
								<option :value="true">Available</option>
								<option :value="false">Unavailable</option>
							</select>
	                    </div>
                    </div>
					<div class="col-md-6">
						<div class="form-group">
	                    	<label>Type</label>
							<select v-model="editData.category" class="form-control">
								<option :value="'BUIHA Registration'">BUIHA Registration</option>
								<option :value="'Ice Fees'">Ice Fees</option>
								<option :value="'Kit'">Kit</option>
								<option :value="'Tickets'">Tickets</option>
								<option :value="'Other'">Other</option>
							</select>
	                    </div>
                    </div>
                </div>
                <div class="row m-b">
					<div class="col-md-6">
						<div class="form-group">
	                    	<label>Price (in pence)</label>
	                    	<input type="text" placeholder="" class="form-control" v-model="editData.price">
	                    </div>
                    </div>
					<div class="col-md-6">
						<div class="form-group">
	                    	<label>Max orders per user</label>
	                    	<input type="text" placeholder="" class="form-control" v-model="editData.max_count">
	                    </div>
                    </div>
                </div>


                <div class="row m-b">
					<div class="col-md-12">
						<div class="form-group">
	                    	<label>Form</label>
	                    	<button class="btn btn-default form-control" @click='editFormAddElement'>Add form element</button>
	                    </div>
                    </div>
                </div>

                <template v-for="(row, index) in editData.form">
					<div class="ibox">
				        <div class="ibox-content">
					        <div class="row m-b">
								<div class="col-xs-8">
					                <h2>Form element @{{index}}</h2>
			                    </div>
								<div class="col-xs-4">
					                <button class="btn btn-danger btn-circle btn-lg pull-right" type="button" @click="editFormRemoveElement(index)"><i class="fa fa-times"></i></button>
			                    </div>
			                </div>
			                <div class="row m-b">
								<div class="col-md-3">
									<div class="form-group">
				                    	<label>Type</label>
										<select v-model="row.type" class="form-control">
											<option :value="'input'">Input</option>
											<option :value="'select'">Select</option>
											<option :value="'image'">Image</option>
											<option :value="'label'">Label</option>
										</select>
				                    </div>
			                    </div>
								<div class="col-md-3">
									<div class="form-group">
				                    	<label>Label</label>
				                    	<input type="text" placeholder="" class="form-control" v-model="row.label">
				                    </div>
			                    </div>
								<div class="col-md-3" v-if="row.type == 'select'">
									<div class="form-group">
				                    	<label>Options</label>
				                    	<input type="text" placeholder="List options seperated by '/'" class="form-control" v-model="row.options">
				                    </div>
			                    </div>
								<div class="col-md-3" v-if="row.type == 'input' || row.type == 'label'">
									<div class="form-group">
				                    	<label>Placeholder</label>
				                    	<input type="text" placeholder="" class="form-control" v-model="row.placeholder">
				                    </div>
			                    </div>
								<div class="col-md-3">
									<div class="form-group">
				                    	<label>Help Text</label>
				                    	<input type="text" placeholder="" class="form-control" v-model="row.help_text">
				                    </div>
			                    </div>
			                </div>

			            </div>
	                </div>
                </template>

				<div class="row m-b" v-show="editValidationMessage != ''">
					<div class="col-xs-12">
	                	<div class="form-group">
	                    	<h3 class="text-danger text-center">@{{editValidationMessage}}</h3>
	                    </div>
                    </div>
                </div>

				<div class="row m-t-lg">
					<div class="col-xs-12">
                        <button class="btn btn-block btn-primary" @click="submitEdit">Submit</button>
                    </div>
                </div>
            </div>
        </div>












        <div class="ibox float-e-margins" v-show="filterBoxOpen">
            <div class="ibox-content">
				<div class="row m-b">
					<div class="col-xs-8">
		                <h2>Filter Products</h2>
		                <p class="text-danger">Viewing <b>@{{ filteredProducts.length }}</b> products out of <b>@{{ products.length }}.</b></p>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-primary btn-circle btn-lg pull-right m-l" type="button" @click="applyFilters"><i class="fa fa-check"></i></button>
		                <button class="btn btn-default btn-circle btn-lg pull-right" type="button" @click="resetFilters"><i class="fa fa-refresh"></i></button>
                    </div>
                </div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label>Name</label>
							<input type="text" placeholder="" class="form-control" v-model="filterData.name">
						</div>
					</div>
					<div class="col-md-4">
	                    <div class="form-group">
	                    	<label>Category</label>
	                    	<select v-model="filterData.category" class="form-control">
								<option :value="'all'"></option>
								<option :value="'BUIHA Registration'">BUIHA Registration</option>
								<option :value="'Ice Fees'">Ice Fees</option>
								<option :value="'Kit'">Kit</option>
								<option :value="'Tickets'">Tickets</option>
								<option :value="'Other'">Other</option>
							</select>
	                    </div>
					</div>
					<div class="col-md-4">
	                    <div class="form-group">
	                    	<label>Available</label>
	                    	<select v-model="filterData.available" class="form-control">
								<option :value="'all'"></option>
								<option :value="true">Yes</option>
								<option :value="false">No</option>
							</select>
	                    </div>
					</div>
				</div>
            </div>
        </div>

        <div class="ibox" v-if="filteredProducts.length > 0">
            <div class="ibox-content">
                <div class="project-list">
                    <table class="table table-hover">
                    	<thead>
                    		<th>Details</th>
                    		<th>Price</th>
                    		<th>Category</th>
                    		<th>Max orders per user</th>
                    		<th>Total number of orders</th>
                    		<th>Status</th>
                    		<th>Actions</th>
                    	</thead>
                        <tbody>
                        <tr v-for="product in filteredProducts">
                            <td class="project-title">
                                <a href="">@{{product.name}}</a>
                                <br/>
                                <small>@{{product.description}}</small>
                            </td>
                            <td class="project-status">
                                <span class="">£@{{ (product.price/100).toFixed(2) }}</span>
                            </td>
                            <td class="project-status">
                                <span class="">@{{ product.category }}</span>
                            </td>
                            <td class="project-status">
                                <span class="">@{{ product.max_count }}</span>
                            </td>
                            <td class="project-status">
                                <span class="">@{{ product.orders.length }}</span>
                            </td>
                            <td class="project-status">
                            	<template v-if="product.available == true">
                                	<span class="label label-primary">Available</span>
                                </template>
                                <template v-if="product.available == false">
                                	<span class="label label-default">Unavailable</span>
                                </template>
                            </td>
                            <td class="">
                                <a :href="'products/' + product.id" class="btn btn-white btn-sm"><i class="fa fa-folder"></i> View </a>
                                <a @click="openEditBox(product)" class="btn btn-white btn-sm"><i class="fa fa-pencil"></i> Edit </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>


</div>

<style>

</style>

@endsection


@section('scripts')

<script>

	const contentApp = new Vue ({
		el: '#contentDiv',
		data: {
			init: { 
				products: false,
			},
			products:[],
			filteredProducts:[],
			filterBoxOpen:false,
			filterData:{},
			filterMessage:"",
			editData:{},
			editValidationMessage:'',
			editBoxOpen: false,
			createData:{},
			createValidationMessage:'',
			createBoxOpen: false,
		},
		computed: {
			initialised: function() {
				return this.init.products;
			}
		},
		methods: {
	        loadProducts() {
	            let app = this;
	            axios.get('/api/products')
	            .then(function (response) {
	                app.products = response.data;
	                app.applyFilters();
	                app.init.products = true;
	            }).catch(function (error) { showSwalError("Error loading products",error) });
	        },
	        toggleFilters(){
				this.filterBoxOpen = !this.filterBoxOpen;
				this.filterMessage = "";
	        },
	        resetFilters(){
	        	this.filterData = {	
	        		name:"",
					category:"all",
					available:"all",
				}		
				this.filterMessage = ""; 	
	        },
	        applyFilters(){
	        	let fd = this.filterData;

				this.filteredProducts = [];
				for(var i=0; i<this.products.length; i++)
				{
					let product = this.products[i];
					if( product.name.toUpperCase().indexOf(fd.name.toUpperCase()) > -1 
						&& (fd.category == "all" || product.category == fd.category) 
						&& (fd.available == "all" || product.available == fd.available))
					{
						this.filteredProducts.push(product);
					}
				}
	        },
	        openCreateBox(){
	        	this.createData = {
	        		name:'',
	        		description:'',
	        		price:null,
	        		available:true,
	        		max_count:null,
	        		category:'',
	        		form:[]
	        	};
				this.createBoxOpen = true;
	        },
	        closeCreateBox(){
				this.createBoxOpen = false;
				this.resetCreateData();
	        },
	        resetCreateData(){
	        	this.createData = {};
	        	this.createValidationMessage = "";				
	        },
	        createFormIsValid(){
	        	if(this.createData.name == "")
	        	{
	        		this.createValidationMessage = "Please enter a product name";
	        		return false;
	        	}

	        	if(this.createData.description == "")
	        	{
	        		this.createValidationMessage = "Please enter a product description";
	        		return false;
	        	}

	        	if(this.createData.max_count == "")
	        	{
	        		this.createValidationMessage = "Please enter a description";
	        		return false;
	        	}

	        	if(this.createData.category == "")
	        	{
	        		this.createValidationMessage = "Please enter a category";
	        		return false;
	        	}

	        	if(parseInt(this.createData.price) < 100)
	        	{
	        		this.createValidationMessage = "Please enter a price greater than 100";
	        		return false;
	        	}

	        	if(this.createData.max_count == null)
	        	{
	        		this.createValidationMessage = "Please enter a maximum orders per user";
	        		return false;
	        	}

	        	this.createValidationMessage = "";
	        	return true;
	        },
	        submitCreate(){		
				if(this.createFormIsValid() == true)
				{
		        	let app = this;
		            axios.post('/api/products', app.createData)
		            .then(function (response) {
		                app.resetCreateData();
						app.loadProducts();
						app.closeCreateBox();
		                showSwalSuccess("Success","The product has been created");
		            }).catch(function (error) { showSwalError("Error creating product",error) });
		        }
	        },
	        createFormAddElement(){
	        	let data = {
	        		label:'',
	        		type:'input',
	        		options:'',
	        		placeholder:'',
	        		help_text:'',
	        	}
	        	this.createData.form.push(data);
	        },
	        createFormRemoveElement(index){
	        	this.createData.form.splice(index, 1);
	        },


	        openEditBox(product){
	        	this.editData = product;
	        	this.editData.available = Boolean(product.available);
	        	this.editData.form = JSON.parse(product.form);
				this.editBoxOpen = true;
	        },
	        closeEditBox(){
				this.editBoxOpen = false;
				this.resetEditData();
	        },
	        resetEditData(){
	        	this.editData = {};
	        	this.editValidationMessage = "";				
	        },
	        editFormIsValid(){
	        	if(this.editData.name == "")
	        	{
	        		this.editValidationMessage = "Please enter a product name";
	        		return false;
	        	}

	        	if(this.editData.description == "")
	        	{
	        		this.editValidationMessage = "Please enter a product description";
	        		return false;
	        	}

	        	if(this.editData.max_count == "")
	        	{
	        		this.editValidationMessage = "Please enter a description";
	        		return false;
	        	}

	        	if(this.editData.category == "")
	        	{
	        		this.editValidationMessage = "Please enter a category";
	        		return false;
	        	}

	        	if(parseInt(this.editData.price) < 100)
	        	{
	        		this.editValidationMessage = "Please enter a price greater than 100";
	        		return false;
	        	}

	        	if(this.editData.max_count == null)
	        	{
	        		this.editValidationMessage = "Please enter a maximum orders per user";
	        		return false;
	        	}

	        	this.editValidationMessage = "";
	        	return true;
	        },
	        submitEdit(){		
				if(this.editFormIsValid() == true)
				{
		        	let app = this;
		            axios.put('/api/products/'+app.editData.id, app.editData)
		            .then(function (response) {
		                app.resetEditData();
						app.loadProducts();
						app.closeEditBox();
		                showSwalSuccess("Success","The product has been updated");
		            }).catch(function (error) { showSwalError("Error updating product",error) });
		        }
	        },
	        editFormAddElement(){
	        	let data = {
	        		label:'',
	        		type:'input',
	        		options:'',
	        		placeholder:'',
	        		help_text:'',
	        	}
	        	this.editData.form.push(data);
	        },
	        editFormRemoveElement(index){
	        	this.editData.form.splice(index, 1);
	        },


		},
		created: function () {
			this.resetFilters();
			this.loadProducts();
		},
	});

	showSwalError = function(title,text){
		console.log(text)
        swal({ title: title, text: text, type: "error", showCancelButton: false, allowEscapeKey: false });
	}

	showSwalSuccess = function(title,text){
        console.log(text);
        swal({ title: title, text: text, type: "success", showCancelButton: false, allowEscapeKey: false });
	}


</script>
@endsection