@extends('club.layout')

@section('title', 'Settings')

@section('content')
<div class="row" id="contentDiv">
	<div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <a class="btn btn-block btn-social btn-facebook" href="/club/settings/facebook/auth"><span class="fa fa-facebook"></span> Link account to Facebook</a>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content profile-content">
				<div class="row m-b">
                </div>
            </div>
        </div>
    </div>
</div>

<link href="/app/css/plugins/bootstrapSocial/bootstrap-social.css" rel="stylesheet">

@endsection


@section('scripts')
<script>

</script>
@endsection