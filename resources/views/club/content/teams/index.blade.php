@extends('club.layout')

@section('title', 'Teams')

@section('content')
<div class="m-b-lg row" id="contentDiv" v-cloak v-show="initialised">
	<div class="col-lg-12">

        <div class="ibox float-e-margins">
            <div class="ibox-content">
				<div class="row">
					<div class="col-xs-8">
		                <h2>Teams</h2>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-success btn-circle btn-lg pull-right m-l" type="button" @click="openCreateBox"><i class="fa fa-plus"></i></button>
		            </div>
                </div>
            </div>
        </div>


        <div class="ibox float-e-margins" v-if="createBoxOpen == true">
            <div class="ibox-content">
				<div class="row m-b-lg">
					<div class="col-xs-8">
		                <h2>Create Team</h2>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-danger btn-circle btn-lg pull-right" type="button" @click="closeCreateBox"><i class="fa fa-times"></i></button>
                    </div>
                </div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
	                    	<label>Name</label>
	                    	<input type="text" placeholder="" class="form-control" v-model="createData.name">
	                    </div>
                    </div>
                </div>
                <div class="row m-b">
					<div class="col-md-12">
						<div class="form-group">
	                    	<label>Group</label>
							<select v-model="createData.group_id" class="form-control">
								<option v-for="group in groups" :value="group.id">@{{group.name}}</option>
							</select>
	                    </div>
                    </div>
                </div>

				<div class="row m-b" v-show="createValidationMessage != ''">
					<div class="col-xs-12">
	                	<div class="form-group">
	                    	<h3 class="text-danger text-center">@{{createValidationMessage}}</h3>
	                    </div>
                    </div>
                </div>

				<div class="row m-t-lg">
					<div class="col-xs-12">
                        <button class="btn btn-block btn-primary" @click="submitCreate">Submit</button>
                    </div>
                </div>
            </div>
        </div>


        <div class="ibox float-e-margins" v-if="editBoxOpen == true">
            <div class="ibox-content">
				<div class="row m-b-lg">
					<div class="col-xs-8">
		                <h2>Edit Team @{{ editData.id }}</h2>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-danger btn-circle btn-lg pull-right" type="button" @click="closeEditBox"><i class="fa fa-times"></i></button>
                    </div>
                </div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
	                    	<label>Name</label>
	                    	<input type="text" placeholder="" class="form-control" v-model="editData.name">
	                    </div>
                    </div>
                </div>
                <div class="row m-b">
					<div class="col-md-12">
						<div class="form-group">
	                    	<label>Group</label>
							<select v-model="editData.group_id" class="form-control">
								<option v-for="group in groups" :value="group.id">@{{group.name}}</option>
							</select>
	                    </div>
                    </div>
                </div>

				<div class="row m-b" v-show="editValidationMessage != ''">
					<div class="col-xs-12">
	                	<div class="form-group">
	                    	<h3 class="text-danger text-center">@{{editValidationMessage}}</h3>
	                    </div>
                    </div>
                </div>

				<div class="row m-t-lg">
					<div class="col-xs-12">
                        <button class="btn btn-block btn-primary" @click="submitEdit">Submit</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="ibox" v-if="teams.length > 0">
            <div class="ibox-content">
                <div class="project-list">
                    <table class="table table-hover">
                    	<thead>
                    		<th>Team Name</th>
                    		<th>Group Name</th>
                    		<th># Of Members</th>
                    		<th>Actions</th>
                    	</thead>
                        <tbody>
                        <tr v-for="team in teams">
                            <td class="project-status">
                                <span class="">@{{team.name}}</span>
                            </td>
                            <td class="project-status">
                                <span class="">@{{team.group.name}}</span>
                            </td>
                            <td class="project-status">
                                <span class="">@{{team.group.users.length}}</span>
                            </td>
                            <td class="">
                                <a :href="'teams/' + team.id" class="btn btn-white btn-sm"><i class="fa fa-folder"></i> View </a>
                                <a @click="openEditBox(team)" class="btn btn-white btn-sm"><i class="fa fa-pencil"></i> Edit </a>
                                <a @click="setActive(team)" data-toggle="modal" data-target="#deleteTeamModal" class="btn btn-white btn-sm"><i class="fa fa-times"></i> Delete </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <div class="modal inmodal" id="deleteTeamModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
        	<div class="modal-content animated bounceInTop">
                <div class="modal-header">
                    <h4 class="modal-title">Delete Team</h4>
                </div>
                <div class="modal-body" style="text-align:center">
                     <p>You are deleting the team <strong>@{{ activeTeam.name }} </strong>.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" @click="submitDelete()">Submit</button>
                </div>
            </div>
        </div>
    </div>


</div>

<style>

</style>

@endsection


@section('scripts')

<script>

	const contentApp = new Vue ({
		el: '#contentDiv',
		data: {
			init: { 
				teams: false,
				groups: false,
			},
			teams:[],
			groups:[],
			filteredProducts:[],
			filterBoxOpen:false,
			filterData:{},
			filterMessage:"",
			editData:{},
			editValidationMessage:'',
			editBoxOpen: false,
			createData:{},
			createValidationMessage:'',
			createBoxOpen: false,
			activeTeam: {}
		},
		computed: {
			initialised: function() {
				return this.init.teams && this.init.groups;
			}
		},
		methods: {
	        loadTeams() {
	            let app = this;
	            axios.get('/api/teams')
	            .then(function (response) {
	                app.teams = response.data;
	                app.init.teams = true;
	            }).catch(function (error) { showSwalError("Error loading teams",error) });
	        },
	        loadGroups() {
	            let app = this;
	            axios.get('/api/groups')
	            .then(function (response) {
	                app.groups = response.data;
	                app.init.groups = true;
	            }).catch(function (error) { showSwalError("Error loading groups",error) });
	        },
	        openCreateBox(){
	        	this.createData = {
	        		name:'',
	        		group_id:null,
	        	};
				this.createBoxOpen = true;
	        },
	        closeCreateBox(){
				this.createBoxOpen = false;
				this.resetCreateData();
	        },
	        resetCreateData(){
	        	this.createData = {};
	        	this.createValidationMessage = "";				
	        },
	        createFormIsValid(){
	        	if(this.createData.name == "")
	        	{
	        		this.createValidationMessage = "Please enter a team name";
	        		return false;
	        	}

	        	if(this.createData.group_id == null)
	        	{
	        		this.createValidationMessage = "Please select a group";
	        		return false;
	        	}

	        	this.createValidationMessage = "";
	        	return true;
	        },
	        submitCreate(){		
				if(this.createFormIsValid() == true)
				{
		        	let app = this;
		            axios.post('/api/teams', app.createData)
		            .then(function (response) {
		                app.resetCreateData();
						app.loadTeams();
						app.closeCreateBox();
		                showSwalSuccess("Success","The team has been created");
		            }).catch(function (error) { showSwalError("Error creating team",error) });
		        }
	        },
	        openEditBox(team){
	        	this.editData = team;
				this.editBoxOpen = true;
	        },
	        closeEditBox(){
				this.editBoxOpen = false;
				this.resetEditData();
	        },
	        resetEditData(){
	        	this.editData = {};
	        	this.editValidationMessage = "";				
	        },
	        editFormIsValid(){
	        	if(this.editData.name == "")
	        	{
	        		this.editValidationMessage = "Please enter a team name";
	        		return false;
	        	}

	        	if(this.editData.group_id == null)
	        	{
	        		this.editValidationMessage = "Please select a group";
	        		return false;
	        	}

	        	this.editValidationMessage = "";
	        	return true;
	        },
	        submitEdit(){		
				if(this.editFormIsValid() == true)
				{
		        	let app = this;
		            axios.put('/api/teams/'+app.editData.id, app.editData)
		            .then(function (response) {
		                app.resetEditData();
						app.loadTeams();
						app.closeEditBox();
		                showSwalSuccess("Success","The team has been updated");
		            }).catch(function (error) { showSwalError("Error updating team",error) });
		        }
	        },
	        submitDelete(){
	        	let app = this;
	            axios.delete('/api/teams/'+app.activeTeam.id, {
	            	id: app.activeTeam.id,
	            })
	            .then(function (response) {
	            	app.activeTeam = {}; 
					app.loadTeams();
					$('#deleteTeamModal').modal('hide');
					$('.modal-backdrop').remove();
	                showSwalSuccess("Success","The team has been deleted");
	            }).catch(function (error) { showSwalError("Error deleting team",error.response.request.response) });
	        },
	        setActive(team){
	        	this.activeTeam = team;
	        },
		},
		created: function () {
			this.loadTeams();
			this.loadGroups();
		},
	});

	showSwalError = function(title,text){
		console.log(text)
        swal({ title: title, text: text, type: "error", showCancelButton: false, allowEscapeKey: false });
	}

	showSwalSuccess = function(title,text){
        console.log(text);
        swal({ title: title, text: text, type: "success", showCancelButton: false, allowEscapeKey: false });
	}


</script>
@endsection