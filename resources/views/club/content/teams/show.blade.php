@extends('club.layout')

@section('title', 'Teams')

@section('content')
<div class="m-b-lg row" id="contentDiv">
	<div class="col-lg-12">



    </div>
</div>


@endsection


@section('scripts')
 
<script>

	const contentApp = new Vue ({
		el: '#contentDiv',
		data: {

		},
		methods: {

		},
		created: function () {

		},
	});

	showSwalError = function(title,text){
		console.log(text)
        swal({ title: title, text: text, type: "error", showCancelButton: false, allowEscapeKey: false });
	}

	showSwalSuccess = function(title,text){
        console.log(text);
        swal({ title: title, text: text, type: "success", showCancelButton: false, allowEscapeKey: false });
	}


</script>
@endsection