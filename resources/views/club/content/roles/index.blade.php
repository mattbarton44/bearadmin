@extends('club.layout')

@section('title', 'Roles')

@section('content')
<div class="row" id="contentDiv">
	<div class="col-lg-8">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
				<div class="row m-b-lg">
					<div class="col-xs-8">
		                <h2>Roles</h2>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-success btn-circle btn-lg pull-right" type="button" data-toggle="modal" data-target="#createRoleModal" @click="openModal()"><i class="fa fa-plus"></i></button>
                    </div>
                </div>
                <div class="input-group">
                    <input type="text" placeholder="Search roles " class="input form-control" v-model="roleSearch">
                    <span class="input-group-btn">
                            <button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i> Search</button>
                    </span>
                </div>
                <div class="clients-list">
					<div style="position: relative; height: 540px;">
	                    <div class="full-height-scroll">
	                        <div class="table-responsive">
	                            <table class="table table-striped table-hover">
	                            	<thead>
	                                    <th>Role Name</th>
	                                    <th>Description</th>
	                                    <th>Permissions</th>
	                                    <th>Users</th>
	                            	</thead>
	                                <tbody>
		                                <template v-for="role in filteredRoles">
			                                <tr @click="viewRole(role)">
			                                    <td>@{{ role.name }}</td>
			                                    <td>@{{ role.description }}</td>
			                                    <td>@{{ role.rolepermissions.length }}</td>
			                                    <td>@{{ role.users.length }}</td>
			                                </tr>
		                                </template>
	                                </tbody>
	                            	<tfoot>
                    					<th>Roles: @{{ filteredRoles.length }}</th>
                    					<th></th>
                    					<th></th>
                    					<th></th>
	                            	</tfoot>
	                            </table>
	                        </div>
	                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4"  v-show="activeRole.id != 0">
        <div class="ibox float-e-margins">
            <div class="ibox-content">

				<div class="row m-b">
					<div class="col-xs-8">
		                <h2>@{{ activeRole.name }}</h2>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-danger btn-circle btn-lg pull-right " :disabled="activeRole.users.length > 0" type="button" style="margin-left:5px" data-toggle="modal" data-target="#deleteRoleModal"  @click="openModal()">
		                	<i class="fa fa-times"></i>
		                </button>
		                <button class="btn btn-warning btn-circle btn-lg pull-right" type="button" data-toggle="modal" data-target="#updateRoleModal"  @click="openModal()"><i class="fa fa-edit"></i></button>
                    </div>
                </div>
                <div class="row m-b-lg">
                    <div class="col-lg-12">
                        <h4>Description</h4>

                        <p>
                            @{{ activeRole.description }}
                        </p>
                    </div>
                </div>
                <div class="row m-b-lg">
                    <div class="col-lg-12">
                        <h4>Permissions</h4>
                        <div style="position: relative; height: 280px;">
                        	<div class="full-height-scroll">
								<div class="table-responsive">
		                            <table class="table table-striped">
		                                <tbody>
			                                <tr v-for="rolepermission in activeRole.rolepermissions">
			                                    <td>@{{rolepermission.permission.name}}</td>
			                                    <td>@{{rolepermission.permission.description}}</td>
			                                </tr>
		                                </tbody>
		                            </table>
		                        </div>
		                    </div>
		                </div>
                    </div>
                </div>
                <div class="row m-b-lg">
                    <div class="col-lg-12">
                        <h4>Assigned Users</h4>
                        <div style="position: relative; height: 140px;">
                        	<div class="full-height-scroll">
								<div class="table-responsive">
		                            <table class="table table-striped">
		                                <tbody>
		                                	<tr v-for="user in activeRole.users">
		                                		<td>@{{user.first_name}} @{{user.last_name}}</td>
		                                	</tr>
		                                </tbody>
		                            </table>
		                        </div>
		                    </div>
		                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="createRoleModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
        	<div class="modal-content animated bounceInTop">
                <div class="modal-header">
                    <h4 class="modal-title">Create Role</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                    	<label>Name</label>
                    	<input type="text" placeholder="" class="form-control" v-model="createRole.name">
                    </div>
                    <div class="form-group">
                    	<label>Description</label>
                    	<input type="text" placeholder="" class="form-control" v-model="createRole.description">
                    </div>
                    <div class="form-group">
                    	<label>Permissions</label>
						<div style="position: relative; height: 260px;">
                        	<div class="full-height-scroll">
								<div class="table-responsive">
		                            <table class="table table-striped">
		                            	<thead>
		                                    <th></th>
		                                    <th>Name</th>
		                                    <th>Description</th>
		                            	</thead>
		                                <tbody>
		                                	<tr v-for="permission in permissions">
		                                		<td><div class="checkbox checkbox-primary abc-checkbox abc-checkbox-primary"><input type="checkbox" class="styled" :value="permission" v-model="createRole.permissions"><label></label></div></td>
		                                		<td>@{{permission.name}}</td>
		                                		<td>@{{permission.description}}</td>
		                                	</tr>
		                                </tbody>
		                            </table>
		                        </div>
		                    </div>
		                </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-white" @click="resetCreateRole()">Reset</button>
                    <button type="button" class="btn btn-primary" @click="submitCreateRole()">Submit</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="updateRoleModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
        	<div class="modal-content animated bounceInTop">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Role</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                    	<label>Name</label>
                    	<input type="text" placeholder="" class="form-control" v-model="editRole.name">
                    </div>
                    <div class="form-group">
                    	<label>Description</label>
                    	<input type="text" placeholder="" class="form-control" v-model="editRole.description">
                    </div>
                    <div class="form-group">
                    	<label>Permissions</label>
						<div style="position: relative; height: 260px;">
                        	<div class="full-height-scroll">
								<div class="table-responsive">
		                            <table class="table table-striped">
		                            	<thead>
		                                    <th></th>
		                                    <th>Name</th>
		                                    <th>Description</th>
		                            	</thead>
		                                <tbody>
		                                	<tr v-for="permission in permissions">
		                                		<td><div class="checkbox checkbox-primary abc-checkbox abc-checkbox-primary"><input type="checkbox" class="styled" :value="permission" v-model="editRole.permissions"><label></label></div></td>
		                                		<td>@{{permission.name}}</td>
		                                		<td>@{{permission.description}}</td>
		                                	</tr>
		                                </tbody>
		                            </table>
		                        </div>
		                    </div>
		                </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-white" @click="resetEditRole()">Reset</button>
                    <button type="button" class="btn btn-primary" @click="submitEditRole()">Submit</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="deleteRoleModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
        	<div class="modal-content animated bounceInTop">
                <div class="modal-header">
                    <h4 class="modal-title">Delete Role</h4>
                </div>
                <div class="modal-body" style="text-align:center">
                     <p>You are deleting the role <strong>@{{ activeRole.name }}</strong>.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" @click="submitDeleteRole()">Submit</button>
                </div>
            </div>
        </div>
    </div>


</div>

<style>
.checkbox, .radio {
    position: relative;
    display: block;
    margin-top: 0px;
    margin-bottom: 0px;
}
</style>
@endsection



@section('scripts')
<script>
	const contentApp = new Vue ({
		el: '#contentDiv',
		data: {
			roles:[],
			filteredRoles:[],
			permissions:[],
			activeRole:{
				id:0,
				name:"",
				description:"",
				rolepermissions:[],
				users:[],
				permissions:[],
			},
			editRole:{
				id:0,
				name:"",
				description:"",
				permissions:[],
			},
			createRole:{
				name:"",
				description:"",
				permissions:[],
			},
			roleSearch:"",
		},
		computed: {

		},
		watch: {
			roleSearch: function (val) {
				this.filterRoles();
			},
		},
		methods: {
	        loadRoles() {
	            let app = this;
	            axios.get('/api/roles')
	            .then(function (response) {
	                app.roles = response.data;
	                app.filterRoles();
	                if(app.activeRole.id != 0)
	                {
	                	for(var i=0; i<app.roles.length; i++)
	                	{
	                		if(app.roles[i].id == app.activeRole.id)
	                		{
	                			app.viewRole(app.roles[i]);
	                		}
	                	}
	                }
	            }).catch(function (error) { showSwalError("Error loading roles",error) });
	        },
	        loadPermissions() {
	            let app = this;
	            axios.get('/api/permissions')
	            .then(function (response) { 
	            	app.permissions = response.data; 
	            }).catch(function (error) { showSwalError("Error loading permissions",error) });
	        },
	        viewRole(role) {
	            this.activeRole = role;
				this.activeRole.permissions = [];
				for(var i=0;i<role.rolepermissions.length; i++)
				{
					this.activeRole.permissions.push(role.rolepermissions[i].permission)
				}
				this.resetEditRole();
	        },
	        submitCreateRole(){
	        	let app = this;
	            axios.post('/api/roles', {
    				name: app.createRole.name, 
    				description: app.createRole.description, 
    				permissions: app.createRole.permissions,  
	            })
	            .then(function (response) {
	                app.resetCreateRole();
					app.loadRoles();
					$('#createRoleModal').modal('hide');
					$('.modal-backdrop').remove();
	                showSwalSuccess("Success","The role has been created");
	            }).catch(function (error) { showSwalError("Error creating role",error) });
	        },
	        resetCreateRole(){
	        	this.createRole = {
	        		name:'',
	        		description:'',
	        		permissions:[]
	        	}
	        },
	        submitEditRole(){
	        	let app = this;
	            axios.put('/api/roles/'+app.editRole.id, {
	            	id:app.editRole.id,
    				name: app.editRole.name, 
    				description: app.editRole.description, 
    				permissions: app.editRole.permissions,  
	            })
	            .then(function (response) {
	                app.resetEditRole();
					app.loadRoles();
					$('#updateRoleModal').modal('hide');
					$('.modal-backdrop').remove();
	                showSwalSuccess("Success","The role has been updated");
	            }).catch(function (error) { showSwalError("Error updating role",error) });
	        },
	        resetEditRole(){
	        	this.editRole = {
	        		id: this.activeRole.id,
	        		name:this.activeRole.name,
	        		description:this.activeRole.description,
	        		permissions: this.activeRole.permissions,
	        	}
	        },
	        submitDeleteRole(){
	        	let app = this;
	            axios.delete('/api/roles/'+app.activeRole.id, {
	            	id:app.activeRole.id,
	            })
	            .then(function (response) {
	            	app.activeRole.id = 0;
					app.loadRoles();
					$('#deleteRoleModal').modal('hide');
					$('.modal-backdrop').remove();
	                showSwalSuccess("Success","The role has been deleted");
	            }).catch(function (error) { showSwalError("Error deleting role",error) });
	        },
	        filterRoles(){
				this.filteredRoles = [];
				for(var i=0;i<this.roles.length; i++)
				{
					if(this.roles[i].name.toUpperCase().indexOf(this.roleSearch.toUpperCase()) > -1 || 
						(this.roles[i].description != null && this.roles[i].description.toUpperCase().indexOf(this.roleSearch.toUpperCase()) > -1))
					{
						this.filteredRoles.push(this.roles[i]);
					}
				}
	        },
	        openModal(){
	        }
		},
		mounted() {
		},
		created: function () {
			this.loadRoles();
			this.loadPermissions();
		}
	});

	showSwalError = function(title,text){
        console.log(text);
        swal({ title: title, text: text, type: "error", showCancelButton: false, allowEscapeKey: false });
	}

	showSwalSuccess = function(title,text){
        console.log(text);
        swal({ title: title, text: text, type: "success", showCancelButton: false, allowEscapeKey: false });
	}

</script>
@endsection