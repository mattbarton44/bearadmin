@extends('club.layout')

@section('title', 'Calendar')

@section('content')
<div class="m-b-lg row" id="contentDiv" v-cloak v-show="initialised">
	<div class="col-lg-12">

        <div class="ibox float-e-margins">
            <div class="ibox-content">
				<div class="row">
					<div class="col-xs-8">
		                <h2>Calendar</h2>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-success btn-circle btn-lg pull-right m-l" type="button" @click="openCreateBox" :disabled="!permissions.events_create">
		                	<i class="fa fa-plus"></i>
		                </button>
		                <button class="btn btn-info btn-circle btn-lg pull-right m-l" type="button" @click="switchView">
		                	<i class="fa fa-eye"></i>
		                </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="ibox float-e-margins" v-show="createBoxOpen == true">
            <div class="ibox-content">
				<div class="row m-b-lg">
					<div class="col-xs-8">
		                <h2>Create Event</h2>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-danger btn-circle btn-lg pull-right" type="button" @click="closeCreateBox"><i class="fa fa-times"></i></button>
                    </div>
                </div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
	                    	<label>Name</label>
	                    	<input type="text" placeholder="" class="form-control" v-model="createData.name">
	                    </div>
                    </div>
                </div>
				<div class="row m-b">
					<div class="col-md-12">
	                    <label>Description</label>
	                    <textarea type="text" placeholder="" class="form-control" v-model="createData.description"></textarea>
                    </div>
                </div>
				<div class="row m-b">
					<div class="col-md-12">
	                    <label>Location</label>
	                    <input type="text" placeholder="" class="form-control" v-model="createData.location">
                    </div>
                </div>

                <div class="row m-b">
					<div class="col-md-12">
                    	<label>Type</label>
						<select v-model="createData.type" class="form-control">
							<option :value="'Game'">Game</option>
							<option :value="'Training'">Training</option>
							<option :value="'Social'">Social</option>
							<option :value="'Committee Meeting'">Committee Meeting</option>
							<option :value="'Other'">Other</option>
						</select>
                    </div>
                </div>

                <div class="row m-b">
					<div class="col-md-4">
						<div class="form-group">
	                    	<label>Start Date</label>
							<datepicker v-model="createData.startDate" :input-class="'form-control'"></datepicker>
	                    </div>
                    </div>
					<div class="col-md-4">
						<div class="form-group">
	                    	<label>Start Time</label>
	                    	<input type="text" placeholder="" class="form-control" v-model="createData.startTime" v-mask="'99:99'">
	                    </div>
                    </div>
					<div class="col-md-4">
						<div class="form-group">
	                    	<label>Duration (minutes)</label>
	                    	<input type="text" placeholder="" class="form-control" v-model="createData.duration">
	                    </div>
                    </div>
                </div>

                <div class="row m-b">
					<div class="col-md-12">
						<div class="form-group">
	                    	<label>Groups</label>
							<div style="height: 150px ;position: relative;">
	                        	<div class="full-height-scroll">
									<div class="table-responsive">
			                            <table class="table table-striped table-hover">
			                                <tbody>
			                                	<tr v-for="group in groups" @click="toggleCreateGroup(group)">
			                                		<td><div class="checkbox checkbox-primary abc-checkbox abc-checkbox-primary"><input type="checkbox" class="styled" :value="group" v-model="createData.groups" style="display: none;"><label></label></div></td>
			                                		<td>@{{group.name}}</td>
			                                	</tr>
			                                </tbody>
			                            </table>
			                        </div>
			                    </div>
			                </div>
	                    </div>
                    </div>
                </div>

				<div class="row m-b" v-show="createValidationMessage != ''">
					<div class="col-xs-12">
	                	<div class="form-group">
	                    	<h3 class="text-danger text-center">@{{createValidationMessage}}</h3>
	                    </div>
                    </div>
                </div>

				<div class="row m-t-lg">
					<div class="col-xs-12">
                        <button class="btn btn-block btn-primary" @click="submitCreate">Submit</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="ibox float-e-margins" v-show="editBoxOpen == true">
        	<div class="ibox-content">
				<div class="row m-b-lg">
					<div class="col-xs-8">
		                <h2>Edit Event - @{{ editData.id }}</h2>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-danger btn-circle btn-lg pull-right" type="button" @click="closeEditBox"><i class="fa fa-times"></i></button>
                    </div>
                </div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
	                    	<label>Name</label>
	                    	<input type="text" placeholder="" class="form-control" v-model="editData.name">
	                    </div>
                    </div>
                </div>
				<div class="row m-b">
					<div class="col-md-12">
	                    <label>Description</label>
	                    <textarea type="text" placeholder="" class="form-control" v-model="editData.description"></textarea>
                    </div>
                </div>
				<div class="row m-b">
					<div class="col-md-12">
	                    <label>Location</label>
	                    <input type="text" placeholder="" class="form-control" v-model="editData.location">
                    </div>
                </div>

                <div class="row m-b">
					<div class="col-md-12">
                    	<label>Type</label>
						<select v-model="editData.type" class="form-control">
							<option :value="'Game'">Game</option>
							<option :value="'Training'">Training</option>
							<option :value="'Social'">Social</option>
							<option :value="'Committee Meeting'">Committee Meeting</option>
							<option :value="'Other'">Other</option>
						</select>
                    </div>
                </div>

                <div class="row m-b">
					<div class="col-md-4">
						<div class="form-group">
	                    	<label>Start Date</label>
							<datepicker v-model="editData.startDate" :input-class="'form-control'"></datepicker>
	                    </div>
                    </div>
					<div class="col-md-4">
						<div class="form-group">
	                    	<label>Start Time</label>
	                    	<input type="text" placeholder="" class="form-control" v-model="editData.startTime" v-mask="'99:99'">
	                    </div>
                    </div>
					<div class="col-md-4">
						<div class="form-group">
	                    	<label>Duration (minutes)</label>
	                    	<input type="text" placeholder="" class="form-control" v-model="editData.duration">
	                    </div>
                    </div>
                </div>

                <div class="row ">
					<div class="col-md-12">
						<div class="form-group">
	                    	<label>Groups</label>
							<div style="height: 150px ;position: relative;">
	                        	<div class="full-height-scroll">
									<div class="table-responsive">
			                            <table class="table table-striped table-hover">
			                                <tbody>
			                                	<tr v-for="group in groups" @click="toggleEditGroup(group)">
			                                		<td><div class="checkbox checkbox-primary abc-checkbox abc-checkbox-primary"><input type="checkbox" class="styled" :value="group" v-model="editData.groups" style="display: none;"><label></label></div></td>
			                                		<td>@{{group.name}}</td>
			                                	</tr>
			                                </tbody>
			                            </table>
			                        </div>
			                    </div>
			                </div>
	                    </div>
                    </div>
                </div>

				<div class="row m-b" v-show="editValidationMessage != ''">
					<div class="col-xs-12">
	                	<div class="form-group">
	                    	<h3 class="text-danger text-center">@{{editValidationMessage}}</h3>
	                    </div>
                    </div>
                </div>

				<div class="row m-t-lg">
					<div class="col-xs-12">
                        <button class="btn btn-block btn-primary" @click="submitEdit">Submit</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="ibox float-e-margins" v-show="viewBoxOpen == true">
            <div class="ibox-content">
				<div class="row m-b-lg">
					<div class="col-xs-8">
		                <h2>@{{viewData.name}}</h2>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-danger btn-circle btn-lg pull-right" type="button" @click="closeViewBox"><i class="fa fa-times"></i></button>
                    </div>
                </div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
	                    	<label>Type</label>
	                    	<p class="form-control-static">@{{viewData.type}}</p>
	                    </div>
                    </div>
                </div>
				<div class="row m-b">
					<div class="col-md-12">
	                    <label>Date</label>
	                    	<p class="form-control-static">@{{this.moment(viewData.start).format("dddd, MMMM Do YYYY")}}</p>
                    </div>
                </div>
				<div class="row m-b">
					<div class="col-md-12">
	                    <label>Time</label>
	                    	<p class="form-control-static">@{{this.moment(viewData.start).format("HH:mm")+" - "+this.moment(viewData.end).format("HH:mm")}}</p>
                    </div>
                </div>
				<div class="row m-b">
					<div class="col-md-12">
	                    <label>Location</label>
	                    	<p class="form-control-static">@{{viewData.location}}</p>
                    </div>
                </div>
				<div class="row m-b">
					<div class="col-md-12">
	                    <label>Description</label>
	                    	<p class="form-control-static">@{{viewData.description}}</p>
                    </div>
                </div>

				<div class="row m-t-lg">
					<div class="col-xs-12">
                        <button v-if="permissions.events_edit" class="btn btn-block btn-warning" @click="openEditBox">Edit</button>
                        <button v-if="permissions.events_destroy" class="btn btn-block btn-danger" data-toggle="modal" data-target="#deleteEventModal">Delete</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="ibox" v-show="viewMode == 'list'">
            <div class="ibox-content">
				<div class="row">
					<div class="col-xs-12">
		                <h2>Upcoming Events</h2>
                    </div>
                </div>
            	<template v-for="event in calendarEvents">
			        <a>
			        <div v-if="moment(event.start).isAfter(moment())":class="'faq-item faq-item-' + event.type.split(' ')[0]" @click="openViewBox(event)">
			            <div class="row">
			                <div class="col-md-6">
				               <span class="faq-question">@{{event.title}}</span>
				               <small>
				                  Location:
				                  <strong>@{{event.location}}</strong>
				               </small>
			                </div>
							<div class="col-md-4">
								<span class="small font-bold">Time:</span>
								<br/>
								<span>@{{ moment(event.start, "YYYY-MM-DD HH:mm:ss").format('MMMM Do YYYY, HH:mm') }} - @{{ moment(event.end, "YYYY-MM-DD HH:mm:ss").format('HH:mm') }}</span>
							</div>
							<div class="col-md-2 text-right">
								<span class="small font-bold">Type:</span>
								<br/>
								<span :class="'label label-' + event.type.split(' ')[0]">@{{event.type}}</span>
							</div>
			            </div>
			        </div>
			    	</a>
			       </template>
            </div>
        </div>
        <div class="ibox" v-show="viewMode == 'list'">
            <div class="ibox-content">
				<div class="row">
					<div class="col-xs-12">
		                <h2>Past Events</h2>
	                </div>
	            </div>
            	<template v-for="event in calendarEvents">
            		<a>
			        <div v-if="moment(event.start).isBefore(moment())" :class="'faq-item faq-item-' + event.type.split(' ')[0]" @click="openViewBox(event)">
			            <div class="row">
			                <div class="col-md-6">
				               <span class="faq-question">@{{event.title}}</span>
				               <small>
				                  Location:
				                  <strong>@{{event.location}}</strong>
				               </small>
			                </div>
							<div class="col-md-4">
								<span class="small font-bold">Time:</span>
								<br/>
								<span>@{{ moment(event.start, "YYYY-MM-DD HH:mm:ss").format('MMMM Do YYYY, HH:mm') }} - @{{ moment(event.end, "YYYY-MM-DD HH:mm:ss").format('HH:mm') }}</span>
							</div>
							<div class="col-md-2 text-right">
								<span class="small font-bold">Type:</span>
								<br/>
								<span :class="'label label-' + event.type.split(' ')[0]">@{{event.type}}</span>
							</div>
			            </div>
			        </div>
			    	</a>
			    </template>
            </div>
        </div>

        <div class="ibox" v-show="viewMode == 'calendar'">
            <div class="ibox-content">
                <full-calendar  :config="calendarConfig" :events="calendarEvents" @event-selected="openViewBox"></full-calendar>
            </div>
        </div>

    </div>

    <div class="modal inmodal" id="deleteEventModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
        	<div class="modal-content animated bounceInTop">
                <div class="modal-header">
                    <h4 class="modal-title">Delete Event</h4>
                </div>
                <div class="modal-body" style="text-align:center">
                     <p>You are deleting the event <strong>@{{ viewData.name }} </strong>.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" @click="submitDeleteEvent()" data-dismiss="modal">Submit</button>
                </div>
            </div>
        </div>
    </div>

</div>

<style>
.checkbox, .radio {
    position: relative;
    display: block;
    margin-top: 0px;
    margin-bottom: 0px;
}
</style>

@endsection


@section('scripts')
<script>

	const contentApp = new Vue ({
		el: '#contentDiv',
		data: {
			init: { 
				events: false,
				groups: false,
				auth: false,
			},
			events:[],
			groups:[],
			viewMode: 'calendar',
			filterBoxOpen:false,
			filterData:{},
			filterMessage:"",
			editData:{},
			editValidationMessage:'',
			editBoxOpen: false,
			createData:{},
			createValidationMessage:'',
			createBoxOpen: false,
			viewData:{},
			viewBoxOpen:false,
			calendarConfig: {
				defaultView: 'month',
				header: {
                    left:   'prev,next today',
                    center: 'title',
                    right:  'month,agendaWeek,agendaDay,listWeek'
                }
			},
			calendarEvents:[],
			permissions:{
				events:false, 				//	View all events
				events_show:false,			//	View details for an event
				events_create:false,		//	Create a new event
				events_edit:false,			//	Edit an events details
				events_destroy:false,		//	Delete an event
			},
			auth_user:{},
		},
		computed: {
			initialised: function() {
				return this.init.events && this.init.groups && this.init.auth;
			}
		},
		methods: {
	        loadAuthUser() {
	        	let app = this;
	        	axios.get('/api/users/auth_data')
	            .then(function (response) {
	            	app.auth_user = response.data;
	                let perms = _.map(response.data.role.permissions, 'name');
	                for(var i=0; i<perms.length; i++)
	                {
	                	if(app.permissions[perms[i]] != null)
	                	{
	                		app.permissions[perms[i]] = true;
	                	}
	                }
	                app.init.auth = true;
	            }).catch(function (error) { showSwalError("Error loading user permissions",error) });
	        },
	        loadEvents() {
	            let app = this;
	            axios.get('/api/events')
	            .then(function (response) {
	                app.events = response.data;
	                app.calendarEvents = [];
	                for(var i=0; i<app.events.length; i++)
	                {
	                	app.calendarEvents.push({
	                		id:app.events[i].id,
	                		title:app.events[i].name,
	                		start:app.events[i].start,
	                		end:app.events[i].end,
	                		color:app.mapTypeColors(app.events[i].type),
	                		type:app.events[i].type,
	                		location:app.events[i].location
	                	});
	                }
	                app.init.events = true;
	            }).catch(function (error) { showSwalError("Error loading events",error) });
	        },
	        loadGroups() {
	            let app = this;
	            axios.get('/api/groups/solo')
	            .then(function (response) {
	                app.groups = response.data;
	                app.init.groups = true;
	            }).catch(function (error) { showSwalError("Error loading groups",error) });
	        },
	        mapTypeColors(type){
	        	switch(type)
	        	{
	        		case "Game":
	        			return "#2196f3";
	        		case "Training":
	        			return "#4caf50";
	        		case "Social":
	        			return "#ffc107";
	        		case "Committee Meeting":
	        			return "#F44336";
	        		case "Other":
	        			return "#9C27B0";
	        	}
	        	return "#9C27B0";
	        },
	        switchView(){
	        	if(this.viewMode == "calendar")
	        	{
	        		this.viewMode = "list";
	        	}
	        	else
	        	{
	        		this.viewMode = "calendar"
	        	}
	        },
	        openCreateBox(){
				this.resetCreateData();
				this.createBoxOpen = true;
	        },
	        closeCreateBox(){
				this.createBoxOpen = false;
				this.resetCreateData();
	        },
	        resetCreateData(){
	        	this.createData = {
	        		name:'',
	        		description:'',
	        		location:'',
	        		startDate:'',
	        		startTime:'',
	        		duration:'60',
	        		start:null,
	        		end:null,
	        		type:'',
	        		groups:[]
	        	};
	        	this.createValidationMessage = "";				
	        },
	        toggleCreateGroup(group){
	        	var hit = false;
	        	for(var i=0; i<this.createData.groups.length; i++)
	        	{
	        		if(this.createData.groups[i] == group)
	        		{
	        			this.createData.groups.splice(i,1);
	        			hit = true;
	        		}
	        	}
	        	if(hit == false)
	        	{
	        		this.createData.groups.push(group);
	        	}
	        },
	        createFormIsValid(){
	        	if(this.createData.name == "")
	        	{
	        		this.createValidationMessage = "Please enter an event name";
	        		return false;
	        	}

	        	if(this.createData.description == "")
	        	{
	        		this.createValidationMessage = "Please enter an event description";
	        		return false;
	        	}

	        	if(this.createData.type == "")
	        	{
	        		this.createValidationMessage = "Please enter an event type";
	        		return false;
	        	}

	        	if(this.createData.startDate == "")
	        	{
	        		this.createValidationMessage = "Please enter a start date";
	        		return false;
	        	}

	        	if(this.createData.startTime == "")
	        	{
	        		this.createValidationMessage = "Please enter a start time";
	        		return false;
	        	}

	        	this.createValidationMessage = "";
	        	return true;
	        },
	        submitCreate(){
				if(this.createFormIsValid() == true)
				{
		        	let app = this;
		        	let timeinmins = (parseInt(app.createData.startTime.split(":")[0]*60))+parseInt(app.createData.startTime.split(":")[1])
		        	app.createData.start = app.moment(app.createData.startDate).add(timeinmins, "m").format("Y-MM-DD HH:mm:ss");
		        	app.createData.end = app.moment(app.createData.start).add(app.createData.duration, "m").format("Y-MM-DD HH:mm:ss");
		            axios.post('/api/events', app.createData)
		            .then(function (response) {
		                app.resetCreateData();
						app.loadEvents();
						app.closeCreateBox();
		                showSwalSuccess("Success","The event has been created");
		            }).catch(function (error) { showSwalError("Error creating event",error) });
		        }
	        },
	        openViewBox(event){
	        	if(this.permissions.events_show == true)
	        	{
		        	for(var i=0; i<this.events.length; i++)
		        	{
		        		if(this.events[i].id == event.id)
		        		{
		        			this.viewData = this.events[i];
		        			this.viewBoxOpen = true;
		        			break;
		        		}
		        	}
		        }
	        },
	        closeViewBox(){

	        	this.viewBoxOpen = false;
	        },
	        openEditBox(){
				this.viewBoxOpen = false;
	        	this.editData = this.viewData;
	        	this.editData.startDate = this.moment(this.editData.start.split(" ")[0]).toDate();
	        	this.editData.startTime = this.editData.start.split(" ")[1].substring(0, 5);
	        	this.editData.duration = this.moment(this.editData.end).diff(this.editData.start, "minutes");
				this.editBoxOpen = true;
	        },
	        closeEditBox(){
				this.editBoxOpen = false;
				this.resetEditData();
	        },
	        resetEditData(){
	        	this.editData = {};
	        	this.editValidationMessage = "";				
	        },
	        toggleEditGroup(group){
	        	var hit = false;
	        	for(var i=0; i<this.editData.groups.length; i++)
	        	{
	        		if(this.editData.groups[i].id == group.id)
	        		{
	        			this.editData.groups.splice(i,1);
	        			hit = true;
	        		}
	        	}
	        	if(hit == false)
	        	{
	        		this.editData.groups.push(group);
	        	}
	        },
	        editFormIsValid(){
	        	if(this.editData.name == "")
	        	{
	        		this.editValidationMessage = "Please enter an event name";
	        		return false;
	        	}

	        	if(this.editData.description == "")
	        	{
	        		this.editValidationMessage = "Please enter an event description";
	        		return false;
	        	}

	        	if(this.editData.type == "")
	        	{
	        		this.editValidationMessage = "Please enter an event type";
	        		return false;
	        	}

	        	if(this.editData.startDate == null)
	        	{
	        		this.editValidationMessage = "Please enter a start date";
	        		return false;
	        	}

	        	if(this.editData.startTime == null)
	        	{
	        		this.editValidationMessage = "Please enter a start time";
	        		return false;
	        	}

	        	this.editValidationMessage = "";
	        	return true;
	        },
	        submitEdit(){
				if(this.editFormIsValid() == true)
				{
		        	let app = this;
		        	let timeinmins = (parseInt(app.editData.startTime.split(":")[0]*60))+parseInt(app.editData.startTime.split(":")[1])
		        	app.editData.start = app.moment(app.editData.startDate).add(timeinmins, "m").format("Y-MM-DD HH:mm:ss");
		        	app.editData.end = app.moment(app.editData.start).add(app.editData.duration, "m").format("Y-MM-DD HH:mm:ss");
		            axios.put('/api/events/'+app.editData.id, app.editData)
		            .then(function (response) {
		                app.resetEditData();
						app.loadEvents();
						app.closeEditBox();
		                showSwalSuccess("Success","The event has been updated");
		            }).catch(function (error) { showSwalError("Error updating event",error) });
		        }
	        },
	        submitDeleteEvent(){
				let app = this;
	            axios.delete('/api/events/'+app.viewData.id)
	            .then(function (response) {
	                showSwalSuccess("Success","The event has been deleted");
	                app.closeViewBox();
	                app.loadEvents();
	            }).catch(function (error) { showSwalError("Error deleting event",error) });
	        }
		},
		created: function () {
			this.loadAuthUser();
			this.loadEvents();
			this.loadGroups();

		},
	});

	showSwalError = function(title,text){
		console.log(text)
        swal({ title: title, text: text, type: "error", showCancelButton: false, allowEscapeKey: false });
	}

	showSwalSuccess = function(title,text){
        console.log(text);
        swal({ title: title, text: text, type: "success", showCancelButton: false, allowEscapeKey: false });
	}

</script>
@endsection