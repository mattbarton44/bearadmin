@extends('club.layout')

@section('title', 'Trainings')

@section('content')
<div class="m-b-lg row" id="contentDiv" v-cloak>
	<div class="col-lg-12">

        <div class="ibox float-e-margins">
            <div class="ibox-content">
				<div class="row">
					<div class="col-xs-8">
		                <h2>Trainings</h2>
                    </div>
					<div class="col-xs-4">
		           </div>
                </div>
            </div>
        </div>

    </div>


</div>

<style>

</style>

@endsection


@section('scripts')

<script>

	const contentApp = new Vue ({
		el: '#contentDiv',
		data: {
		},
		methods: {
		},
		created: function () {
		},
	});

	showSwalError = function(title,text){
		console.log(text)
        swal({ title: title, text: text, type: "error", showCancelButton: false, allowEscapeKey: false });
	}

	showSwalSuccess = function(title,text){
        console.log(text);
        swal({ title: title, text: text, type: "success", showCancelButton: false, allowEscapeKey: false });
	}


</script>
@endsection