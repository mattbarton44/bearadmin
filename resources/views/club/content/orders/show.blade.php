@extends('club.layout')

@section('title', 'Order')

@section('content')
<div class="m-b-lg row" id="contentDiv">

	<div class="col-lg-10 col-lg-offset-1">
		<div class="ibox">
		    <div class="ibox-content" v-if="data != null">

		       	<h1>Order Details - @{{ data.id }}</h1>

		       	<form method="get" class="form-horizontal m-t-lg">

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Username</label>
                        <div class="col-lg-10"><p class="form-control-static">@{{ data.user.first_name }} @{{ data.user.last_name }}</p></div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Date</label>
                        <div class="col-lg-10"><p class="form-control-static">@{{ data.updated_at }}</p></div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Product</label>
                        <div class="col-lg-10"><p class="form-control-static">@{{ data.product.name }}</p></div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Price</label>
                        <div class="col-lg-10"><p class="form-control-static">£@{{ (data.product.price/100).toFixed(2) }}</p></div>
                    </div>

                    <div class="form-group" v-for="(item, index) in data.form">
                    	<label class="col-lg-2 control-label">@{{ index }}</label>
                        <div class="col-lg-10"><p class="form-control-static">@{{ item }}</p></div>
                    </div>
                </form>


		    </div>
		</div>
    </div>


</div>

@endsection


@section('scripts')

<script>

	const contentApp = new Vue ({
		el: '#contentDiv',
		data: {
			data:null,
		},
		computed: {
		},
		watch: {
		},
		methods: {
	        loadRegistration() {
	            let app = this;
	            let pathname = window.location.pathname.split("/");
	            let path = "/api/orders/" + pathname[pathname.length-1]
	            axios.get(path)
	            .then(function (response) {
	                app.data = response.data;
	                app.data.form = JSON.parse(response.data.form);
	            }).catch(function (error) { showSwalError("Error loading details",error) });
	        },
		},
		mounted() {
		},
		created: function () {
			this.loadRegistration();
		},
		ready: function () {
			
		}
	});

	showSwalError = function(title,text){
		console.log(text)
        swal({ title: title, text: text, type: "error", showCancelButton: false, allowEscapeKey: false });
	}

	showSwalSuccess = function(title,text){
        console.log(text);
        swal({ title: title, text: text, type: "success", showCancelButton: false, allowEscapeKey: false });
	}


</script>
@endsection