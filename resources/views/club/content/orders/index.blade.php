@extends('club.layout')

@section('title', 'Orders')

@section('content')
<div class="m-b-lg row" id="contentDiv">
	<div class="col-lg-12">

        <div class="ibox float-e-margins">
            <div class="ibox-content">
				<div class="row">
					<div class="col-xs-8">
		                <h2>Orders</h2>
		                
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-default btn-circle btn-lg pull-right" type="button" @click="toggleFilters"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>

        <div class="ibox float-e-margins" v-show="filterBoxOpen">
            <div class="ibox-content">
				<div class="row m-b">
					<div class="col-xs-8">
		                <h2>Filter Orders</h2>
		                <p class="text-danger">Viewing <b>@{{ filteredData.length }}</b> orders out of <b>@{{ data.length }}.</b></p>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-primary btn-circle btn-lg pull-right m-l" type="button" @click="applyFilters"><i class="fa fa-check"></i></button>
		                <button class="btn btn-default btn-circle btn-lg pull-right" type="button" @click="resetFilters"><i class="fa fa-refresh"></i></button>
                    </div>
                </div>
				<div class="row">
					<div class="col-md-6">
	                    <label>Date From (age)</label>
	                    <div class="form-group">
							<div class="col-md-6" style="padding-left: 0px; padding-right: 5px;">
								<input type="text" placeholder="" class="form-control" v-model="filterData.from_date_count">
							</div>
							<div class="col-md-6" style="padding-left: 5px; padding-right: 0px;">
								<select v-model="filterData.from_date_type" class="form-control">
									<option :value="'all'"></option>
									<option :value="'minutes'">Minutes</option>
									<option :value="'hours'">Hours</option>
									<option :value="'days'">Days</option>
									<option :value="'weeks'">Week</option>
									<option :value="'months'">Months</option>
									<option :value="'years'">Years</option>
								</select>
							</div>
	                    </div>
					</div>
					<div class="col-md-6">
	                    <label>Date To (age)</label>
	                    <div class="form-group">
							<div class="col-md-6" style="padding-left: 0px; padding-right: 5px;">
								<input type="text" placeholder="" class="form-control" v-model="filterData.to_date_count">
							</div>
							<div class="col-md-6" style="padding-left: 5px; padding-right: 0px;">
								<select v-model="filterData.to_date_type" class="form-control">
									<option :value="'all'"></option>
									<option :value="'minutes'">Minutes</option>
									<option :value="'hours'">Hours</option>
									<option :value="'days'">Days</option>
									<option :value="'weeks'">Week</option>
									<option :value="'months'">Months</option>
									<option :value="'years'">Years</option>
								</select>
							</div>
	                    </div>
					</div>
				</div>
            </div>
        </div>

        <div class="ibox float-e-margins">
            <div class="ibox-content">
				<div class="row m-b">
					<div class="col-xs-12">
		                <h2>Finance Totals</h2>
		                <p class="text-info">View total cost of orders between selected dates</b></p>
                    </div>
                </div>
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
		                    <table class="table table-striped">
		                    	<thead>
			                    	<th>Product</th>
			                    	<th>Total</th>
		                    	</thead>
		                        <tbody>
		                        <tr v-for="(item, index) in financeTotals">
		                            <td>@{{ index }}</td>
		                            <td>£@{{ (item/100).toFixed(2) }}</td>
		                        </tr>
		                        </tbody>
		                    </table>
		                </div>
		                <h3>Total: £@{{ (financeTotal/100).toFixed(2)}}</h3>
					</div>
				</div>
            </div>
        </div>

        <div class="ibox">
            <div class="ibox-content">
                <div class="table-responsive">
                    <table class="table table-striped">
                    	<thead>
	                    	<th>Username</th>
	                    	<th>Product</th>
	                    	<th>Cost</th>
	                    	<th>Status</th>
	                    	<th>Date</th>
	                    	<th>Actions</th>
                    	</thead>
                        <tbody>
                        <tr v-for="datum in filteredData">
                            <td>@{{ datum.user.first_name }} @{{ datum.user.last_name }}</td>
                            <td>@{{ datum.product.name }}</td>
                            <td>£@{{ (datum.product.price/100).toFixed(2) }}</td>
                            <td>@{{ datum.status }}</td>
                            <td>@{{ moment(datum.updated_at, "YYYY-MM-DD HH:mm:ss").format('DD/MM/YYYY HH:mm:ss') }}</td>
                            <td><a :href="'orders/' + datum.id" class="btn btn-white btn-xs btn-block"> View Order</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('scripts')

<script>

	const contentApp = new Vue ({
		el: '#contentDiv',
		data: {
			data:[],
			filteredData:[],
			filterBoxOpen:true,
			filterData:{},
			financeTotals:[],
			financeTotal:""
		},
		computed: {
		},
		watch: {
		},
		methods: {
	        loadData() {
	            let app = this;
	            axios.get('/api/orders')
	            .then(function (response) {
	                app.data = response.data;
	                app.applyFilters();
	            }).catch(function (error) { showSwalError("Error loading data",error) });
	        },
	        toggleFilters(){
				this.filterBoxOpen = !this.filterBoxOpen;
				this.filterMessage = "";
	        },
	        resetFilters(){
	        	this.filterData = {	
					from_date_type:"all",
					from_date_count:"",
					to_date_type:"all",
					to_date_count:"",
				}		
				this.filterMessage = ""; 	
	        },
	        applyFilters(){
	        	let fd = this.filterData;

	        	let from_moment = this.moment();
	        	let to_moment = this.moment();

				if(fd.from_date_type != "" && fd.from_date_count != "")
				{
					from_moment.subtract(fd.from_date_count, fd.from_date_type);
				}
				if(fd.to_date_type != "" && fd.to_date_count != "")
				{
					to_moment.subtract(fd.to_date_count, fd.to_date_type);
				}

				this.filteredData = [];
				for(var i=0; i<this.data.length; i++)
				{
					let datum = this.data[i];
					if((fd.from_date_type == "all" || from_moment.isBefore(datum.updated_at)) && to_moment.isAfter(datum.updated_at))
					{
						this.filteredData.push(datum);
					}
				}

				this.financeTotals = {};
				for(var i=0; i<this.filteredData.length; i++)
				{
					if(this.financeTotals[this.filteredData[i].product.name] == null)
					{
						this.financeTotals[this.filteredData[i].product.name] = this.filteredData[i].product.price;
					}
					else
					{
						this.financeTotals[this.filteredData[i].product.name] = this.financeTotals[this.filteredData[i].product.name] + this.filteredData[i].product.price;
					}
				}
				this.financeTotal = _.reduce(this.financeTotals, function(sum, n) { return sum + n; }, 0);
	        }
		},
		mounted() {
		},
		created: function () {
			this.resetFilters();
			this.loadData();
		},
		ready: function () {
		}
	});

	showSwalError = function(title,text){
		console.log(text)
        swal({ title: title, text: text, type: "error", showCancelButton: false, allowEscapeKey: false });
	}

	showSwalSuccess = function(title,text){
        console.log(text);
        swal({ title: title, text: text, type: "success", showCancelButton: false, allowEscapeKey: false });
	}


</script>
@endsection