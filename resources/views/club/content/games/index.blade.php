@extends('club.layout')

@section('title', 'Games')

@section('content')
<div class="m-b-lg row" id="contentDiv" v-cloak v-show="initialised">
	<div class="col-lg-12">

		<div class="ibox float-e-margins" v-show="filterBoxOpen">
            <div class="ibox-content">
				<div class="row m-b">
					<div class="col-xs-8">
		                <h2>Filter Games</h2>
		                <p class="text-danger">Viewing <b>@{{ filteredData.length }}</b> games out of <b>@{{ games.length }}.</b></p>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-primary btn-circle btn-lg pull-right m-l" type="button" @click="applyFilters"><i class="fa fa-check"></i></button>
		                <button class="btn btn-default btn-circle btn-lg pull-right" type="button" @click="resetFilters"><i class="fa fa-refresh"></i></button>
                    </div>
                </div>
                <div class="row">
					<div class="col-md-6">
	                    <div class="form-group">
	                    	<label>From Season</label>
							<select v-model="filterData.season" class="form-control">
								<option :value="'all'">All</option>
								<option :value="'09-01-2019'">2019/20</option>
								<option :value="'09-01-2020'">2020/21</option>
								<option :value="'09-01-2021'">2021/22</option>
								<option :value="'09-01-2022'">2022/23</option>
								<option :value="'09-01-2023'">2023/24</option>
								<option :value="'09-01-2024'">2024/25</option>
							</select>
	                    </div>
					</div>
					<div class="col-md-6">
	                    <div class="form-group">
	                    	<label>Status</label>
							<select v-model="filterData.complete" class="form-control">
								<option :value="'all'">All</option>
								<option :value="'Complete'">Complete</option>
								<option :value="'Past'">Past</option>
								<option :value="'Upcoming'">Upcoming</option>
							</select>
	                    </div>
					</div>
				</div>
            </div>
        </div>

        <div class="ibox float-e-margins" v-show="manageBoxOpen == true">
        	<div class="ibox-content">
				<div class="row m-b-lg">
					<div class="col-xs-8">
		                <h2>Manage Game - @{{ manageData.id }}</h2>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-danger btn-circle btn-lg pull-right" type="button" @click="closeManageBox"><i class="fa fa-times"></i></button>
                    </div>
                </div>
				<div class="row m-b">
					<div class="col-md-12">
                    	<label>Game</label>
                    	<p>@{{ manageData.homeTeam }} v @{{ manageData.awayTeam }}</p>
                    </div>
                </div>

				<div class="row m-b">
					<div class="col-md-12">
                    	<label>Home Team Goals</label>
						<input type="text" placeholder="" class="form-control" v-model="manageData.homeTeamGoals">
                    </div>
                </div>

				<div class="row m-b">
					<div class="col-md-12">
                    	<label>Away Team Goals</label>
						<input type="text" placeholder="" class="form-control" v-model="manageData.awayTeamGoals">
                    </div>
                </div>
				<div class="row m-b">
					<div class="col-md-12">
                    	<label>Complete</label>
						<select v-model="manageData.complete" class="form-control">
							<option :value="1">Yes</option>
							<option :value="0">No</option>
						</select>
                    </div>
                </div>

				<div class="row m-b">
					<div class="col-md-6">
						<div class="form-group">
	                    	<label>Game Jobs</label>
							<div class="table-responsive">
	                            <table class="table table-bordered table-striped">
	                            	<thead>
	                            		<tr>
	                            			<td>Name</td>
	                            			<td>Role</td>
	                            			<td>Previous Experience</td>
	                            			<td>Signed up at</td>
	                            			<td>Attended</td>
	                            		</tr>
	                            	</thead>
	                                <tbody>
	                                	<tr v-for="job in manageData.jobs">
	                                		<td>@{{ job.user.first_name }} @{{ job.user.last_name }}</td>
	                                		<td>@{{ job.role }}</td>
	                                		<td v-if="job.prevExp == true">Yes</td>
	                                		<td v-else>No</td>
	                                		<td>@{{ moment(job.created_at, "YYYY-MM-DD HH:mm:ss").format('MMMM Do YYYY, HH:mm') }}</td>
	                                		<td>
												<select v-model="job.attended" class="form-control-sm">
													<option :value="1">Yes</option>
													<option :value="0">No</option>
												</select>
	                                		</td>
	                                	</tr>
	                                </tbody>
	                            </table>
	                        </div>
	                    </div>
                    </div>
                </div>

				<div class="row m-t-lg">
					<div class="col-xs-12">
                        <button class="btn btn-block btn-primary" @click="submitManage">Submit</button>
                    </div>
                </div>

            </div>
        </div>

        <div class="ibox float-e-margins" v-show="jobsBoxOpen == true">
        	<div class="ibox-content">
				<div class="row m-b-lg">
					<div class="col-xs-8">
		                <h2>Game Jobs - @{{ jobsData.game.id }}</h2>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-danger btn-circle btn-lg pull-right" type="button" @click="closeJobsBox"><i class="fa fa-times"></i></button>
                    </div>
                </div>
				<div class="row m-b">
					<div class="col-md-12">
                    	<label>Game</label>
                    	<p>@{{ jobsData.game.homeTeam }} v @{{ jobsData.game.awayTeam }}</p>
                    </div>
                </div>

				<div class="row m-b">
					<div class="col-md-6">
						<div class="form-group">
	                    	<label>Reserved Jobs</label>
							<div class="table-responsive">
	                            <table class="table table-bordered table-striped">
	                            	<thead>
	                            		<tr>
	                            			<td>Name</td>
	                            			<td>Role</td>
	                            			<td>Previous Experience</td>
	                            			<td>Signed up at</td>
	                            			<td>Attended</td>
	                            		</tr>
	                            	</thead>
	                                <tbody>
	                                	<tr v-for="job in jobsData.game.jobs">
	                                		<td>@{{ job.user.first_name }} @{{ job.user.last_name }}</td>
	                                		<td>@{{ job.role }}</td>
	                                		<td v-if="job.prevExp == true">Yes</td>
	                                		<td v-else>No</td>
	                                		<td>@{{ moment(job.created_at, "YYYY-MM-DD HH:mm:ss").format('MMMM Do YYYY, HH:mm') }}</td>
	                                		<td v-if="job.attended == 1">Yes</td>
	                                		<td v-else>No</td>
	                                	</tr>
	                                </tbody>
	                            </table>
	                        </div>
	                    </div>
                    </div>
                </div>

                <template v-if="hasGameJob != null && jobsData.game.complete == false">
				<div class="row m-t-lg">
					<div class="col-xs-12">
                        <button class="btn btn-block btn-danger" @click="deleteMyJob(jobsData.game)">Delete my game job!</button>
                    </div>
                </div>
            	</template>
            	<template v-else-if="jobsData.game.complete == false">
	                <div class="row m-b">
						<div class="col-md-12">
	                    	<label>Role</label>
							<select v-model="jobsData.role" class="form-control">
								<option :value="'Game Clock'">Game Clock</option>
								<option :value="'Gamesheet'">Gamesheet</option>
								<option :value="'Penalty Box'">Penalty Box (max 4)</option>
								<option :value="'Announcer'">Announcer</option>
								<option :value="'Music'">Music</option>
								<option :value="'Goal Judge'">Goal Judge (max 2)</option>
							</select>
	                    </div>
	                </div>

					<div class="row m-b">
						<div class="col-md-12">
		                    <label>Have you done this job before?</label>
							<select v-model="jobsData.prevExp" class="form-control">
								<option :value="true">Yes</option>
								<option :value="false">No</option>
							</select>
	                    </div>
	                </div>

					<div class="row m-t-lg">
						<div class="col-xs-12">
	                        <button class="btn btn-block btn-primary" @click="submitJobs">Submit</button>
	                    </div>
	                </div>
            	</template>

            </div>
        </div>

        <div class="ibox float-e-margins">
            <div class="ibox-content">
				<div class="row">
					<div class="col-xs-8">
		                <h2>Games</h2>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-default btn-circle btn-lg pull-right" type="button" @click="toggleFilters"><i class="fa fa-search"></i></button>
                    </div>
                </div>
				<div class="project-list">
                    <table class="table table-hover">
                    	<thead>
                    		<th>Details</th>
                    		<th>Location</th>
                    		<th>Jobs Left</th>
                    		<th>Result</th>
                    		<th>Status</th>
                    		<th>Actions</th>
                    	</thead>
                        <tbody>
                        <tr v-for="game in filteredData">
                            <td class="project-title">
                                <a href="">@{{ game.homeTeam }} v @{{ game.awayTeam }}</a>
                                <br/>
                                <small>@{{ moment(game.event.start, "YYYY-MM-DD HH:mm:ss").format('MMMM Do YYYY, HH:mm') }} - @{{ moment(game.event.end, "YYYY-MM-DD HH:mm:ss").format('HH:mm') }}</small>
                            </td>
                            <td class="project-status">
                                <span class="">@{{ game.event.location }}</span>
                            </td>
                            <td class="project-status">
                            	<template v-if="game.jobs.length <= 10 && game.event.location == 'IceSheffield'">
                                	<span class="">@{{ 10 - game.jobs.length }}</span>
                            	</template>
                                <template v-else>
                                	<span class="">0</span>
                                </template>
                            </td>
                            <td class="project-status">

                            	<template v-if="game.complete == true">
                                	<span class="">@{{ game.homeTeamGoals }} - @{{ game.awayTeamGoals }}</span>
                            	</template>
                                <template v-else>
                                	<span class="">Pending</span>
                                </template>
                            </td>
                            <td class="project-status">
                            	<template v-if="game.complete == true">
	                                <span class="label label-success">Complete</span>
                            	</template>
                                <template v-else>
	                            	<template v-if="moment(game.event.start).isBefore(moment())">
	                                	<span class="label label-danger">Past</span>
	                                </template>
	                                <template v-else>
	                                	<span class="label label-default">Upcoming</span>
	                                </template>
                                </template>
                            </td>
                            <td class="">
            <a @click="openJobsBox(game)" class="btn btn-primary btn-sm" v-show="permissions.gamejobs && game.event.location == 'IceSheffield'">Jobs </a>
            <a @click="openManageBox(game)" class="btn btn-success btn-sm" v-show="permissions.games_edit">Manage </a>
            <a @click="openViewBox(game)" class="btn btn-info btn-sm" v-show="permissions.games_show">View </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
	            </div>
            </div>
        </div>

        <div class="ibox float-e-margins" v-show="registerBoxOpen == true">
        	<div class="ibox-content">
				<div class="row m-b-lg">
					<div class="col-xs-8">
		                <h2>Register Game - @{{ registerData.id }}</h2>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-danger btn-circle btn-lg pull-right" type="button" @click="closeRegisterBox"><i class="fa fa-times"></i></button>
                    </div>
                </div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
	                    	<label>Name</label>
	                    	<span>@{{ registerData.name }}</span>
	                    </div>
                    </div>
                </div>
                <div class="row m-b">
					<div class="col-md-12">
                    	<label>Team</label>
						<select v-model="registerData.team" class="form-control">
							<template v-for="team in teams">
								<option :value="team.id">@{{ team.name }}</option>
							</template>
						</select>
                    </div>
                </div>

				<div class="row m-b">
					<div class="col-md-12">
	                    <label>Home Team Name</label>
	                    <input type="text" placeholder="" class="form-control" v-model="registerData.homeTeam">
                    </div>
                </div>

				<div class="row m-b">
					<div class="col-md-12">
	                    <label>Away Team Name</label>
	                    <input type="text" placeholder="" class="form-control" v-model="registerData.awayTeam">
                    </div>
                </div>

				<div class="row m-t-lg">
					<div class="col-xs-12">
                        <button class="btn btn-block btn-primary" @click="submitRegister">Submit</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="ibox float-e-margins" v-if="permissions.games_create">
            <div class="ibox-content">
				<div class="row m-b">
					<div class="col-xs-8">
		                <h2>Register Games</h2>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-success btn-lg pull-right m-l" type="button" @click="getAllEvents">Get All Events</button>
                    </div>
                </div>
            	<div class="project-list">
                    <table class="table table-hover">
                    	<thead>
                    		<th>Details</th>
                    		<th>Location</th>
                    		<th>Date</th>
                    		<th>Status</th>
                    		<th>Actions</th>
                    	</thead>
                        <tbody>
                        <tr v-for="event in events">
                            <td class="project-title">
                                <a href="">@{{event.name}}</a>
                                <br/>
                                <small>@{{event.description}}</small>
                            </td>
                            <td class="project-status">
                                <span class="">@{{ event.location }}</span>
                            </td>

                            <td class="project-status">
                                <span class="">@{{ moment(event.start, "YYYY-MM-DD HH:mm:ss").format('MMMM Do YYYY, HH:mm') }} - @{{ moment(event.end, "YYYY-MM-DD HH:mm:ss").format('HH:mm') }}</span>
                            </td>
                            <td class="project-status">
                            	<template v-if="moment(event.start).isBefore(moment())">
                                	<span class="label label-primary">Past</span>
                                </template>
                                <template v-else>
                                	<span class="label label-default">Upcoming</span>
                                </template>
                            </td>
                            <td class="">
                                <a @click="openRegisterBox(event)" class="btn btn-success btn-sm">Register Game </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>



    </div>
</div>

<style>
.checkbox, .radio {
    position: relative;
    display: block;
    margin-top: 0px;
    margin-bottom: 0px;
}
</style>

@endsection


@section('scripts')

<script>

	const contentApp = new Vue ({
		el: '#contentDiv',
		data: {
			init: { 
				games: false,
				events: false,
				auth: false,
				teams: false
			},
			games:[],
			events:[],
			teams:[],
			permissions:{
				games:false, 				//	View all events
				games_show:false,			//	View details for an event
				games_create:false,			//	Create a new event
				games_edit:false,			//	Edit an events details
				games_destroy:false,		//	Delete an event
				gamejobs:false, 			//	View all events
				gamejobs_show:false,		//	View details for an event
				gamejobs_create:false,		//	Create a new event
				gamejobs_edit:false,		//	Edit an events details
				gamejobs_destroy:false,		//	Delete an event
			},
			auth_user:{},
			viewMode: "list",
			registerBoxOpen: false,
			registerData: {
				id: null,
				name: null,
				team: null,
				homeTeam: null,
				awayTeam: null
			},
			jobsBoxOpen: false,
			jobsData: {
				game: {id:null},
				role: null,
				prevExp: null,
			},
			manageBoxOpen: false,
			manageData: {
			},
			filteredData:[],
			filterBoxOpen:false,
			filterData:{},
		},
		computed: {
			initialised: function() {
				return this.init.games && this.init.events && this.init.auth && this.init.teams;
			},
			hasGameJob: function() {
				if(this.jobsData.game.jobs != null)
				{
					for(var i=0; i<this.jobsData.game.jobs.length; i++)
					{
						if(this.jobsData.game.jobs[i].user_id == this.auth_user.id)
						{
							return this.jobsData.game.jobs[i].id;
						}
					}
				}
				return null;
			}
		},
		methods: {
	        loadAuthUser() {
	        	let app = this;
	        	axios.get('/api/users/auth_data')
	            .then(function (response) {
	            	app.auth_user = response.data;
	                let perms = _.map(response.data.role.permissions, 'name');
	                for(var i=0; i<perms.length; i++)
	                {
	                	if(app.permissions[perms[i]] != null)
	                	{
	                		app.permissions[perms[i]] = true;
	                	}
	                }
	                app.init.auth = true;
	            }).catch(function (error) { showSwalError("Error loading user permissions",error) });
	        },
	        loadGames() {
	            let app = this;
	            axios.get('/api/games')
	            .then(function (response) {
	            	app.games = response.data;
					app.games.sort(function(a,b){
					  return new Date(a.event.start) - new Date(b.event.start);
					});
	                app.init.games = true;
	                app.applyFilters();
	            }).catch(function (error) { showSwalError("Error loading games",error) });
	        },
	        loadTeams() {
	            let app = this;
	            axios.get('/api/teams')
	            .then(function (response) {
	                app.teams = response.data;
	                app.init.teams = true;
	            }).catch(function (error) { showSwalError("Error loading teams",error) });
	        },
	        loadEvents() {
	            let app = this;
	            axios.get('/api/events/games')
	            .then(function (response) {
	                app.events = [];
	                for(var i=0; i<response.data.length; i++)
	                {
	                	if(response.data[i].type == "Game")
	                	{
	                		app.events.push(response.data[i]);
	                	}
	                }
	                app.init.events = true;
	            }).catch(function (error) { showSwalError("Error loading events",error) });
	        },
	        openRegisterBox(event) {
	        	this.registerData = {
	        		id: event.id,
	        		name: event.name,
	        		team: null,
	        		homeTeam: "",
	        		awayTeam: "",
	        	}
	        	this.registerBoxOpen = true;
	        },
	        closeRegisterBox(){
	        	this.registerBoxOpen = false;
	        },
	        submitRegister(){
	        	let app = this;
	            axios.post('/api/games', app.registerData)
	            .then(function (response) {
					app.loadGames();
					app.loadEvents();
					app.closeRegisterBox();

	                showSwalSuccess("Success","The game has been registered");
	            }).catch(function (error) { showSwalError("Error registering game",error) });
	        },
	        getAllEvents(){
	        	let app = this;
	            axios.get('/api/events')
	            .then(function (response) {
	                app.events = [];
	                for(var i=0; i<response.data.length; i++)
	                {
	                	if(response.data[i].type == "Game")
	                	{
	                		app.events.push(response.data[i]);
	                	}
	                }
	                app.init.events = true;
	            }).catch(function (error) { showSwalError("Error loading events",error) });
	        },
	        openJobsBox(game){
	        	this.jobsData = {
					game: game,
					role: "",
					prevExp: false,
	        	}
				this.closeManageBox();
	        	this.jobsBoxOpen = true;
	        },
	        closeJobsBox(){
	        	this.jobsBoxOpen = false;
	        },
	        submitJobs(){
	        	let app = this;
	            axios.post('/api/gamejobs', app.jobsData)
	            .then(function (response) {
					app.loadGames();
					app.closeJobsBox();
	                showSwalSuccess("Success","You have reigstered to do a job for this game");
	            }).catch(function (error) { showSwalError("Error registering job",error) });
	        },
	        deleteMyJob(game){
	        	let app = this;
	            axios.delete('/api/gamejobs/'+app.hasGameJob)
	            .then(function (response) {
					app.loadGames();	
					app.closeJobsBox();            	
	                showSwalSuccess("Success","The game job has been deleted");
	            }).catch(function (error) { showSwalError("Error deleting game job",error) });
	        },
	        openManageBox(game){
	        	this.manageData = game;
				this.closeJobsBox();
	        	this.manageBoxOpen = true;
	        },
	        closeManageBox(){
	        	this.manageBoxOpen = false;
	        },
	        submitManage(){
	        	let app = this;
	            axios.put('/api/games/'+app.manageData.id, app.manageData)
	            .then(function (response) {
					app.loadGames();
					app.closeManageBox();
	                showSwalSuccess("Success","You have updated the information for this game");
	            }).catch(function (error) { showSwalError("Error updating game",error) });
	        },
	        openViewBox(game){

	        },
	        toggleFilters(){
				this.filterBoxOpen = !this.filterBoxOpen;
				this.filterMessage = "";
	        },
	        resetFilters(){
	        	this.filterData = {	
					season:"09-01-2019",
					status:"all",
				}		
				this.filterMessage = ""; 	
	        },
	        applyFilters(){
	        	let fd = this.filterData;

				this.filteredData = [];
				for(var i=0; i<this.games.length; i++)
				{
					let datum = this.games[i];

					let status = "Upcoming";
					if(datum.complete == true)
					{
						status = "Complete";
					}
					else if(this.moment(datum.event.start).isBefore(this.moment()) == true)
					{
						status = "Past";
					}

					if(	(fd.season == "all" || this.moment(datum.event.start) >= this.moment(fd.season, "MM-DD_YYYY")) &&
						(fd.status == "all" || status == fd.status) )
					{
						this.filteredData.push(datum);
					}
				}
	        }
	    },
		created: function () {
			this.resetFilters();
			this.loadAuthUser();
			this.loadGames();
			this.loadEvents();
			this.loadTeams();
		},
	});

	showSwalError = function(title,text){
		console.log(text)
        swal({ title: title, text: text, type: "error", showCancelButton: false, allowEscapeKey: false });
	}

	showSwalSuccess = function(title,text){
        console.log(text);
        swal({ title: title, text: text, type: "success", showCancelButton: false, allowEscapeKey: false });
	}


</script>
@endsection