@extends('club.layout')

@section('title', 'Registration')

@section('content')
<div class="m-b-lg row" id="contentDiv">

	<div class="col-lg-10 col-lg-offset-1"  v-cloak>
		<div class="ibox">
		    <div class="ibox-content" v-if="reg != null">

		       	<h1>Registration Details - @{{ reg.id }}</h1>

		       	<form method="get" class="form-horizontal m-t-lg">


                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                    	<label class="col-lg-2 control-label">Registration Status</label>
                        <div class="col-lg-10"><p class="form-control-static">@{{ reg.status }}</p></div>
                    </div>

                    <div class="form-group">
                    	<label class="col-lg-2 control-label">Registration Season</label>
                        <div class="col-lg-10"><p class="form-control-static">@{{ reg.season }}</p></div>
                    </div>

                    <div class="form-group">
                    	<label class="col-lg-2 control-label">Resgistration Type</label>
                        <div class="col-lg-10"><p class="form-control-static">@{{ reg.type }}</p></div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                    	<label class="col-lg-2 control-label">Full Name</label>
                        <div class="col-lg-10"><p class="form-control-static">@{{ reg.user.first_name }} @{{ reg.user.last_name }}</p></div>
                    </div>

                    <div class="form-group">
                    	<label class="col-lg-2 control-label">Email Address</label>
                        <div class="col-lg-10"><p class="form-control-static">@{{ reg.user.email }}</p></div>
                    </div>
                    <div class="form-group">
                    	<label class="col-lg-2 control-label">Date Of Birth</label>
                        <div class="col-lg-10"><p class="form-control-static">@{{ reg.form["Date Of Birth"] }}</p></div>
                    </div>
                    <div class="form-group">
                    	<label class="col-lg-2 control-label">Nationality</label>
                        <div class="col-lg-10"><p class="form-control-static">@{{ reg.form["Nationality"] }}</p></div>
                    </div>
                      <div class="form-group">
                    	<label class="col-lg-2 control-label">Gender</label>
                        <div class="col-lg-10"><p class="form-control-static">@{{ reg.form["Gender"] }}</p></div>
                    </div>
                    <div class="form-group">
                    	<label class="col-lg-2 control-label">Address Line 1</label>
                        <div class="col-lg-10"><p class="form-control-static">@{{ reg.form["Address Line 1"] }}</p></div>
                    </div>
                    <div class="form-group">
                    	<label class="col-lg-2 control-label">Address Line 2</label>
                        <div class="col-lg-10"><p class="form-control-static">@{{ reg.form["Address Line 2"] }}</p></div>
                    </div>
                    <div class="form-group">
                    	<label class="col-lg-2 control-label">Address Line 3</label>
                        <div class="col-lg-10"><p class="form-control-static">@{{ reg.form["Address Line 3"] }}</p></div>
                    </div>
                    <div class="form-group">
                    	<label class="col-lg-2 control-label">Postcode</label>
                        <div class="col-lg-10"><p class="form-control-static">@{{ reg.form["Postcode"] }}</p></div>
                    </div>
                    <div class="form-group">
                    	<label class="col-lg-2 control-label">Telephone Number</label>
                        <div class="col-lg-10"><p class="form-control-static">@{{ reg.form["Telephone Number"] }}</p></div>
                    </div>
                    
                    <div class="hr-line-dashed"></div>
                    <div class="form-group" v-if="reg.form['University'] != null">
                    	<label class="col-lg-2 control-label">University</label>
                        <div class="col-lg-10"><p class="form-control-static">@{{ reg.form["University"] }}</p></div>
                    </div>
                    <div class="form-group" v-if="reg.form['Course Title'] != null">
                    	<label class="col-lg-2 control-label">Course Title</label>
                        <div class="col-lg-10"><p class="form-control-static">@{{ reg.form["Course Title"] }}</p></div>
                    </div>
                    <div class="form-group" v-if="reg.form['Year Of Graduation'] != null">
                    	<label class="col-lg-2 control-label">Year Of Graduation</label>
                        <div class="col-lg-10"><p class="form-control-static">@{{ reg.form["Year Of Graduation"] }}</p></div>
                    </div>
                    <div class="form-group" v-if="reg.form['Student Number'] != null">
                    	<label class="col-lg-2 control-label">Student Number</label>
                        <div class="col-lg-10"><p class="form-control-static">@{{ reg.form["Student Number"] }}</p></div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group" v-if="reg.form['Personal Identification'] != null">
                    	<label class="col-lg-2 control-label">Personal Identification</label>
                        <div class="col-lg-10">
                            <p class="form-control-static"><img :src="'/storage/' + reg.form['Personal Identification']" style="max-width: 100%;"></p>
                        </div>
                    </div>
                    <div class="form-group" v-if="reg.form['Student Card'] != null">
                    	<label class="col-lg-2 control-label">Student Card</label>
                        <div class="col-lg-10">
                            <p class="form-control-static"><img :src="'/storage/' + reg.form['Student Card']" style="max-width: 100%;"></p>
                        </div>
                    </div>
                    <div class="form-group" v-if="reg.form['Proof Of Enrolment'] != null">
                    	<label class="col-lg-2 control-label">Proof Of Enrolment</label>
                        <div class="col-lg-10">
                            <p class="form-control-static"><img :src="'/storage/' + reg.form['Proof Of Enrolment']" style="max-width: 100%;"></p>
                        </div>
                    </div>
                    <div class="form-group" v-if="reg.form['Proof of Graduation'] != null">
                    	<label class="col-lg-2 control-label">Proof of Graduation</label>
                        <div class="col-lg-10">
                            <p class="form-control-static"><img :src="'/storage/' + reg.form['Proof of Graduation']" style="max-width: 100%;"></p>
                        </div>
                    </div>
                    <div class="form-group" v-if="reg.form['Staff Card'] != null">
                    	<label class="col-lg-2 control-label">Staff Card</label>
                        <div class="col-lg-10">
                            <p class="form-control-static"><img :src="'/storage/' + reg.form['Staff Card']" style="max-width: 100%;"></p>
                        </div>
                    </div>
                    <div class="form-group" v-if="reg.form['Payslip'] != null">
                    	<label class="col-lg-2 control-label">Payslip</label>
                        <div class="col-lg-10">
                            <p class="form-control-static"><img :src="'/storage/' + reg.form['Payslip']" style="max-width: 100%;"></p>
                        </div>
                    </div>
                    <div class="form-group" v-if="reg.form['Proof Of Payment'] != null">
                    	<label class="col-lg-2 control-label">Proof Of Payment</label>
                        <div class="col-lg-10">
                            <p class="form-control-static"><img :src="'/storage/' + reg.form['Proof Of Payment']" style="max-width: 100%;"></p>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group" v-if="reg.status == 'Pending'">
                        <div class="col-sm-12">
                            <button class="btn btn-danger btn-block" type="button" @click="updateStatus('Rejected')">Reject Registration</button>
                            <button class="btn btn-primary btn-block" type="button" @click="updateStatus('Complete')">Complete Registration</button>
                        </div>
                    </div>
                    <div class="form-group" v-if="reg.status != 'Pending'">
                        <div class="col-sm-12">
                            <button class="btn btn-white btn-block" type="button" @click="updateStatus('Pending')">Revert to Pending</button>
                        </div>
                    </div>
                </form>


		    </div>
		</div>
    </div>


</div>

@endsection


@section('scripts')

<script>

	const contentApp = new Vue ({
		el: '#contentDiv',
		data: {
			reg:null,
		},
		computed: {
		},
		watch: {
		},
		methods: {
	        loadRegistration() {
	            let app = this;
	            let pathname = window.location.pathname.split("/");
	            let path = "/api/registrations/" + pathname[pathname.length-1]
	            axios.get(path)
	            .then(function (response) {
	                app.reg = response.data;
	                app.reg.form = JSON.parse(response.data.form);
	            }).catch(function (error) { showSwalError("Error loading registration details",error) });
	        },
	        updateStatus(status) {
	        	let app = this;
	        	swal({
					title: "Confirm",
					text: "You are setting this registration status to " + status,
					type: "info",
					showCancelButton: true,
					confirmButtonText: "Confirm!",
					cancelButtonText: "Cancel!",
					closeOnConfirm: false,
					closeOnCancel: true
				},
				function(isConfirm){
					if (isConfirm) 
					{
						axios.put('/api/registrations/'+app.reg.id, {
			        		id: app.reg.id,
			        		status:status,
			            })
			            .then(function (response) {
			                app.loadRegistration();
			                showSwalSuccess("Success","The registration has been updated");
                        }).catch(function (error) { showSwalError("Error updating registration",error) });
					}
				});
	        },

		},
		mounted() {
		},
		created: function () {
			this.loadRegistration();
		},
		ready: function () {
			
		}
	});

	showSwalError = function(title,text){
		console.log(text)
        swal({ title: title, text: text, type: "error", showCancelButton: false, allowEscapeKey: false });
	}

	showSwalSuccess = function(title,text){
        console.log(text);
        swal({ title: title, text: text, type: "success", showCancelButton: false, allowEscapeKey: false });
	}


</script>
@endsection