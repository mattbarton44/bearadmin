@extends('club.layout')

@section('title', 'Registrations')

@section('content')
<div class="m-b-lg row" id="contentDiv">
	<div class="col-lg-12">

        <div class="ibox float-e-margins">
            <div class="ibox-content">
				<div class="row">
					<div class="col-xs-8">
		                <h2>Registrations</h2>
		                
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-default btn-circle btn-lg pull-right" type="button" @click="toggleFilters"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>

        <div class="ibox float-e-margins" v-show="filterBoxOpen">
            <div class="ibox-content">
				<div class="row m-b">
					<div class="col-xs-8">
		                <h2>Filter Registrations</h2>
		                <p class="text-danger">Viewing <b>@{{ filteredData.length }}</b> registrations out of <b>@{{ data.length }}.</b></p>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-primary btn-circle btn-lg pull-right m-l" type="button" @click="applyFilters"><i class="fa fa-check"></i></button>
		                <button class="btn btn-default btn-circle btn-lg pull-right" type="button" @click="resetFilters"><i class="fa fa-refresh"></i></button>
                    </div>
                </div>
                <div class="row">
					<div class="col-md-6">
	                    <div class="form-group">
	                    	<label>User</label>
							<input type="text" placeholder="" class="form-control" v-model="filterData.user">
	                    </div>
					</div>
					<div class="col-md-6">
	                    <div class="form-group">
	                    	<label>Season</label>
							<select v-model="filterData.season" class="form-control">
								<option :value="'all'"></option>
								<option :value="'2017/18'">2017/18</option>
								<option :value="'2018/19'">2018/19</option>
								<option :value="'2019/20'">2019/20</option>
								<option :value="'2020/21'">2020/21</option>
								<option :value="'2021/22'">2021/22</option>
							</select>
	                    </div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
	                    <div class="form-group">
	                    	<label>Status</label>
							<select v-model="filterData.status" class="form-control">
								<option :value="'all'"></option>
								<option :value="'Pending'">Pending</option>
								<option :value="'Completed'">Completed</option>
								<option :value="'Rejected'">Rejected</option>
							</select>
	                    </div>
					</div>
					<div class="col-md-6">
	                    <div class="form-group">
	                    	<label>Type</label>
							<select v-model="filterData.type" class="form-control">
								<option :value="'all'"></option>
								<option :value="'Student'">Student</option>
								<option :value="'Alumni'">Alumni</option>
								<option :value="'Staff'">Staff</option>
							</select>
	                    </div>
					</div>
				</div>
            </div>
        </div>

        <div class="ibox">
            <div class="ibox-content">
                <div class="table-responsive">
                    <table class="table table-striped">
                    	<thead>
	                    	<th>Username</th>
	                    	<th>Season</th>
	                    	<th>Type</th>
	                    	<th>Status</th>
	                    	<th>Updated At</th>
	                    	<th>Actions</th>
                    	</thead>
                        <tbody>
                        <tr v-for="datum in filteredData">
                            <td>@{{ datum.user.first_name }} @{{ datum.user.last_name }}</td>
                            <td>@{{ datum.season }}</td>
                            <td>@{{ datum.type }}</td>
                            <td>@{{ datum.status }}</td>
                            <td>@{{ moment(datum.updated_at, "YYYY-MM-DD HH:mm:ss").fromNow() }}</td>
                            <td><a :href="'registrations/' + datum.id" class="btn btn-white btn-xs btn-block"> View</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection


@section('scripts')

<script>

	const contentApp = new Vue ({
		el: '#contentDiv',
		data: {
			data:[],
			filteredData:[],
			filterBoxOpen:false,
			filterData:{},
		},
		computed: {
		},
		watch: {
		},
		methods: {
	        loadData() {
	            let app = this;
	            axios.get('/api/registrations')
	            .then(function (response) {
	                app.data = response.data;
	                app.applyFilters();
	            }).catch(function (error) { showSwalError("Error loading data",error) });
	        },
	        toggleFilters(){
				this.filterBoxOpen = !this.filterBoxOpen;
				this.filterMessage = "";
	        },
	        resetFilters(){
	        	this.filterData = {	
					user:"",
					season:"2019/20",
					status:"all",
					type:"all",
				}		
				this.filterMessage = ""; 	
	        },
	        applyFilters(){
	        	let fd = this.filterData;

				this.filteredData = [];
				for(var i=0; i<this.data.length; i++)
				{
					let datum = this.data[i];
					let username = datum.user.first_name.toUpperCase() + " " + datum.user.last_name.toUpperCase();

					if(username.indexOf(fd.user.toUpperCase()) > -1 &&
						(fd.season == "all" || datum.season == fd.season) &&
						(fd.status == "all" || datum.status == fd.status) &&
						(fd.type == "all" || datum.type == fd.type))
					{
						this.filteredData.push(datum);
					}
				}
	        }
		},
		mounted() {
		},
		created: function () {
			this.resetFilters();
			this.loadData();
		},
		ready: function () {
		}
	});

	showSwalError = function(title,text){
		console.log(text)
        swal({ title: title, text: text, type: "error", showCancelButton: false, allowEscapeKey: false });
	}

	showSwalSuccess = function(title,text){
        console.log(text);
        swal({ title: title, text: text, type: "success", showCancelButton: false, allowEscapeKey: false });
	}


</script>
@endsection