@extends('club.layout')

@section('title', 'Groups')

@section('content')
<div class="row" id="contentDiv">
	<div class="col-lg-8">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
				<div class="row m-b-lg">
					<div class="col-xs-8">
		                <h2>Groups</h2>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-success btn-circle btn-lg pull-right" type="button" data-toggle="modal" data-target="#createGroupModal"><i class="fa fa-plus"></i></button>
                    </div>
                </div>

                <div class="input-group">
					<input type="text" placeholder="Search groups" class="input form-control" v-model="groupSearch">
                    <span class="input-group-btn"><button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i> Search</button></span>
                </div>


                <div class="clients-list">
					<div style="position: relative; height: 540px;">
	                    <div class="full-height-scroll">
	                        <div class="table-responsive">
	                            <table class="table table-striped table-hover">
	                            	<thead>
	                                    <th>ID</th>
	                                    <th>Name</th>
	                                    <th># of Members</th>
	                            	</thead>
	                                <tbody>
		                                <template v-for="group in filteredGroups">
			                                <tr @click="viewGroup(group)">
			                                    <td>@{{ group.id }}</td>
			                                    <td>@{{ group.name }}</td>
			                                    <td>@{{ group.users.length }}</td>
			                                </tr>
		                                </template>
	                                </tbody>
	                            </table>
                    			<span>Groups: @{{ filteredGroups.length }}</span>
	                        </div>
	                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4"  v-show="activeGroup.id != 0">
        <div class="ibox float-e-margins">
            <div class="ibox-content profile-content">
				<div class="row m-b">
					<div class="col-xs-8">
		                <h2>@{{ activeGroup.name }}</h2>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-danger btn-circle btn-lg pull-right " type="button" style="margin-left:5px" data-toggle="modal" data-target="#deleteGroupModal">
		                	<i class="fa fa-times"></i>
		                </button>
		                <button class="btn btn-warning btn-circle btn-lg pull-right" type="button" data-toggle="modal" data-target="#updateGroupModal"><i class="fa fa-edit"></i></button>
                    </div>
                </div>
                <div class="row m-b">
                    <div class="col-xs-12">
                        <h4>Group Members</h4>
                        <div style="position: relative; height: 595px;">
                        	<div class="full-height-scroll">
								<div class="table-responsive">
		                            <table class="table table-striped">
		                                <tbody>
		                                	<tr v-for="user in activeGroup.users">
		                                		<td>@{{user.first_name}} @{{user.last_name}}</td>
		                                	</tr>
		                                	<tr v-IF="activeGroup.users.length == 0">
		                                		<td>This group has no members</td>
		                                	</tr>

		                                </tbody>
		                            </table>
		                        </div>
		                    </div>
		                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="createGroupModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
        	<div class="modal-content animated bounceInTop">
                <div class="modal-header">
                    <h4 class="modal-title">Create Group</h4>
                </div>
                <div class="modal-body">
                 	<div class="form-group" v-if="createGroupValidationMessage != ''">
                    	<h3 class="text-danger text-center">@{{createGroupValidationMessage}}</h3>
                    </div>
                    <div class="form-group">
                    	<label>Name</label>
                    	<input type="text" placeholder="" class="form-control" v-model="createGroup.name">
                    </div>
                    <div class="form-group">
                    	<label>Users</label>
						<div style="position: relative; height: 260px;">
                        	<div class="full-height-scroll">
								<div class="table-responsive">
		                            <table class="table table-striped">
		                                <tbody>
		                                	<tr v-for="user in users">
		                                		<td><div class="checkbox checkbox-primary abc-checkbox abc-checkbox-primary"><input type="checkbox" class="styled" :value="user" v-model="createGroup.users"><label></label></div></td>
		                                		<td>@{{user.first_name}} @{{user.last_name}}</td>
		                                	</tr>
		                                </tbody>
		                            </table>
		                        </div>
		                    </div>
		                </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-white" @click="resetCreateGroup()">Reset</button>
                    <button type="button" class="btn btn-primary" @click="submitCreateGroup()">Submit</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="updateGroupModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
        	<div class="modal-content animated bounceInTop">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Group</h4>
                </div>
                <div class="modal-body">
                 	<div class="form-group" v-if="editGroupValidationMessage != ''">
                    	<h3 class="text-danger text-center">@{{editGroupValidationMessage}}</h3>
                    </div>
                    <div class="form-group">
                    	<label>Name</label>
                    	<input type="text" placeholder="" class="form-control" v-model="editGroup.name">
                    </div>
                    <div class="form-group">
                    	<label>Users</label>
						<div style="position: relative; height: 260px;">
                        	<div class="full-height-scroll">
								<div class="table-responsive">
		                            <table class="table table-striped">
		                                <tbody>
		                                	<tr v-for="user in users">
		                                		<td><div class="checkbox checkbox-primary abc-checkbox abc-checkbox-primary"><input type="checkbox" class="styled" :value="user" v-model="editGroup.users"><label></label></div></td>
		                                		<td>@{{user.first_name}} @{{user.last_name}}</td>
		                                	</tr>
		                                </tbody>
		                            </table>
		                        </div>
		                    </div>
		                </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-white" @click="resetEditGroup()">Reset</button>
                    <button type="button" class="btn btn-primary" @click="submitEditGroup()">Submit</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="deleteGroupModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
        	<div class="modal-content animated bounceInTop">
                <div class="modal-header">
                    <h4 class="modal-title">Delete Group</h4>
                </div>
                <div class="modal-body" style="text-align:center">
                     <p>You are deleting the group <strong>@{{ activeGroup.name }} </strong>.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" @click="submitDeleteGroup()">Submit</button>
                </div>
            </div>
        </div>
    </div>


</div>

<style>
.checkbox, .radio {
    position: relative;
    display: block;
    margin-top: 0px;
    margin-bottom: 0px;
}
</style>
@endsection


@section('scripts')
<script>
	const contentApp = new Vue ({
		el: '#contentDiv',
		data: {
			groups:[],
			filteredGroups:[],
			tablePage:1,
			users:[],
			activeGroup:{
				id:0,
        		name:'',
        		users:[]
			},
			editGroup:{
				id:0,
        		name:'',
        		users:[]
			},
			createGroup:{
        		name:'',
        		users:[]
			},
			groupSearch:"",
			createGroupValidationMessage:"",
			editGroupValidationMessage:"",
		},
		computed: {

		},
		watch: {
		},
		methods: {
	        loadUsers() {
	            let app = this;
	            axios.get('/api/users')
	            .then(function (response) {
	                app.users = [];
	                for(var i=0; i<response.data.length; i++)
            		{
            			app.users.push({
            				id:response.data[i].id,
            				first_name:response.data[i].first_name,
            				last_name:response.data[i].last_name
            			});
            		}
	            }).catch(function (error) { showSwalError("Error loading users",error.response.request.response) });
	        },
	        loadGroups() {
	            let app = this;
	            axios.get('/api/groups')
	            .then(function (response) { 
	            	app.groups = [];
	                for(var i=0; i<response.data.length; i++)
            		{
            			var users = [];
            			for(var j=0; j<response.data[i].users.length; j++)
                		{
                			users.push({
                				id:response.data[i].users[j].id,
                				first_name:response.data[i].users[j].first_name,
                				last_name:response.data[i].users[j].last_name
                			});
                		}
                		app.groups[i] = response.data[i];
                		app.groups[i].users = users;
            		}

	                app.filterGroups();
	                if(app.activeGroup.id != 0)
	                {
	                	for(var i=0; i<app.groups.length; i++)
	                	{
	                		if(app.groups[i].id == app.activeGroup.id)
	                		{
	                			app.viewGroup(app.groups[i]);
	                		}
	                	}
	                }
	            }).catch(function (error) { showSwalError("Error loading groups",error.response.request.response) });
	        },
	        viewGroup(group) {
	            this.activeGroup = group;
				this.resetEditGroup();
	        },
	        submitCreateGroup(){
				if(this.createGroupFormIsValid() == true)
				{
		        	let app = this;
		            axios.post('/api/groups', {
		        		name:app.createGroup.name,
		        		users:app.createGroup.users,
		            })
		            .then(function (response) {
		                app.resetCreateGroup();
						app.loadGroups();
						$('#createGroupModal').modal('hide');
						$('.modal-backdrop').remove();
		                showSwalSuccess("Success","The group has been created");
		            }).catch(function (error) { showSwalError("Error creating group",error.response.request.response) });
	        	}
	        },
	        resetCreateGroup(){
	        	this.createGroup = {
	        		name:'',
	        		users:[],
	        	}
	        	this.createGroupValidationMessage = '';
	        },
	        submitEditGroup(){
	        	if(this.editGroupFormIsValid() == true)
				{
		        	let app = this;
		            axios.put('/api/groups/'+app.editGroup.id, {
		        		id: app.editGroup.id,
		        		name:app.editGroup.name,
		        		users:app.editGroup.users,
		            })
		            .then(function (response) {
		                app.resetEditGroup();
						app.loadGroups();
						$('#updateGroupModal').modal('hide');
						$('.modal-backdrop').remove();
		                showSwalSuccess("Success","The group has been updated");
		            }).catch(function (error) { 
		            	console.log(error); 
		            	showSwalError("Error updating group",error.response.request.response) });
	        	}
	        },
	        resetEditGroup(){
	        	this.editGroup = {
	        		id: this.activeGroup.id,
	        		name:this.activeGroup.name,
	        		users:this.activeGroup.users,
	        	}
	        	this.editGroupValidationMessage = '';
	        },
	        submitDeleteGroup(){
	        	let app = this;
	            axios.delete('/api/groups/'+app.activeGroup.id, {
	            	id:app.activeGroup.id,
	            })
	            .then(function (response) {
	            	app.activeGroup.id = 0;
					app.loadGroups();
					$('#deleteGroupModal').modal('hide');
					$('.modal-backdrop').remove();
	                showSwalSuccess("Success","The group has been deleted");
	            }).catch(function (error) { showSwalError("Error deleting group",error.response.request.response) });
	        },
	        filterGroups(){
				this.filteredGroups = [];
				for(var i=0;i<this.groups.length; i++)
				{
					if(this.groups[i].name.toUpperCase().indexOf(this.groupSearch.toUpperCase()) > -1)
					{
						this.filteredGroups.push(this.groups[i]);
					}
				}
	        },
	        createGroupFormIsValid(){
	        	if(this.createGroup.name == "")
	        	{
	        		this.createGroupValidationMessage = "Please enter a name";
	        		return false;
	        	}

	        	this.createGroupValidationMessage = "";
	        	return true;
	        },
	        editGroupFormIsValid(){
	        	if(this.editGroup.name == "")
	        	{
	        		this.editGroupValidationMessage = "Please enter a name";
	        		return false;
	        	}

	        	this.editGroupValidationMessage = "";
	        	return true;
	        },
		},
		mounted() {
		},
		created: function () {
			this.loadGroups();
			this.loadUsers();
		}
	});

	showSwalError = function(title,text){
		console.log(text)
        swal({ title: title, text: text, type: "error", showCancelButton: false, allowEscapeKey: false });
	}

	showSwalSuccess = function(title,text){
        console.log(text);
        swal({ title: title, text: text, type: "success", showCancelButton: false, allowEscapeKey: false });
	}


</script>
@endsection