@extends('club.layout')

@section('title', 'News')

@section('content')
<div class="m-b-lg row" id="contentDiv" v-cloak v-show="initialised">
	<div class="col-lg-12">

        <div class="ibox float-e-margins">
            <div class="ibox-content">
				<div class="row">
					<div class="col-xs-8">
		                <h2>News</h2>
		                Keep up to date with the latest news and information from around the club
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-success btn-circle btn-lg pull-right m-l" type="button" @click="openCreateBox"><i class="fa fa-plus"></i></button>
		                <button class="btn btn-default btn-circle btn-lg pull-right" type="button" @click="toggleFilters"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>

        <div class="ibox float-e-margins" v-show="filterBoxOpen">
            <div class="ibox-content">

				<div class="row m-b">
					<div class="col-xs-8">
		                <h2>Filter News</h2>
		                <p class="text-danger">Viewing <b>@{{ filteredNews.length }}</b> articles out of <b>@{{ news.length }}.</b></p>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-primary btn-circle btn-lg pull-right m-l" type="button" @click="applyFilters"><i class="fa fa-check"></i></button>
		                <button class="btn btn-default btn-circle btn-lg pull-right" type="button" @click="resetFilters"><i class="fa fa-refresh"></i></button>
                    </div>
                </div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Subject</label>
							<input type="text" placeholder="" class="form-control" v-model="filterData.subject">
						</div>
					</div>
					<div class="col-md-6">
	                    <div class="form-group">
	                    	<label>User</label>
							<input type="text" placeholder="" class="form-control" v-model="filterData.user">
	                    </div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
	                    <label>Date (age)</label>
	                    <div class="form-group">
							<div class="col-md-6" style="padding-left: 0px; padding-right: 5px;">
								<input type="text" placeholder="" class="form-control" v-model="filterData.date_count">
							</div>
							<div class="col-md-6" style="padding-left: 5px; padding-right: 0px;">
								<select v-model="filterData.date_type" class="form-control">
									<option :value="'all'"></option>
									<option :value="'minutes'">Minutes</option>
									<option :value="'hours'">Hours</option>
									<option :value="'days'">Days</option>
									<option :value="'weeks'">Week</option>
									<option :value="'months'">Months</option>
									<option :value="'years'">Years</option>
								</select>
							</div>
	                    </div>
					</div>
					<div class="col-md-6">
	                    <div class="form-group">
	                    	<label>Public</label>
							<select v-model="filterData.public" class="form-control">
								<option :value="'all'"></option>
								<option :value="true">Yes</option>
								<option :value="false">No</option>
							</select>
	                    </div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
	                    <div class="form-group">
	                    	<label>Type</label>
							<select v-model="filterData.type_id" class="form-control">
								<option :value="'all'"></option>
								<option v-for="type in articletypes" :value="type.id">@{{type.name}}</option>
							</select>
	                    </div>
					</div>
					<div class="col-md-6">
	                    <div class="form-group">
	                    	<label>Group</label>
							<select v-model="filterData.group" class="form-control">
								<option :value="'all'"></option>
								<option v-for="group in groups" :value="group.id">@{{group.name}}</option>
							</select>
	                    </div>
					</div>
				</div>

            </div>
        </div>

        <div class="ibox float-e-margins" v-show="createBoxOpen == true">
            <div class="ibox-content">
				<div class="row m-b-lg">
					<div class="col-xs-8">
		                <h2>Create Article</h2>
		                Articles will only appear for users who are in the assigned groups
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-danger btn-circle btn-lg pull-right" type="button" @click="closeCreateBox"><i class="fa fa-times"></i></button>
                    </div>
                </div>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
	                    	<label>Subject</label>
	                    	<input type="text" placeholder="" class="form-control" v-model="createData.subject">
	                    </div>
                    </div>
                </div>
				<div class="row m-b">
					<div class="col-xs-12">
	                    <label>Content</label>
                        <summernote class=form-control name="editor" :model="createData.content" v-on:change="value => { createData.content = value }"></summernote>
						<div class="clearfix"></div>
                    </div>
                </div>
                <div class="row m-b">
					<div class="col-xs-12">
	                    <div class="form-group">
	                    	<label>Type</label>
							<select v-model="createData.type_id" class="form-control">
								<option v-for="type in articletypes" :value="type.id">@{{type.name}}</option>
							</select>
	                    </div>
	                    <div class="form-group">
	                    	<label>Public</label>
							<select v-model="createData.public" class="form-control">
								<option :value="true">Yes</option>
								<option :value="false">No</option>
							</select>
	                    </div>
	                    <div class="form-group">
	                    	<label>Groups</label>
							<div style="height: 150px ;position: relative;">
	                        	<div class="full-height-scroll">
									<div class="table-responsive">
			                            <table class="table table-striped table-hover">
			                                <tbody>
			                                	<tr v-for="group in groups" @click="toggleCreateGroup(group)">
			                                		<td><div class="checkbox checkbox-primary abc-checkbox abc-checkbox-primary"><input type="checkbox" class="styled" :value="group" v-model="createData.groups" style="display: none;"><label></label></div></td>
			                                		<td>@{{group.name}}</td>
			                                	</tr>
			                                </tbody>
			                            </table>
			                        </div>
			                    </div>
			                </div>
	                    </div>
                    </div>
                </div>

				<div class="row m-b" v-show="createArticleValidationMessage != ''">
					<div class="col-xs-12">
	                	<div class="form-group">
	                    	<h3 class="text-danger text-center">@{{createArticleValidationMessage}}</h3>
	                    </div>
                    </div>
                </div>

				<div class="row">
					<div class="col-xs-12">
                        <button class="btn btn-block btn-primary" @click="submitCreateNews">Submit</button>
                    </div>
                </div>
            </div>
        </div>

        <template v-for="article in filteredNews">
        <a :href="'news/' + article.id">
        <div :class="'faq-item faq-item-' + article.type.colour">
            <div class="row">
                <div class="col-md-6">
	               <span class="faq-question">@{{article.subject}}</span>
	               <small>
	                  Posted by
	                  <strong>@{{article.user.first_name}} @{{article.user.last_name}}</strong>
	                  <i class="fa fa-clock-o"></i>
	                  @{{ moment(article.updated_at, "YYYY-MM-DD HH:mm:ss").fromNow() }}
	               </small>
                </div>
				<div class="col-md-4">
					<span class="small font-bold">Groups</span>
					<div class="tag-list">
						<span class="label m-r" v-for="group in article.groups">@{{group.name}}</span>
					</div>
				</div>
				<div class="col-md-2 text-right">
					<span class="small font-bold">Comments</span>
					<br/>
					<span :class="'label label-' + article.type.colour">@{{article.comments.length}}</span>
				</div>
            </div>
        </div>
        </template>

    </div>


</div>

<style>
.checkbox, .radio {
    position: relative;
    display: block;
    margin-top: 0px;
    margin-bottom: 0px;
}
</style>
<link href="/app/css/plugins/summernote/summernote.css" rel="stylesheet">
<link href="/app/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
@endsection


@section('scripts')

<script src="/app/js/plugins/summernote/summernote.min.js"></script>

<script>

    let config = {
  		height: 200,
    }

	const contentApp = new Vue ({
		el: '#contentDiv',
		data: {
			init: { 
				news: false,
				groups: false,
				types: false,
			},
			news:[],
			filteredNews:[],
			groups:[],
			articletypes:[],
			newsSearch:"",
			createBoxOpen:false,
			filterBoxOpen:false,
			createData:{},
			filterData:{},
			createArticleValidationMessage:"",
		},
		computed: {
			initialised: function() {
				return this.init.news && this.init.groups && this.init.types;
			}
		},
		watch: {
		},
		methods: {
	        loadNews() {
	            let app = this;
	            axios.get('/api/articles')
	            .then(function (response) {
	                app.news = response.data;
	                app.applyFilters();
	                app.init.news = true;
	            }).catch(function (error) { showSwalError("Error loading news",error) });
	        },
	        loadGroups() {
	            let app = this;
	            axios.get('/api/groups')
	            .then(function (response) {
	                app.groups = response.data;
	                app.init.groups = true;
	            }).catch(function (error) { showSwalError("Error loading groups",error.response.request.response) });
	        },
	        loadTypes() {
	            let app = this;
	            axios.get('/api/articletypes')
	            .then(function (response) {
	                app.articletypes = response.data;
	                app.init.types = true;
	            }).catch(function (error) { showSwalError("Error loading types",error.response.request.response) });
	        },
	        openCreateBox(){
				this.createBoxOpen = true;
	        },
	        closeCreateBox(){
				this.createBoxOpen = false;
				this.resetCreateData();
	        },
	        resetCreateData(){
	        	this.createData = {	
	        		subject:"",
					content:"",
					type_id:0,
					public:false,
					groups:[],
				}
	        	this.createArticleValidationMessage = "";				
	        },
	        submitCreateNews(){
	        	if(this.createArticleFormIsValid() == true)
				{
		        	let app = this;
		            axios.post('/api/articles', app.createData)
		            .then(function (response) {
		                app.resetCreateData();
						app.loadNews();
						app.closeCreateBox();
		                showSwalSuccess("Success","The article has been created");
		            }).catch(function (error) { showSwalError("Error creating article",error.response.request.response) });
		        }
	        },
	        toggleFilters(){
				this.filterBoxOpen = !this.filterBoxOpen;
				this.filterMessage = "";
	        },
	        resetFilters(){
	        	this.filterData = {	
	        		subject:"",
					user:"",
					type_id:"all",
					public:"all",
					group:"all",
					date_type:"years",
					date_count:1
				}		
				this.filterMessage = ""; 	
	        },
	        toggleCreateGroup(group){
	        	var hit = false;
	        	for(var i=0; i<this.createData.groups.length; i++)
	        	{
	        		if(this.createData.groups[i] == group)
	        		{
	        			this.createData.groups.splice(i,1);
	        			hit = true;
	        		}
	        	}
	        	if(hit == false)
	        	{
	        		this.createData.groups.push(group);
	        	}
	        },
	        createArticleFormIsValid(){
	        	if(this.createData.subject == "")
	        	{
	        		this.createArticleValidationMessage = "Please enter a subject";
	        		return false;
	        	}

	        	if(this.createData.content == "")
	        	{
	        		this.createArticleValidationMessage = "Please enter content";
	        		return false;
	        	}

	        	if(this.createData.type_id == 0)
	        	{
	        		this.createArticleValidationMessage = "Please select a type";
	        		return false;
	        	}

	        	this.createArticleValidationMessage = "";
	        	return true;
	        },
	        applyFilters(){
	        	let fd = this.filterData;

				let momentCheck = this.moment();
				if(fd.date_type != "" && fd.date_count != "")
				{
					momentCheck.subtract(fd.date_count, fd.date_type);
				}

				this.filteredNews = [];
				for(var i=0; i<this.news.length; i++)
				{
					let article = this.news[i];
					let username = article.user.first_name.toUpperCase() + " " + article.user.last_name.toUpperCase();
					let groupIDs = _.map(article.groups, 'id');

					if(article.subject.toUpperCase().indexOf(fd.subject.toUpperCase()) > -1 &&
						username.indexOf(fd.user.toUpperCase()) > -1 &&
						(fd.type_id == "all" || article.articleType_id == fd.type_id) &&
						(fd.public == "all" || article.public == fd.public) &&
						(fd.group == "all" || _.includes(groupIDs, fd.group)) &&
						(fd.date_type == "all" || momentCheck.isBefore(article.updated_at)))
					{
						this.filteredNews.push(article);
					}
				}
	        }
		},
		mounted() {
		},
		created: function () {
			this.resetCreateData();
			this.resetFilters();
			this.loadGroups();
			this.loadTypes();
			this.loadNews();
		},
		ready: function () {
		}
	});

	showSwalError = function(title,text){
		console.log(text)
        swal({ title: title, text: text, type: "error", showCancelButton: false, allowEscapeKey: false });
	}

	showSwalSuccess = function(title,text){
        console.log(text);
        swal({ title: title, text: text, type: "success", showCancelButton: false, allowEscapeKey: false });
	}


</script>
@endsection