@extends('club.layout')

@section('title', 'News')

@section('content')
<div class="m-b-lg row" id="contentDiv" v-cloak v-show="initialised">

	<div class="col-lg-10 col-lg-offset-1" v-if="articleLoaded == true" v-show="!editMode">
		<div :class="'ibox ibox-' + article.type.colour">
		    <div class="ibox-content">
		        <div class="row">
		            <div class="col-md-6">
					<span class="small font-bold m-r">Category:</span><br>
					<span :class="'label label-' + article.type.colour">@{{article.type.name}}</span>
		            </div>
		            <div class="col-md-6">
		                <div class="text-right">
		                	<button :disabled="!canDeleteArticle" class="btn btn-danger btn-circle btn-lg pull-right m-l" type="button" data-toggle="modal" data-target="#deleteArticleModal"><i class="fa fa-times"></i></button>
		                	<button :disabled="!canEditArticle" class="btn btn-warning btn-circle btn-lg pull-right" type="button" @click="toggleEditMode"><i class="fa fa-edit"></i></button>
		                </div>
		            </div>
		        </div>

		        <div class="text-center article-title" style="margin-bottom:40px !important">
		        <span class="text-muted"><i class="fa fa-clock-o"></i> @{{ moment(article.updated_at, "YYYY-MM-DD HH:mm:ss").calendar() }}</span>
		            <h1>
		                @{{ article.subject }}
		            </h1>
		        </div>
		        <div v-html="article.body"></div>
		        <hr>
		        <div class="row">
		            <div class="col-md-6">
		                    <h5>Groups:</h5>
		                    <span class="btn btn-white btn-xs" type="button" v-for="group in article.groups">@{{group.name}}</span>
		            </div>
		            <div class="col-md-6">
		                <div class="text-right">
		                    <div> <i class="fa fa-user"> </i> Posted by <b>@{{ article.user.first_name }} @{{ article.user.last_name }}</b></div>
		                </div>
		            </div>
		        </div>

		        <div class="row m-t">
		            <div class="col-lg-6">
		                <h2>Comments:</h2>
		            </div>
	            	<div class="col-lg-6">
				        <div class="pull-right">
		                    <div> <i class="fa fa-comments-o"> </i> @{{ article.comments.length }} comments </div>
				        </div>
	                </div>
	            </div>



		        <div class="row">
		            <div class="col-lg-12">
		                <div class="social-feed-box" v-for="comment in article.comments">
		                    <div class="pull-right social-action dropdown" v-show="permissions.articlecomments_admin == true || (auth_user.id == comment.user_id && permissions.articlecomments_destroy == true)">
			                    <button class="btn btn-danger btn-circle btn-xs pull-right" type="button" @click="deleteComment(comment)"><i class="fa fa-times"></i></button>
			                </div>
		                    <div class="social-avatar">
		                        <a href="" class="pull-left">
		                            <img alt="image" :src="comment.user.avatarPath">
		                        </a>
		                        <div class="media-body">
		                            <a :href="'/club/users/' + comment.user_id">
		                                @{{ comment.user.first_name }}  @{{ comment.user.last_name }}
		                            </a>
		                            <small class="text-muted">@{{ moment(comment.updated_at, "YYYY-MM-DD HH:mm:ss").calendar() }}</small>

		                        </div>
		                    </div>
		                    <div class="social-body">
		                        <p>
		                            @{{ comment.comment }}
		                        </p>
		                    </div>
		                </div>
		            </div>
		        </div>
		        <hr>
		        <div class="row">
		            <div class="col-lg-12">
		                <textarea class="form-control" rows="3" placeholder="Enter comment here....." v-model="commentData"  :disabled="!canCreateComment"></textarea>
		            </div>
		        </div>
		        <div class="row">
					<div class="col-lg-12">
						<button class="btn btn-success m-t" type="submit" @click="createComment" :disabled="!canCreateComment || commentData == ''">Submit</button>
					</div>
				</div>
		    </div>
		</div>
    </div>

    <div class="ibox float-e-margins" v-show="editMode">
            <div class="ibox-content">
				<div class="row m-b-lg">
					<div class="col-xs-8">
		                <h2>Edit Article</h2>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-danger btn-circle btn-lg pull-right" type="button" @click="toggleEditMode"><i class="fa fa-times"></i></button>
                    </div>
                </div>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
	                    	<label>Subject</label>
	                    	<input type="text" placeholder="" class="form-control" v-model="editData.subject">
	                    </div>
                    </div>
                </div>
				<div class="row m-b">
					<div class="col-xs-12">
	                    <label>Content</label>
                        <summernote class=form-control name="editor" :model="editData.content" v-on:change="value => { editData.content = value }" ref="myEditor"></summernote>
						<div class="clearfix"></div>
                    </div>
                </div>
                <div class="row m-b">
					<div class="col-xs-12">
	                    <div class="form-group">
	                    	<label>Type</label>
							<select v-model="editData.type_id" class="form-control">
								<option v-for="type in articletypes" :value="type.id">@{{type.name}}</option>
							</select>
	                    </div>
	                    <div class="form-group">
	                    	<label>Public</label>
							<select v-model="editData.public" class="form-control">
								<option :value="true">Yes</option>
								<option :value="false">No</option>
							</select>
	                    </div>
	                    <div class="form-group">
	                    	<label>Groups</label>
							<div style="height: 150px ;position: relative;">
	                        	<div class="full-height-scroll">
									<div class="table-responsive">
			                            <table class="table table-striped table-hover">
			                                <tbody>
			                                	<tr v-for="group in groups" @click="toggleEditGroup(group)">
			                                		<td><div class="checkbox checkbox-primary abc-checkbox abc-checkbox-primary"><input type="checkbox" class="styled" :value="group" v-model="editData.groups" style="display: none;"><label></label></div></td>
			                                		<td>@{{group.name}}</td>
			                                	</tr>
			                                </tbody>
			                            </table>
			                        </div>
			                    </div>
			                </div>
	                    </div>
                    </div>
                </div>

				<div class="row">
					<div class="col-xs-12">
                        <button class="btn btn-block btn-primary" @click="submitEditArticle">Submit</button>
                    </div>
                </div>
            </div>
        </div>


    <div class="modal inmodal" id="deleteArticleModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
        	<div class="modal-content animated bounceInTop">
                <div class="modal-header">
                    <h4 class="modal-title">Delete Article</h4>
                </div>
                <div class="modal-body" style="text-align:center">
                     <p>You are deleting the article <strong>@{{ article.subject }} </strong>.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" @click="submitDeleteArticle()">Submit</button>
                </div>
            </div>
        </div>
    </div>


</div>



<style>
.checkbox, .radio {
    position: relative;
    display: block;
    margin-top: 0px;
    margin-bottom: 0px;
}
</style>
<link href="/app/css/plugins/summernote/summernote.css" rel="stylesheet">
<link href="/app/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
@endsection


@section('scripts')

<script src="/app/js/plugins/summernote/summernote.min.js"></script>

<script>

	const contentApp = new Vue ({
		el: '#contentDiv',
		data: {
			init: { 
				news: false,
				types: false,
				groups: false,
				auth: false,
			},
			article:{},
			groups:[],
			articletypes:[],
			articleLoaded:false,
			commentData:"",
			editMode:false,
			permissions:{
				articles:false,
				articles_show:false,
				articles_create:false,
				articles_edit:false,
				articles_destroy:false,
				articles_administrate:false,
				articlecomments_admin:false,
				articlecomments_create:false,
				articlecomments_edit:false,
				articlecomments_destroy:false,
			},
			editData:{
	        		subject:"",
					content:"",
					type_id:0,
					public:false,
					groups:[],
			},
			auth_user:{},
		},
		computed: {
			initialised: function() {
				return this.init.news && this.init.groups && this.init.auth && this.init.types;
			},
			canDeleteArticle: function () {
		      return (this.permissions.articles_administrate == true || (this.permissions.articles_destroy == true && this.auth_user.id == this.article.user_id))
		    },
			canEditArticle: function () {
		      return (this.permissions.articles_administrate == true || (this.permissions.articles_edit == true && this.auth_user.id == this.article.user_id))
		    },
			canCreateComment: function () {
		      return (this.permissions.articlecomments_admin == true || this.permissions.articlecomments_create == true )
		    },
			canEditAnyComment: function () {
		      return (this.permissions.articlecomments_admin == true)
		    },
			canDeleteAnyComment: function () {
		      return (this.permissions.articlecomments_admin == true)
		    },
		},
		watch: {
		},
		methods: {
			loadGroups() {
	            let app = this;
	            axios.get('/api/groups')
	            .then(function (response) {
	                app.groups = response.data;
	                app.init.groups = true;
	            }).catch(function (error) { showSwalError("Error loading groups",error) });
	        },
	        loadTypes() {
	            let app = this;
	            axios.get('/api/articletypes')
	            .then(function (response) {
	                app.articletypes = response.data;
	                app.init.types = true;
	            }).catch(function (error) { showSwalError("Error loading types",error) });
	        },
	        loadArticle() {
	            let app = this;
	            let pathname = window.location.pathname.split("/");
	            let path = "/api/articles/" + pathname[pathname.length-1]
	            axios.get(path)
	            .then(function (response) {
	                app.article = response.data;
	                app.articleLoaded = true;
	                app.init.news = true;
	            }).catch(function (error) { showSwalError("Error loading article",error) });
	        },
	        loadAuthUser() {
	        	let app = this;
	        	axios.get('/api/users/auth_data')
	            .then(function (response) {
	            	app.auth_user = response.data;
	                let perms = _.map(response.data.role.permissions, 'name');
	                for(var i=0; i<perms.length; i++)
	                {
	                	if(app.permissions[perms[i]] != null)
	                	{
	                		app.permissions[perms[i]] = true;
	                	}
	                }
	                app.init.auth = true;
	            }).catch(function (error) { showSwalError("Error loading user permissions",error) });
	        },
	        submitDeleteArticle() {
				let app = this;
	            axios.delete('/api/articles/'+app.article.id)
	            .then(function (response) {
	                showSwalSuccess("Success","The article has been deleted");
					window.location.href = '/club/news';
	            }).catch(function (error) { showSwalError("Error deleting article",error) });
	        },
	        submitEditArticle() {
				let app = this;
	            axios.put('/api/articles/'+app.article.id, {
	        		id: app.article.id,
	        		subject:app.editData.subject,
	        		content:app.editData.content,
	        		type_id:app.editData.type_id,
	        		public:app.editData.public,
	        		groups:app.editData.groups,
	            })
	            .then(function (response) {
	                app.loadArticle();
	                app.editMode = false;
	                showSwalSuccess("Success","The article has been updated");
	            }).catch(function (error) { showSwalError("Error updating article",error) });
	        },
	        toggleEditMode() {
	        	this.editData = {
	        		subject:this.article.subject,
					content:this.article.body,
					type_id:this.article.articleType_id,
					public:Boolean(this.article.public),
					groups:this.article.groups,
				};
				this.$refs.myEditor.run('code', this.editData.content);
	        	this.editMode = !this.editMode;


	        },
	        toggleEditGroup(group){
	        	var hit = false;
	        	for(var i=0; i<this.editData.groups.length; i++)
	        	{
	        		if(this.editData.groups[i].id == group.id)
	        		{
	        			this.editData.groups.splice(i,1);
	        			hit = true;
	        		}
	        	}
	        	if(hit == false)
	        	{
	        		this.editData.groups.push(group);
	        	}
	        },
	        createComment() {
	        	let app = this;
	            axios.post('/api/articlecomments', {
	        		comment:app.commentData,
	        		article_id:app.article.id,
	            })
	            .then(function (response) {
	                app.commentData = "";
					app.loadArticle();
	                showSwalSuccess("Success","Your comment has been added");
	            }).catch(function (error) { showSwalError("Error creating comment",error) });
	        },
	        deleteComment(comment) {
	        	
	        	let app = this;

	        	swal({
					title: "Are you sure?",
					text: "You will not be able to recover this comment!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Delete!",
					cancelButtonText: "Cancel!",
					closeOnConfirm: false,
					closeOnCancel: true
				},
				function(isConfirm){
					if (isConfirm) 
					{
						axios.delete('/api/articlecomments/'+comment.id)
			            .then(function (response) {
			                showSwalSuccess("Success","The comment has been deleted");
							app.loadArticle();
			            }).catch(function (error) { showSwalError("Error deleting comment",error) });
					}
				});
	        	
	        }

		},
		mounted() {
		},
		created: function () {
			this.loadGroups();
			this.loadTypes();
			this.loadArticle();
			this.loadAuthUser();
		},
		ready: function () {
			
		}
	});

	showSwalError = function(title,text){
		console.log(text)
        swal({ title: title, text: text, type: "error", showCancelButton: false, allowEscapeKey: false });
	}

	showSwalSuccess = function(title,text){
        console.log(text);
        swal({ title: title, text: text, type: "success", showCancelButton: false, allowEscapeKey: false });
	}


</script>
@endsection