@extends('club.layout')

@section('title', 'Configuration')

@section('content')
	<div class="row" id="contentDiv" v-cloak>
	 	<div class="ibox float-e-margins">
            <div class="ibox-content">
				<div class="row m-b">
					<div class="col-xs-8">
		                <h2>Application Configuration</h2>
		                <p class="text-danger">Warning! Don't fuck with this stuff unless you know what you're doing!</b></p>
                    </div>
                </div>
				<div class="row">
					<div class="col-xs-12">
						<template v-for="datum in data">
				            <div class="form-group">
				            	<label>@{{datum.name}}</label>
				            	<input type="text" placeholder="" class="form-control" v-model="datum.value">
				            </div>
						</template>
                    </div>
				</div>
				<div class="row">
                    <button type="button" class="btn btn-primary btn-block" @click="updateConfiguration()">Submit</button>
				</div>
            </div>
        </div>
	</div>
@endsection


@section('scripts')
<script>
	const contentApp = new Vue ({
		el: '#contentDiv',
		data: {
			data:[],
		},
		computed: {
		},
		watch: {
		},
		methods: {
	        loadData() {
	            let app = this;
	            axios.get('/api/configuration')
	            .then(function (response) {
	                app.data = response.data;
	            }).catch(function (error) { showSwalError("Error loading data",error) });
	        },
	        updateConfiguration() {
	        	let app = this;
	            axios.put('/api/configuration/all', {
	        		data: app.data,
	            })
	            .then(function (response) {
					app.loadData();
	                showSwalSuccess("Success","The configuration has been updated");
	            }).catch(function (error) { 
	            	console.log(error); 
	            	showSwalError("Error updating configuration",error.response.request.response)
	            });
	        }
		},
		mounted() {
		},
		created: function () {
			this.loadData();
		},
		ready: function () {
		}
	});

	showSwalError = function(title,text){
		console.log(text)
        swal({ title: title, text: text, type: "error", showCancelButton: false, allowEscapeKey: false });
	}

	showSwalSuccess = function(title,text){
        console.log(text);
        swal({ title: title, text: text, type: "success", showCancelButton: false, allowEscapeKey: false });
	}


</script>
@endsection