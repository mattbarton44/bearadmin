@extends('club.layout')

@section('title', 'Shop')

@section('content')
<div class="m-b-lg row" id="contentDiv">
	<div class="col-lg-12">
        <div class="ibox product-detail" v-if="productLoaded == true && product.available == true">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-md-4">
                        <div class="product-images">
                            <div>
                                <div class="">
		                        	<img style="width:240px;" :src="'/images/shop/' + product.category + '.png'">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <h2 class="font-bold m-b-xs">
                            @{{ product.name }}
                        </h2>
                        <div class="m-t-md">
                            <h2 class="product-main-price">£@{{product.price / 100}}</h2>
                        </div>
                        <hr>
                        <h4>Product description</h4>
                        <div class="small text-muted" v-html="product.description"></div>
                    </div>
                </div>
                <hr>


                <form id="myForm" name="myForm">
                <div class="row" v-if="product.form.length > 0">
                    <div class="col-md-12">
                    	<div class="row" v-for="item in product.form">
							<div class="col-xs-12">
								<div class="form-group">
			                    	<label>@{{item.label}}</label>


			                    	<template v-if="item.type == 'input'">
			                    		<input type="text" :placeholder="item.placeholder" class="form-control" v-model="item.value" :name="item.label">
			                    	</template>


			                    	<template v-if="item.type == 'select'">
										<select v-model="item.value" class="form-control" :name="item.label">
											<option v-for="option in item.options" :value="option">@{{option}}</option>
										</select>
			                    	</template>


			                    	<template v-if="item.type == 'image'">
								        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
								            <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
								            <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" @change="imageChange(item, $event)" :name="item.label"></span>
								            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput" @click="imageRemove(item)">Remove</a>
								        </div>
			                    	</template>


			                    	<template v-if="item.type == 'label'">
										<p class="form-control-static">@{{ item.placeholder }}</p>
			                    	</template>

			                    	<span v-if="item.help_text != ''" class="help-block m-b-none">@{{ item.help_text }}</span>
			                    </div>
		                    </div>
		                </div>
                    </div>
                </div>
                <hr>

                <div class="row">
                    <div class="col-md-12">
                    	<button @click.prevent="submitForm" class="btn btn-primary btn-block">Submit Order</button>
		            </div>
		        </div>


                </form>



            </div>
        </div>



    </div>
</div>


@endsection


@section('scripts')

<link href="/app/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<script src="/app/js/plugins/jasny/jasny-bootstrap.min.js"></script>
 
<script>

	const contentApp = new Vue ({
		el: '#contentDiv',
		data: {
			product:{},
			auth_user:{},
			productLoaded:false,
		},
		methods: {
	        loadProduct() {
	            let app = this;
	            let pathname = window.location.pathname.split("/");
	            let path = "/api/products/" + pathname[pathname.length-1]
	            axios.get(path)
	            .then(function (response) {
	                app.product = response.data;
	                app.product.form = JSON.parse(response.data.form);
	                app.productLoaded = true;

	            }).catch(function (error) { showSwalError("Error loading product",error) });
	        },
	        loadAuthUser() {
	        	let app = this;
	        	axios.get('/api/users/auth_data')
	            .then(function (response) {
	            	app.auth_user = response.data;
	            }).catch(function (error) { showSwalError("Error loading user data",error) });
	        },
	        imageChange(item, event) {
	            item.value = event.target.value;
	        },
	        imageRemove(item) {
	            item.value = "";
	        },
	        submitForm() {
				swal({
				  title: "Processing",
				  text: "Your order is being processed",
				  type: "info",
				  showCancelButton: false,
				  showConfirmButton: false,
				  closeOnConfirm: false,
				  showLoaderOnConfirm: true,
				});

				let myForm = document.getElementById('myForm');
				let formData = new FormData(myForm);
				formData.append('product_id', this.product.id);

	        	axios.post('/api/orders', formData, { headers: { 'Content-Type': 'multipart/form-data' }
	        	}).then(function (response) 
	        	{
					if(response.data.error_message != null)
	                {
	                	showSwalError("Error", response.data.error_message)
	                }
	                else
	                {
	                	swal({
							title: "Success",
							text: "Your order was successful!",
							type: "success",
							showCancelButton: false,
							closeOnConfirm: false,
						},
						function(isConfirm){
							if (isConfirm) 
							{
			                	window.location.href = window.location.origin + "/club/shop";
							}
						});
	                }

	            }).catch(function (error) { showSwalError("Error",error) });


	        },
		},
		created: function () {
			this.loadProduct();
			this.loadAuthUser();
		},
	});

	showSwalError = function(title,text){
		console.log(text)
        swal({ title: title, text: text, type: "error", showCancelButton: false, allowEscapeKey: false });
	}

	showSwalSuccess = function(title,text){
        console.log(text);
        swal({ title: title, text: text, type: "success", showCancelButton: false, allowEscapeKey: false });
	}


</script>
@endsection