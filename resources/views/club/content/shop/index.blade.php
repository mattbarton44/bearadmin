@extends('club.layout')

@section('title', 'Shop')

@section('content')
<div class="m-b-lg row" id="contentDiv" v-cloak v-show="initialised">
	<div class="col-lg-12">

        <div class="ibox float-e-margins">
            <div class="ibox-content">
				<div class="row">
					<div class="col-xs-8">
		                <h2>Shop</h2>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-default btn-circle btn-lg pull-right" type="button" @click="toggleFilters"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>

        <div class="ibox float-e-margins" v-show="filterBoxOpen">
            <div class="ibox-content">
				<div class="row m-b">
					<div class="col-xs-8">
		                <h2>Filter Products</h2>
		                <p class="text-danger">Viewing <b>@{{ filteredProducts.length }}</b> products out of <b>@{{ products.length }}.</b></p>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-primary btn-circle btn-lg pull-right m-l" type="button" @click="applyFilters"><i class="fa fa-check"></i></button>
		                <button class="btn btn-default btn-circle btn-lg pull-right" type="button" @click="resetFilters"><i class="fa fa-refresh"></i></button>
                    </div>
                </div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Name</label>
							<input type="text" placeholder="" class="form-control" v-model="filterData.name">
						</div>
					</div>
					<div class="col-md-6">
	                    <div class="form-group">
	                    	<label>Category</label>
	                    	<select v-model="filterData.category" class="form-control">
								<option :value="'all'"></option>
								<option :value="'BUIHA Registration'">BUIHA Registration</option>
								<option :value="'Ice Fees'">Ice Fees</option>
								<option :value="'Kit'">Kit</option>
								<option :value="'Tickets'">Tickets</option>
								<option :value="'Other'">Other</option>
							</select>
	                    </div>
					</div>
				</div>
            </div>
        </div>

        <div class="flex-container">
	        <div class="flex-item" v-for="product in filteredProducts">
	        	<a :href="'shop/' + product.id">
		            <div class="ibox">
		                <div class="ibox-content product-box">
		                    <div class="">
		                        <img style="width:280px;" :src="'/images/shop/' + product.category + '.png'">
		                    </div>
		                    <div class="product-desc">
		                        <span class="ba-price">
		                            £@{{ (product.price/100).toFixed(2) }}
		                        </span>
		                        <small class="text-muted">@{{ product.category }}</small>
		                        <a href="#" class="product-name"> @{{ product.name }}</a>

	                        </div>
		                </div>
		            </div>
	            </a>
	        </div>

        </div>

    </div>


</div>

<style>
.checkbox, .radio {
    position: relative;
    display: block;
    margin-top: 0px;
    margin-bottom: 0px;
}

.flex-container {
  padding: 0;
  margin: 0;
  list-style: none;
  
  display: flex;
  flex-flow: row wrap;
  justify-content: flex-start;
}

.flex-item {
  padding: 10px;
  width: 300px;
  
}

.ba-price{
    font-size: 14px;
    font-weight: 600;
    color: #ffffff;
    background-color: #323150;
    padding: 6px 12px;
    position: absolute;
    top: -32px;
    left: 0;
}

</style>

@endsection


@section('scripts')

<script>

	const contentApp = new Vue ({
		el: '#contentDiv',
		data: {
			init: { 
				products: false,
			},
			products:[],
			filteredProducts:[],
			filterBoxOpen:false,
			filterData:{},
			filterMessage:""
		},
		computed: {
			initialised: function() {
				return this.init.products;
			}
		},
		methods: {
	        loadProducts() {
	            let app = this;
	            axios.get('/api/products/shop')
	            .then(function (response) {
	                app.products = response.data;
	                app.applyFilters();
	                app.init.products = true;
	            }).catch(function (error) { showSwalError("Error loading products",error) });
	        },
	        toggleFilters(){
				this.filterBoxOpen = !this.filterBoxOpen;
				this.filterMessage = "";
	        },
	        resetFilters(){
	        	this.filterData = {	
	        		name:"",
					category:"all",
				}		
				this.filterMessage = ""; 	
	        },
	        applyFilters(){
	        	let fd = this.filterData;

				this.filteredProducts = [];
				for(var i=0; i<this.products.length; i++)
				{
					let product = this.products[i];
					if( product.name.toUpperCase().indexOf(fd.name.toUpperCase()) > -1 && (fd.category == "all" || product.category == fd.category) )
					{
						this.filteredProducts.push(product);
					}
				}
	        }
		},
		created: function () {
			this.resetFilters();
			this.loadProducts();
		},
	});

	showSwalError = function(title,text){
		console.log(text)
        swal({ title: title, text: text, type: "error", showCancelButton: false, allowEscapeKey: false });
	}

	showSwalSuccess = function(title,text){
        console.log(text);
        swal({ title: title, text: text, type: "success", showCancelButton: false, allowEscapeKey: false });
	}


</script>
@endsection