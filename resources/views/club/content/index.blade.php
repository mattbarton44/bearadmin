@extends('club.layout')

@section('title', 'Index')

@section('content')
<div class="m-b-lg row" id="contentDiv" v-cloak v-show="initialised">
	<div class="col-lg-12">
		
		<div class="m-b">
			<div :class="regStatusClass">
	            <div class="row vertical-align">
	                <div class="col-12">
	                    <h2 class="font-bold">@{{ reg.message }}</h2>
	                </div>
	            </div>
	        </div>
		</div>

        <div class="ibox">
            <div class="ibox-content">
				<div class="row m-b">
					<div class="col-xs-8">
		                <h2>Your Game Jobs</h2>
                    </div>
					<div class="col-xs-4">
		                <a class="btn btn-success pull-right m-l" type="button" href="/club/games">Sign Up</a>
                    </div>
                </div>
                <div class="table-responsive">
					<table class="table table-bordered table-striped">
                    	<thead>
                    		<tr>
                    			<td>Game</td>
                    			<td>Date</td>
                    			<td>Role</td>
                    			<td>Previous Experience</td>
                    			<td>Signed up at</td>
                    			<td>Attended</td>
                    		</tr>
                    	</thead>
                        <tbody>
                        	<tr v-for="job in gameJobs">
                        		<td>@{{ job.game.homeTeam }} v @{{ job.game.awayTeam }}</td>
                        		<td>@{{ moment(job.game.event.start, "YYYY-MM-DD HH:mm:ss").format('MMMM Do YYYY, HH:mm') }} - @{{ moment(job.game.event.end, "YYYY-MM-DD HH:mm:ss").format('HH:mm') }}</td>
                        		<td>@{{ job.role }}</td>
                        		<td v-if="job.prevExp == true">Yes</td>
                        		<td v-else>No</td>
                        		<td>@{{ moment(job.created_at, "YYYY-MM-DD HH:mm:ss").format('MMMM Do YYYY, HH:mm') }}</td>
                        		<td v-if="job.attended == 1">Yes</td>
                        		<td v-else>No</td>
                        	</tr>
                        </tbody>
                    </table>	
                </div>
            </div>
        </div>



    </div>
</div>

<style>
.checkbox, .radio {
    position: relative;
    display: block;
    margin-top: 0px;
    margin-bottom: 0px;
}
</style>

@endsection


@section('scripts')

<script>

	const contentApp = new Vue ({
		el: '#contentDiv',
		data: {
			init: { 
				gameJobs: false,
				reg: false,
			},
			gameJobs:[],
			reg:{
				message: null,
				type: null
			},
		},
		computed: {
			initialised: function() {
				return this.init.gameJobs;
			},
			regStatusClass: function() {
				if(this.reg.type == 'Complete')
				{
					return "widget label-Training text-center";
				}
				else if(this.reg.type == 'Pending')
				{
					return "widget label-Social text-center";
				}
				else
				{
					return "widget label-Committee text-center";
				}
			}
		},
		methods: {
	        loadGameJobs() {
	            let app = this;
	            axios.get('/api/gamejobs/my_jobs')
	            .then(function (response) {
	                app.gameJobs = response.data;
	                app.init.gameJobs = true;
	            }).catch(function (error) { showSwalError("Error loading game jobs",error) });
	        },
	        loadReg() {
	        	let app = this;
	            axios.get('/api/registrations/my_registration')
	            .then(function (response) {
	            	if(response.data != null)
	            	{
	            		app.reg = {
	                		message: response.data,
	                		type: response.data.split(" ")[response.data.split(" ").length-1]
	                	}
	            	}
	            	else
	            	{
	            		app.reg = {
	                		message: "",
	                		type: ""
	                	}
	            	}	                
	                app.init.reg = true;
	            }).catch(function (error) { showSwalError("Error loading registration",error) });
	        }
	    },
		created: function () {
			this.loadGameJobs();
			this.loadReg();
		},
	});

	showSwalError = function(title,text){
		console.log(text)
        swal({ title: title, text: text, type: "error", showCancelButton: false, allowEscapeKey: false });
	}

	showSwalSuccess = function(title,text){
        console.log(text);
        swal({ title: title, text: text, type: "success", showCancelButton: false, allowEscapeKey: false });
	}


</script>
@endsection