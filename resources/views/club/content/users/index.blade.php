@extends('club.layout')

@section('title', 'Users')

@section('content')
<div class="row" id="contentDiv">
	<div class="col-lg-8">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
				<div class="row m-b-lg">
					<div class="col-xs-8">
		                <h2>Users</h2>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-success btn-circle btn-lg pull-right" type="button" data-toggle="modal" data-target="#createUserModal"><i class="fa fa-plus"></i></button>
                    </div>
                </div>

                <div class="input-group">
					<input type="text" placeholder="Search users" class="input form-control" v-model="userSearch">
                    <span class="input-group-btn"><button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i> Search</button></span>
                </div>
                <div class="clients-list">
		            <div style="position: relative; height: 540px;">
	                    <div class="full-height-scroll">
	                        <div class="table-responsive">
	                            <table class="table table-striped table-hover">
	                            	<thead>
	                                    <th>ID</th>
	                                    <th>First Name</th>
	                                    <th>Last Name</th>
	                                    <th>Role</th>
	                                    <th>Email Address</th>
	                            	</thead>
	                                <tbody>
		                                <template v-for="user in filteredUsers">
			                                <tr @click="viewUser(user)">
			                                    <td>@{{ user.id }}</td>
			                                    <td>@{{ user.first_name }}</td>
			                                    <td>@{{ user.last_name }}</td>
			                                    <td v-if="user.role != null">@{{ user.role.name }}</td>
			                                    <td v-else></td>
			                                    <td>@{{ user.email }}</td>
			                                </tr>
		                                </template>
	                                </tbody>
	                            </table>
                    			<span>Users: @{{ filteredUsers.length }}</span>
	                        </div>
	                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4"  v-if="activeUser.id != 0">
        <div class="ibox float-e-margins">
            <div class="ibox-content profile-content">
				<div class="row m-b">
					<div class="col-xs-8">
		                <h2>@{{ activeUser.first_name }} @{{ activeUser.last_name }}</h2>
                    </div>
					<div class="col-xs-4">
		                <button class="btn btn-danger btn-circle btn-lg pull-right " type="button" style="margin-left:5px" data-toggle="modal" data-target="#deleteUserModal">
		                	<i class="fa fa-times"></i>
		                </button>
		                <button class="btn btn-warning btn-circle btn-lg pull-right" type="button" data-toggle="modal" data-target="#updateUserModal"><i class="fa fa-edit"></i></button>
                    </div>
                </div>
                <img v-if="activeUser.avatarPath != null" alt="Avatar" class="img-responsive" :src="activeUser.avatarPath">
                <div class="form-group">
                	<label>Email Address</label>
        			<p class="form-control" >@{{ activeUser.email }}</p>
                </div>
                <div class="form-group">
                	<label>Role</label>
        			<p class="form-control" v-if="activeUser.role != null">@{{ activeUser.role.name }}</p>
                	<p v-else class="form-control text-warning">No role assigned</p>
                </div>
                <div class="form-group">
                	<label>Last Login</label>
        			<p class="form-control" >@{{ activeUser.last_login }}</p>
                </div>
                <div class="form-group">
                	<label>Groups</label>
	                <p class="form-control" v-for="group in activeUser.groups">@{{ group.name }}</p>
	                <p class="form-control text-warning" v-if="activeUser.groups.length == 0">User is not part of any groups</p>
                </div>
                <hr>
                <div class="user-button">
                    <div class="row">
                        <div class="col-md-6">
                            <button type="button" class="btn btn-primary btn-sm btn-block" data-toggle="modal" data-target="#modifyGroupsModal"></i> Modify Groups</button>
                        </div>
                        <div class="col-md-6">
                            <a type="button" class="btn btn-info btn-sm btn-block" :href="'/club/users/'+activeUser.id "></i> View Profile</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="createUserModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
        	<div class="modal-content animated bounceInTop">
                <div class="modal-header">
                    <h4 class="modal-title">Create User</h4>
                </div>
                <div class="modal-body">
                 	<div class="form-group" v-if="createUserValidationMessage != ''">
                    	<h3 class="text-danger text-center">@{{createUserValidationMessage}}</h3>
                    </div>
                    <div class="form-group">
                    	<label>First Name</label>
                    	<input type="text" placeholder="" class="form-control" v-model="createUser.first_name">
                    </div>
                    <div class="form-group">
                    	<label>Last Name</label>
                    	<input type="text" placeholder="" class="form-control" v-model="createUser.last_name">
                    </div>
                    <div class="form-group">
                    	<label>Email Address</label>
                    	<input type="email" placeholder="" class="form-control" v-model="createUser.email">
                    </div>
                    <div class="form-group">
                    	<label>Role</label>
						<select v-model="createUser.role_id" class="form-control">
							<option v-for="role in roles" :value="role.id">@{{role.name}}</option>
						</select>
                    </div>
                    <div class="form-group">
                    	<label>Password</label>
                    	<input type="password" placeholder="" class="form-control" v-model="createUser.password">
                    </div>
                    <div class="form-group">
                    	<label>Password Confirmation</label>
                    	<input type="password" placeholder="" class="form-control" v-model="createUser.password_confirmation">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-white" @click="resetCreateUser()">Reset</button>
                    <button type="button" class="btn btn-primary" @click="submitCreateUser()">Submit</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="updateUserModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
        	<div class="modal-content animated bounceInTop">
                <div class="modal-header">
                    <h4 class="modal-title">Edit User</h4>
                </div>
                <div class="modal-body">
                 	<div class="form-group" v-if="editUserValidationMessage != ''">
                    	<h3 class="text-danger text-center">@{{editUserValidationMessage}}</h3>
                    </div>
                    <div class="form-group">
                    	<label>First Name</label>
                    	<input type="text" placeholder="" class="form-control" v-model="editUser.first_name">
                    </div>
                    <div class="form-group">
                    	<label>Last Name</label>
                    	<input type="text" placeholder="" class="form-control" v-model="editUser.last_name">
                    </div>
                    <div class="form-group">
                    	<label>Email Address</label>
                    	<input type="email" placeholder="" class="form-control" v-model="editUser.email">
                    </div>
                    <div class="form-group">
                    	<label>Role</label>
						<select v-model="editUser.role_id" class="form-control">
							<option v-for="role in roles" :value="role.id">@{{role.name}}</option>
						</select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-white" @click="resetEditUser()">Reset</button>
                    <button type="button" class="btn btn-primary" @click="submitEditUser()">Submit</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="deleteUserModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
        	<div class="modal-content animated bounceInTop">
                <div class="modal-header">
                    <h4 class="modal-title">Delete User</h4>
                </div>
                <div class="modal-body" style="text-align:center">
                     <p>You are deleting the user <strong>@{{ activeUser.first_name }} @{{ activeUser.last_name }}</strong>.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" @click="submitDeleteUser()">Submit</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal inmodal" id="modifyGroupsModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
        	<div class="modal-content animated bounceInTop">
                <div class="modal-header">
                    <h4 class="modal-title">@{{ activeUser.first_name }} @{{ activeUser.last_name }} - Modify Groups</h4>
                </div>
				<div class="modal-body">
                    <div class="form-group">
                    	<label>Groups</label>
						<div style="position: relative; height: 260px;">
                        	<div class="full-height-scroll">
								<div class="table-responsive">
		                            <table class="table table-striped">
		                                <tbody>
		                                	<tr v-for="group in groups">
		                                		<td><div class="checkbox checkbox-primary abc-checkbox abc-checkbox-primary"><input type="checkbox" class="styled" :value="group" v-model="editUser.groups"><label></label></div></td>
		                                		<td>@{{group.name}}</td>
		                                	</tr>
		                                </tbody>
		                            </table>
		                        </div>
		                    </div>
		                </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-white" @click="resetEditUser()">Reset</button>
                    <button type="button" class="btn btn-primary" @click="submitModifyGroups()">Submit</button>
                </div>
            </div>
            </div>
        </div>
    </div>


</div>

<style>
.checkbox, .radio {
    position: relative;
    display: block;
    margin-top: 0px;
    margin-bottom: 0px;
}
</style>
@endsection


@section('scripts')
<script>
	const contentApp = new Vue ({
		el: '#contentDiv',
		data: {
			users:[],
			filteredUsers:[],
			tablePage:1,
			roles:[],
			groups:[],
			activeUser:{
				id:0,
        		first_name:'',
        		last_name:'',
        		email:'',
        		role:'',
        		avatarPath:'',
        		groups:[]
			},
			editUser:{
				id:0,
        		first_name:'',
        		last_name:'',
        		email:'',
        		role_id:'',
        		groups:[],
			},
			createUser:{
        		first_name:'',
        		last_name:'',
        		password:'',
        		password_confirmation:'',
        		email:'',
        		role_id:''
			},
			userSearch:"",
			createUserValidationMessage:"",
			editUserValidationMessage:"",
			modifyGroupsValidationMessage:"",
		},
		computed: {

		},
		watch: {
			userSearch: function (val) {
				this.filterUsers();
			},
		},
		methods: {
	        loadRoles() {
	            let app = this;
	            axios.get('/api/roles')
	            .then(function (response) {
	                app.roles = response.data;
	            }).catch(function (error) { showSwalError("Error loading roles",error.response.request.response) });
	        },
	        loadGroups() {
	            let app = this;
	            axios.get('/api/groups/solo')
	            .then(function (response) {
	                app.groups = response.data
	            }).catch(function (error) { showSwalError("Error loading groups",error.response.request.response) });
	        },
	        loadUsers() {
	            let app = this;
	            axios.get('/api/users')
	            .then(function (response) { 
	                app.users = response.data;
	                app.filterUsers();
	                if(app.activeUser.id != 0)
	                {
	                	for(var i=0; i<app.users.length; i++)
	                	{
	                		if(app.users[i].id == app.activeUser.id)
	                		{
	                			app.viewUser(app.users[i]);
	                		}
	                	}
	                }
	            }).catch(function (error) { showSwalError("Error loading users",error.response.request.response) });
	        },
	        viewUser(user) {
	            this.activeUser = user;
				this.resetEditUser();
	        },
	        submitCreateUser(){
				if(this.createUserFormIsValid() == true)
				{
		        	let app = this;
		            axios.post('/api/users', {
		        		first_name:app.createUser.first_name,
		        		last_name:app.createUser.last_name,
		        		password:app.createUser.password,
		        		password_confirmation:app.createUser.password_confirmation,
		        		email:app.createUser.email,
		        		role_id:app.createUser.role_id
		            })
		            .then(function (response) {
		                app.resetCreateUser();
						app.loadUsers();
						$('#createUserModal').modal('hide');
						$('.modal-backdrop').remove();
		                showSwalSuccess("Success","The user has been created");
		            }).catch(function (error) { showSwalError("Error creating user",error.response.request.response) });
	        	}
	        },
	        resetCreateUser(){
	        	this.createUser = {
	        		first_name:'',
	        		last_name:'',
	        		password:'',
	        		password_confirmation:'',
	        		email:'',
	        		role_id:''
	        	}
	        	this.createUserValidationMessage = '';
	        },
	        submitEditUser(){
	        	if(this.editUserFormIsValid() == true)
				{
		        	let app = this;
		            axios.put('/api/users/'+app.editUser.id, {
		            	id:app.editUser.id,
		        		first_name:app.editUser.first_name,
		        		last_name:app.editUser.last_name,
		        		email:app.editUser.email,
		        		role_id:app.editUser.role_id,
	            		groups:app.editUser.groups,
		            })
		            .then(function (response) {
		                app.resetEditUser();
						app.loadUsers();
						$('#updateUserModal').modal('hide');
						$('.modal-backdrop').remove();
		                showSwalSuccess("Success","The user has been updated");
		            }).catch(function (error) { 
		            	showSwalError("Error updating user",error.response.request.response) });
	        	}
	        },
	        resetEditUser(){
	        	this.editUser = {
	        		id: this.activeUser.id,
	        		first_name:this.activeUser.first_name,
	        		last_name:this.activeUser.last_name,
	        		email:this.activeUser.email,
	        		role_id:this.activeUser.role_id,
	        		groups:this.activeUser.groups,
	        	}
	        	this.editUserValidationMessage = '';
	        },
	        submitDeleteUser(){
	        	let app = this;
	            axios.delete('/api/users/'+app.activeUser.id, {
	            	id:app.activeUser.id,
	            })
	            .then(function (response) {
	            	app.activeUser.id = 0;
					app.loadUsers();
					$('#deleteUserModal').modal('hide');
					$('.modal-backdrop').remove();
	                showSwalSuccess("Success","The user has been deleted");
	            }).catch(function (error) { showSwalError("Error deleting user",error.response.request.response) });
	        },
	        filterUsers(){
				this.filteredUsers = [];
				for(var i=0;i<this.users.length; i++)
				{
					if(this.users[i].first_name.toUpperCase().indexOf(this.userSearch.toUpperCase()) > -1 || 
						this.users[i].last_name.toUpperCase().indexOf(this.userSearch.toUpperCase()) > -1 || 
						(this.users[i].role != null && this.users[i].role.name.toUpperCase().indexOf(this.userSearch.toUpperCase()) > -1) || 
						this.users[i].email.toUpperCase().indexOf(this.userSearch.toUpperCase()) > -1 )
					{
						this.filteredUsers.push(this.users[i]);
					}
				}
	        },
	        submitModifyGroups(){
				let app = this;
		        axios.put('/api/users/'+app.editUser.id, {
	            	id:app.editUser.id,
	        		first_name:app.editUser.first_name,
	        		last_name:app.editUser.last_name,
	        		email:app.editUser.email,
	        		role_id:app.editUser.role_id,
	            	groups:app.editUser.groups,
	            })
	            .then(function (response) {
	                app.resetEditUser();
					app.loadUsers();
					$('#modifyGroupsModal').modal('hide');
					$('.modal-backdrop').remove();
	                showSwalSuccess("Success","The user has been updated");
	            }).catch(function (error) { 
	            	showSwalError("Error updating user",error.response.request.response) });
	        },
	        createUserFormIsValid(){
	        	if(this.createUser.first_name == "")
	        	{
	        		this.createUserValidationMessage = "Please enter a first name";
	        		return false;
	        	}

	        	if(this.createUser.last_name == "")
	        	{
	        		this.createUserValidationMessage = "Please enter a last name";
	        		return false;
	        	}

	        	if(this.createUser.password == "")
	        	{
	        		this.createUserValidationMessage = "Please enter a password";
	        		return false;
	        	}

	        	if(this.createUser.password != this.createUser.password_confirmation)
	        	{
	        		this.createUserValidationMessage = "Passwords must match";
	        		return false;
	        	}

	        	if(this.createUser.email == "")
	        	{
	        		this.createUserValidationMessage = "Please enter an email address";
	        		return false;
	        	}

	        	if(this.editUser.role_id == null || this.editUser.role_id == "")
	        	{
	        		this.createUserValidationMessage = "Please select a role";
	        		return false;
	        	}

	        	this.createUserValidationMessage = "";
	        	return true;
	        },
	        editUserFormIsValid(){
	        	if(this.editUser.first_name == "")
	        	{
	        		this.editUserValidationMessage = "Please enter a first name";
	        		return false;
	        	}

	        	if(this.editUser.last_name == "")
	        	{
	        		this.editUserValidationMessage = "Please enter a last name";
	        		return false;
	        	}

	        	if(this.editUser.email == "")
	        	{
	        		this.editUserValidationMessage = "Please enter an email address";
	        		return false;
	        	}

	        	if(this.editUser.role_id == null || this.editUser.role_id == "")
	        	{
	        		this.editUserValidationMessage = "Please select a role";
	        		return false;
	        	}

	        	this.editUserValidationMessage = "";
	        	return true;
	        },
		},
		mounted() {
		},
		created: function () {
			this.loadUsers();
			this.loadRoles();
			this.loadGroups();
		}
	});

	showSwalError = function(title,text){
		console.log(text)
        swal({ title: title, text: text, type: "error", showCancelButton: false, allowEscapeKey: false });
	}

	showSwalSuccess = function(title,text){
        console.log(text);
        swal({ title: title, text: text, type: "success", showCancelButton: false, allowEscapeKey: false });
	}


</script>
@endsection