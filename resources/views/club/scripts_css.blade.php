<link href="/app/css/bootstrap.min.css" rel="stylesheet">
<link href="/app/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="/app/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet">
<link href="/app/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

<link href="/app/css/animate.css" rel="stylesheet">
<link href="/css/bigdog.css" rel="stylesheet">


<!--<link href="/app/css/style.css" rel="stylesheet">-->


<link href="{{ mix('/app/css/app.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ mix('/front/css/custom.css') }}">