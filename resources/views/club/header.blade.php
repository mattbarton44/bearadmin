<div class="row border-bottom">
    <nav class="navbar navbar-fixed-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li>
                <form class="form-horizontal" role="form" method="POST" action="{{ route('logout') }}" style="height: 48px; line-height: 48px;">
                   {{ csrf_field() }}
                   <button type="submit" class="btn btn-primary"><i class="fa fa-sign-out"></i> Log out</button>
                </form>

            </li>
        </ul>

    </nav>
</div>