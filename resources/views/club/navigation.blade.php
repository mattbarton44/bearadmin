<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">

            <li class="nav-header" style="padding:4px;">
                <div class="dropdown profile-element">
                    <span><img alt="image" class="" src="/app/img/hockey/logo@2x.png" /></span>
                </div>
                <div class="logo-element" style="padding:0px;">
                    <span><img alt="image" class="" src="/app/img/hockey/logo@2x.png" width="50px" /></span>
                </div>
            </li>
            <li class="{{ Bears::isActiveRoute('club') }}"><a href="/club"><i class="fa fa-paw"></i> <span class="nav-label">Dashboard</span></a></li>
       
        @if(Auth::user()->has('events'))
            <li class="{{ Bears::isActiveRoute('club/events') }}"><a href="/club/events"><i class="fa fa-calendar"></i> <span class="nav-label">Calendar</span></a></li>
        @endif

        @if(Auth::user()->has('articles'))
            <li class="{{ Bears::isActiveRoute('club/news') }}"><a href="/club/news"><i class="fa fa-newspaper-o"></i> <span class="nav-label">News</span></a></li>
        @endif

        @if(Auth::user()->has('member'))
            <li class="{{ Bears::isActiveRoute('club/shop') }}"><a href="/club/shop"><i class="fa fa-ticket"></i> <span class="nav-label">Shop</span></a></li>
        @endif

        @if(Auth::user()->has('games'))
            <li class="{{ Bears::isActiveRoute('club/games') }}"><a href="/club/games"><i class="fa fa-cog"></i> <span class="nav-label">Games</span></a></li>
        @endif

        @if(Auth::user()->has('trainings'))
            <li class="{{ Bears::isActiveRoute('club/trainings') }}"><a href="/club/trainings"><i class="fa fa-cog"></i> <span class="nav-label">Trainings</span></a></li>
        @endif

        @if(Auth::user()->has('teams'))
            <li class="{{ Bears::isActiveRoute('club/teams') }}"><a href="/club/teams"><i class="fa fa-cog"></i> <span class="nav-label">Teams</span></a></li>
        @endif

        @if(Auth::user()->has('shop_admin'))
            <li class="{{ Bears::isActiveRoute('club/products') }}"><a href="/club/products"><i class="fa fa-barcode"></i> <span class="nav-label">Products</span></a></li>
        @endif

        @if(Auth::user()->has('shop_admin'))
            <li class="{{ Bears::isActiveRoute('club/orders') }}"><a href="/club/orders"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Orders</span></a></li>
        @endif

        @if(Auth::user()->has('registrations'))
            <li class="{{ Bears::isActiveRoute('club/registrations') }}"><a href="/club/registrations"><i class="fa fa-address-book-o"></i> <span class="nav-label">Registrations</span></a></li>
        @endif

        @if(Auth::user()->has('users'))
            <li class="{{ Bears::isActiveRoute('club/users') }}"><a href="/club/users"><i class="fa fa-user"></i> <span class="nav-label">Users</span></a></li>
        @endif

        @if(Auth::user()->has('groups'))
            <li class="{{ Bears::isActiveRoute('club/groups') }}"><a href="/club/groups"><i class="fa fa-group"></i> <span class="nav-label">Groups</span></a></li>
        @endif

        @if(Auth::user()->has('roles'))
            <li class="{{ Bears::isActiveRoute('club/roles') }}"><a href="/club/roles"><i class="fa fa-lock"></i> <span class="nav-label">Roles</span></a></li>
        @endif

        @if(Auth::user()->has('settings'))
            <li class="{{ Bears::isActiveRoute('club/settings') }}"><a href="/club/settings"><i class="fa fa-sliders"></i> <span class="nav-label">Settings</span></a></li>
        @endif

        @if(Auth::user()->has('configuration'))
            <li class="{{ Bears::isActiveRoute('club/configuration') }}"><a href="/club/configuration"><i class="fa fa-cog"></i> <span class="nav-label">Configuration</span></a></li>
        @endif
        </ul>

    </div>
</nav>