<!DOCTYPE html>
<html lang="zxx">
<head>
	<title>Sheffield Bears UIHC</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="We are the league">
	<meta name="author" content="Big dog">
	<meta name="keywords" content="Sheffield Bears Ice Hockey University BUIHA Varsity">
	
    @include('club.scripts_css')

</head>
<body class="" style="background-color: #323150;">
   	<div class="site-wrapper clearfix">
	   	<div class="col-md-12">
			<div class="" style="text-align: center; margin-top:20px; margin-bottom:20px;">
				<img src="/front/images/bears_logo_front.png" alt="Bears" style="max-width: 100%;">
			</div>
		</div>

		<div class="site-content ">
			<div class="row" id="contentDiv">
				<div class="col-xs-12" v-cloak>

			        <div class="flex-container" v-if="activeProduct == null" >
						<div class="flex-item" v-for="product in products">
							<a @click="setActiveProduct(product)">
							    <div class="ibox">
							        <div class="ibox-content product-box">

							            <div class="">
							                <img style="width:280px;" :src="'/images/shop/' + product.category + '.png'">
							            </div>
							            <div class="product-desc">
							                <span class="ba-price">
							                    £@{{ (product.price/100).toFixed(2) }}
							                </span>
							                <small class="text-muted">@{{ product.category }}</small>
							                <a href="#" class="product-name"> @{{ product.name }}</a>
							                <small class="text-danger">@{{ product.tickets_left }} Tickets Left</small>
							                <button class="btn btn-primary btn-block m-t" @click="setActiveProduct(product)">Purchase</button>
							            </div>
							        </div>
							    </div>
							</a>
						</div>
			        </div>


					<div class="col-sm-6 col-sm-offset-3" v-if="activeProduct != null">
						<button class="btn btn-default btn-block m-t m-b" @click="activeProduct = null">Back to event selection</button>
			        </div>

			        <div class="ibox product-detail" v-if="activeProduct != null" style="margin-bottom:0px;">
			            <div class="ibox-content">
			                <div class="row">
			                    <div class="col-md-4">
			                        <div class="product-images">
			                            <div>
			                                <div class="">
					                        	<img style="width:240px;" :src="'/images/shop/' + activeProduct.category + '.png'">
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                    <div class="col-md-8">
			                        <h2 class="font-bold m-b-xs">
			                            @{{ activeProduct.name }}
			                        </h2>
			                        <div class="m-t-md">
			                            <h2 class="product-main-price">£@{{(activeProduct.price / 100).toFixed(2)}}</h2>
			                        </div>
							        <small class="text-danger">@{{ activeProduct.tickets_left }} Tickets Left</small>
			                        <hr>
			                        <h4>Product description</h4>
			                        <div class="small text-muted" v-html="activeProduct.description"></div>
			                    </div>
			                </div>
			                <hr>
			                <form id="myForm" name="myForm">
			                <div class="row" v-if="activeProduct.form.length > 0">
			                    <div class="col-xs-12">
			                    	<div class="row" v-for="item in activeProduct.form">
										<div class="col-xs-12">
											<div class="form-group">
						                    	<label>@{{item.label}}</label>
						                    	<template v-if="item.type == 'input'">
						                    		<input type="text" :placeholder="item.placeholder" class="form-control" v-model="item.value" :name="item.label">
						                    	</template>
						                    	<template v-if="item.type == 'select'">
													<select v-model="item.value" class="form-control" :name="item.label">
														<option v-for="option in item.options" :value="option">@{{option}}</option>
													</select>
						                    	</template>
						                    	<template v-if="item.type == 'image'">
											        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
											            <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
											            <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" @change="imageChange(item, $event)" :name="item.label"></span>
											            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput" @click="imageRemove(item)">Remove</a>
											        </div>
						                    	</template>
						                    	<template v-if="item.type == 'label'">
													<p class="form-control-static">@{{ item.placeholder }}</p>
						                    	</template>
						                    	<span v-if="item.help_text != ''" class="help-block m-b-none">@{{ item.help_text }}</span>
						                    </div>
					                    </div>
					                </div>
			                    </div>
			                </div>
			                <hr>
			                <div class="row">
			                    <div class="col-xs-12">
			                    	<button @click.prevent="checkPayment" class="btn btn-primary btn-block">Confirm and Pay</button>
					            </div>
					        </div>
			                </form>
			            </div>
			        </div>
			    </div>
			</div>
		</div>

		<style>
		.flex-container {
		  padding: 0;
		  margin: auto;
		  list-style: none;
		  margin-top: 20px;
		  display: flex;
		  flex-flow: row wrap;
		  justify-content: center;
		}

		.flex-item {
		  padding: 10px;
		  width: 300px;
		  
		}

		.ba-price{
		    font-size: 14px;
		    font-weight: 600;
		    color: #ffffff;
		    background-color: #323150;
		    padding: 6px 12px;
		    position: absolute;
		    top: -32px;
		    left: 0;
		}

		</style>


		@include('club.scripts_js')

		<link href="/app/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
		<script src="/app/js/plugins/jasny/jasny-bootstrap.min.js"></script>
		<script src="https://checkout.stripe.com/checkout.js"></script>


		<script>

			const contentApp = new Vue ({
				el: '#contentDiv',
				data: {
					products:{},
					activeProduct:null,
					stripe: null,
					stripeEmail:"",
					stripeToken:""
				},
				methods: {
			        loadProduct() {
			            let app = this;
			            axios.get("/exposed_product_data/")
			            .then(function (response) {
			                app.products = response.data;
			                app.productLoaded = true;

			            }).catch(function (error) { showSwalError("Error loading product",error) });
			        },
			        imageChange(item, event) {
			            item.value = event.target.value;
			        },
			        imageRemove(item) {
			            item.value = "";
			        },
			        submitForm() {
						swal({
						  title: "Processing",
						  text: "Your purchase is being processed",
						  type: "info",
						  showCancelButton: false,
						  showConfirmButton: false,
						  closeOnConfirm: false,
						  showLoaderOnConfirm: true,
						});

						let myForm = document.getElementById('myForm');
						let formData = new FormData(myForm);
						formData.append('stripeToken', this.stripeToken);
						formData.append('stripeEmail', this.stripeEmail);
						formData.append('product_id', this.activeProduct.id);

			        	axios.post('/exposed_product', formData, { headers: { 'Content-Type': 'multipart/form-data' }
			        	}).then(function (response) 
			        	{
							if(response.data.error_message != null)
			                {
			                	showSwalError("Error", response.data.error_message)
			                }
			                else
			                {
			                	swal({
									title: "Success",
									text: "Your purchase was successful!",
									type: "success",
									showCancelButton: false,
									closeOnConfirm: false,
								},
								function(isConfirm){
									if (isConfirm) 
									{
					                	window.location.href = window.location.origin;
									}
								});
			                }

			            }).catch(function (error) { showSwalError("Error",error) });


			        },
			        setActiveProduct(product){
			        	this.activeProduct = product;
			        	this.activeProduct.form = JSON.parse(product.form);
			        },
			        checkPayment() {

						this.stripe.open({
							name: this.activeProduct.name,
							description: this.activeProduct.description,
							zipCode: false,
							currency: 'gbp',
							amount: this.activeProduct.price
						});

			        },
			        configureStripe() {
			        	this.stripe = StripeCheckout.configure({
							key: BearAdmin.stripeKey,
							image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
							locale: 'auto',
							token: (token) => {
								this.stripeToken = token.id;
								this.stripeEmail = token.email;
								this.submitForm();
							},
							allowRememberMe: false,
						});
			        }
				},
				created: function () {
					this.loadProduct();
					this.configureStripe();
				},
			});

			showSwalError = function(title,text){
				console.log(text)
		        swal({ title: title, text: text, type: "error", showCancelButton: false, allowEscapeKey: false });
			}

			showSwalSuccess = function(title,text){
		        console.log(text);
		        swal({ title: title, text: text, type: "success", showCancelButton: false, allowEscapeKey: false });
			}

		</script>
	</div>
</body>
</html>