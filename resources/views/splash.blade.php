<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sheffield Bears UIHC</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="Official website of the Sheffield Bears University Ice Hockey Team">
	<meta name="author" content="Matt Barton">
	<meta name="keywords" content="Sheffield Bears Ice Hockey University BUIHA Varsity Hallam UoS SHU Sports">

    @include('club.scripts_css')

</head>
<body id="page-top" class="landing-page" style="background-color:#0e0e0e; color:#fff;">
	<section  class="container features">
	    <div class="row">
	        <div class="col-lg-12 text-center">
	            <div class="navy-line"></div>
	            <h1>SHEFFIELD BEARS<br/> <span class="navy"> UNIVERSITY ICE HOCKEY CLUB</span> </h1>
	        </div>
	    </div>
	    <div class="row">
	        <div class="col-md-3 text-center wow fadeInLeft">
	            <div>
	            </div>
	            <div class="m-t-lg">
	            </div>
	        </div>
	        <div class="col-md-6 text-center  wow zoomIn">
	            <img src="{{ URL::asset('/front/images/bears_logo_front.png') }}" alt="dashboard" class="img-responsive" padding="auto">
	        </div>
	        <div class="col-md-3 text-center wow fadeInRight">
	            <div>
	            </div>
	            <div class="m-t-lg">
	            </div>
	        </div>
	    </div>
	    <div class="row">
	        <div class="col-lg-12 text-center">
	        </div>
	    </div>
	    <div class="row">
	        <div class="col-lg-12 text-center">
	        </div>
	    </div>
	</section>

	<section class="features">
	    <div class="container">
	        <div class="row m-b-lg">
	            <div class="col-lg-12 text-center">
	                <div class="navy-line"></div>
	                <h1>Contact Us</h1>
	                <p>Any enquires can be made by contacting the Sheffield Bears committee via email</p>
	            </div>
	        </div>
	        
	        <div class="row">
	            <div class="col-lg-12 text-center">
	                <a href="mailto:info@sheffieldbears.com" class="btn btn-success m-b">Send us mail</a>
	                <p class="m-t-sm">
	                    Or follow us on social platform
	                </p>
	                <ul class="list-inline social-icon">
	                    <li><a href="http://www.twitter.com/sheffieldbears"><i class="fa fa-twitter" style="font-size:x-large;"></i></a>
	                    </li>
	                    <li><a href="http://www.facebook.com/sheffieldbears"><i class="fa fa-facebook"  style="font-size:x-large;"></i></a>
	                    </li>
	                </ul>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-lg-8 col-lg-offset-2 text-center m-t-lg m-b-lg">
	                <p><strong>&copy; 2017 Sheffield Bears</strong><br/></p>
	            </div>
	        </div>
	    </div>

	</section>

	@include('club.scripts_js')

</body>
</html>