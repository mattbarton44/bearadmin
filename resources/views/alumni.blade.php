@extends('front.layout')

@section('content')
<div class="container" id="contentDiv">

	<div class="row">

		<div class="col-lg-8" v-if="showOrderForm == true">
			<article class="card post post--single">
				<header class="card__header card__header--has-btn">
					<h4>@{{ product.name }}</h4>
					<a class="btn btn-default btn-xs card-header__button" @click="showOrderForm = false">Close Form</a>
				</header>
				<div class="card__content">
					<div class="post__content">
						<div class="ibox">
					        <div class="ibox product-detail" style="margin-bottom:0px;">
					            <div class="ibox-content">
					                <div class="row">
					                    <div class="col-md-12">
					                        <div class="m-t-md">
					                            <h2 class="product-main-price">£@{{(product.price / 100).toFixed(2)}}</h2>
					                        </div>
									        <small class="text-danger">@{{ 80 - product.orders_count }} Tickets Left</small>
					                    </div>
					                </div>
					                <hr>
					                <form id="myForm" name="myForm">
					                <div class="row" v-if="product.form.length > 0">
					                    <div class="col-xs-12">
					                    	<div class="row" v-for="item in product.form">
												<div class="col-xs-12">
													<div class="form-group">
								                    	<label>@{{item.label}}</label>
								                    	<template v-if="item.type == 'input'">
								                    		<input type="text" :placeholder="item.placeholder" class="form-control" v-model="item.value" :name="item.label">
								                    	</template>
								                    	<template v-if="item.type == 'select'">
															<select v-model="item.value" class="form-control" :name="item.label">
																<option v-for="option in item.options" :value="option">@{{option}}</option>
															</select>
								                    	</template>
								                    	<template v-if="item.type == 'image'">
													        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
													            <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
													            <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" @change="imageChange(item, $event)" :name="item.label"></span>
													            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput" @click="imageRemove(item)">Remove</a>
													        </div>
								                    	</template>
								                    	<template v-if="item.type == 'label'">
															<p class="form-control-static">@{{ item.placeholder }}</p>
								                    	</template>
								                    	<span v-if="item.help_text != ''" class="help-block m-b-none">@{{ item.help_text }}</span>
								                    </div>
							                    </div>
							                </div>
					                    </div>
					                </div>
					                <hr>
					                <div class="row">
					                    <div class="col-xs-12">
					                    	<button @click.prevent="processPayment" class="btn btn-info btn-block" :disabled="formIsValid == false">Confirm and Pay</button>
							            </div>
							        </div>
					                </form>
					            </div>
					        </div>
						</div>

					</div>
				</div>
			</article>
		</div>

		<div class="col-lg-8" v-else>
			<article class="card post post--single">
				<div class="card__content">
					<header class="post__header">
						<h2 class="post__title">Alumni Tournament 2019!</h2>
					</header> 
					<div class="post__content">
						<p>After taking a gap year in 2018, we're happy to announce the semi-annual <b>Official Sheffield Bears Universities Ice Hockey Club Alumni Tournament</b> is back for 2019!</p><p>This year the event will be taking place over the weekend of the <b>27th/28th July</b>.</p><p>The games will run from <b>10:30 - 17:30</b> on the Saturday and <b>12:00 - 15:30</b> on the Sunday.&nbsp;</p><p>To be able to play in the tournament you will need to have a full hockey kit (the club will help where possible if you're in need), and you must purchase a ticket from this website.</p><p>Tickets are <b>£35</b>, with the revenue being used to cover ice costs, referees, the inaugural <b>Sheffield Bears Superbowl</b>, and any money left over will be put towards food/drink/room hire on the Saturday for the night out.</p><p><br></p><p>Details regarding team allocation, game times, and night out plans will be announced once we have some more concrete numbers! </p><p>News updates will be added to the facebook event found here:&nbsp;<a href="https://www.facebook.com/events/465841527485076/">https://www.facebook.com/events/465841527485076/</a></p>
					</div>
				</div>
			</article>
		</div>

		<div class="col-lg-4">
			<aside class="widget widget--sidebar card widget-preview">
				<div class="widget__title card__header">
					<h4>
						Buy Ticket
					</h4>
				</div>
				<div class="widget__content card__content">
					<!-- Match Preview -->
					<div class="match-preview">
						<section class="match-preview__body">
							<div class="match-preview__match-info match-preview__match-info--header">
								<div class="match-preview__match-place">
									Ice Sheffield
								</div>
								<time class="match-preview__match-time">
									Saturday, July 27th - 10:30 AM
								</time>
							</div>
							<header class="match-preview__header match-preview__header--decor">
								<div class="match-preview__header-inner">
									<div class="match-preview__info">
										Sheffield Bears 2019
									</div>
									<h3 class="match-preview__title match-preview__title--lg">Alumni Tournament</h3>
								</div>
							</header>
						</section>
						<div class="countdown__content" style="padding: 0px">
							<div class="countdown-counter" data-date="July 27, 2019 10:30:00">
								<div class="countdown-counter__item countdown-counter__item--days">
									77 <span class="countdown-counter__label">days</span>
								</div>
								<div class="countdown-counter__item countdown-counter__item--hours">
									13 <span class="countdown-counter__label">hours</span>
								</div>
								<div class="countdown-counter__item countdown-counter__item--mins">
									34 <span class="countdown-counter__label">mins</span>
								</div>
								<div class="countdown-counter__item countdown-counter__item--secs">
									37 <span class="countdown-counter__label">secs</span>
								</div>
							</div>
						</div>
						<div class="match-preview__action match-preview__action--ticket">
							<a class="btn btn-primary btn-lg btn-block" v-if="product.orders_count >= 80" disabled>SOLD OUT</a>
							<a class="btn btn-info btn-lg btn-block" @click="buyTicket" v-else>Buy Ticket Now</a>
						</div>
					</div>
					<!-- Match Preview / End -->
				</div>
			</aside>
		</div>
	</div>





</div>
    
    <script>
		
	window.Laravel =  <?php echo json_encode([
	    'csrfToken' => csrf_token(),
	]); ?>

	BearAdmin = {
		csrfToken: "{{ csrf_token() }}",
		stripeKey: "{{ config('services.stripe.key') }}",
	}


	</script>
  
<link href="/app/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet">
	<script src="/app/plugins/sweetalert/dist/sweetalert.min.js"></script>
	<script src="https://checkout.stripe.com/checkout.js"></script>
	<script src="{{ mix('/front/js/app.js') }}"></script>
	<script src="{{ mix('/js/app.js') }}"></script>

	<script>
		
		const contentApp = new Vue ({
			el: '#contentDiv',
			data: {
				product:{
					form:[]
				},
				showOrderForm: false,
				stripe: null,
				stripeEmail:"",
				stripeToken:""
			},
			methods: {
		        loadProduct() {
		            let app = this;
		            axios.get("/alumni_product")
		            .then(function (response) {
		                app.product = response.data;
		                app.product.form = JSON.parse(response.data.form);
		                app.productLoaded = true;
		            }).catch(function (error) { showSwalError("Error loading product",error) });
		        },
		        submitForm() {
					swal({
					  title: "Processing",
					  text: "Your purchase is being processed",
					  type: "info",
					  showCancelButton: false,
					  showConfirmButton: false,
					  closeOnConfirm: false,
					  showLoaderOnConfirm: true,
					});

					let myForm = document.getElementById('myForm');
					let formData = new FormData(myForm);
					let app = this;
					formData.append('stripeToken', this.stripeToken);
					formData.append('stripeEmail', this.stripeEmail);
					formData.append('product_id', this.product.id);

		        	axios.post('/exposed_product', formData, { headers: { 'Content-Type': 'multipart/form-data' }
		        	}).then(function (response) 
		        	{
						if(response.data.error_message != null)
		                {
		                	showSwalError("Error", response.data.error_message)
		                }
		                else
		                {
		                	swal({
								title: "Success",
								text: "Your purchase was successful!",
								type: "success",
								showCancelButton: false,
								closeOnConfirm: true,
							},
							function(isConfirm){
								if (isConfirm) 
								{
									app.showOrderForm = false;
								}
							});
		                }

		            }).catch(function (error) { showSwalError("Error",error) });


		        },
		        buyTicket() {
					this.showOrderForm = true;					
		        },
		        processPayment() {


		        	if(this.formIsValid() == true)
		        	{
		        		this.checkPayment();
		        		
		        	}
		        	else
		        	{
		        		alert("Please fill out the form!");
		        	}
		        	
		        },
		        formIsValid() {
		        	let state = true;
		        	for(let i=0; i<this.product.form.length; i++)
		        	{
		        		if(this.product.form[i].value == null || this.product.form[i].value == "")
		        		{
		        			state = false;
		        		} 
		        	}
		        	return state;
		        },
		        checkPayment() {

					this.stripe.open({
						name: this.product.name,
						zipCode: false,
						currency: 'gbp',
						amount: this.product.price
					});

		        },
		        configureStripe() {
		        	this.stripe = StripeCheckout.configure({
						key: BearAdmin.stripeKey,
						image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
						locale: 'auto',
						token: (token) => {
							this.stripeToken = token.id;
							this.stripeEmail = token.email;
							this.submitForm();
						},
						allowRememberMe: false,
					});
		        }
			},
			created: function () {
				this.loadProduct();
				this.configureStripe();
			},
		});

		showSwalError = function(title,text){
			console.log(text)
	        swal({ title: title, text: text, type: "error", showCancelButton: false, allowEscapeKey: false });
		}

		showSwalSuccess = function(title,text){
	        console.log(text);
	        swal({ title: title, text: text, type: "success", showCancelButton: false, allowEscapeKey: false });
		}

	</script>
@endsection
