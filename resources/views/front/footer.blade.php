
    <!-- Footer
    ================================================== -->
    <footer id="footer" class="footer">
    
      <!-- Footer Secondary -->
      <div class="footer-secondary">
        <div class="container">
          <div class="footer-secondary__inner">
            <div class="row">
              <div class="col-md-4">
                <div class="footer-copyright"><a href="/">Sheffield Bears UIHC</a> 2017 &nbsp; | &nbsp; All Rights Reserved</div>
              </div>
              <div class="col-md-8">
                <ul class="footer-nav footer-nav--right footer-nav--sm">
                  <li class="footer-nav__item"><a href="/">Home</a></li>
                  <li class="footer-nav__item"><a href="/news">News</a></li>
                  <li class="footer-nav__item"><a href="/fixtures">Fixtures</a></li>
                  <li class="footer-nav__item"><a href="/statistics">Statistics</a></li>
                  <li class="footer-nav__item"><a href="/about">About Us</a></li>
                  <li class="footer-nav__item"><a href="http://www.buiha.org.uk" target="_blank">BUIHA</a></li>
                  <li class="footer-nav__item"><a href="/club">Club</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Footer Secondary / End -->
    </footer>
    <!-- Footer / End -->