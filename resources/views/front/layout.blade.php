<!DOCTYPE html>
<html lang="zxx">
<head>
   <title>Sheffield Bears UIHC</title>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="description" content="Sheffield Bears Ice Hockey University">
   <meta name="author" content="Big dog">
   <meta name="keywords" content="Sheffield Bears Ice Hockey University BUIHA Varsity">
   @include('front/scripts_css')
</head>
<body class="template-football">
   <div class="site-wrapper clearfix">
      <div class="site-overlay"></div>
      @include('front/header')

      <div class="site-content">
         @yield('content')
      </div>
      @include('front/footer')
      @include('front/scripts_js')
      @yield('scripts')
   </div>
</body>
</html>
