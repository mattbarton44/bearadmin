<!-- Header Mobile -->
<div class="header-mobile clearfix" id="header-mobile">
   <div class="header-mobile__logo">
      <a href=""><img src="/front/images/hockey/logo.png" srcset="/front/images/hockey/logo@2x.png 2x" alt="Bears" class="header-mobile__logo-img"></a>
   </div>
   <div class="header-mobile__inner">
      <a id="header-mobile__toggle" class="burger-menu-icon"><span class="burger-menu-icon__line"></span></a>
   </div>
</div>

<!-- Header Desktop -->
<header class="header">
   <div class="header__top-bar clearfix">
      <div class="container">
         <ul class="nav-account">
         @if(Auth::check() == true)
         <li class="nav-account__item"><a href="/club/settings">Your Account: <span class="highlight">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</span></a></li>
         <li class="nav-account__item nav-account__item--logout">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('logout') }}" style="height: 48px; line-height: 48px;">
               {{ csrf_field() }}
               <button type="submit" class="btn btn-primary btn-xs">Logout</button>
            </form>
         </li>
         @else
         <li class="nav-account__item"><a href="/club/">You are not currently logged in</span></a></li>
         @endif
         </ul>
      </div>
   </div>
   <div class="header__secondary">
      <div class="container">
         <ul class="info-block info-block--header">
            <li class="info-block__item info-block__item--contact-secondary">
               <div class="df-icon df-icon--football-helmet social-links__link" style="font-size: 32px; color: #ffdc11;">
                  <i class="fa fa-envelope"></i>
               </div>
               <h6 class="info-block__heading">Contact Us</h6>
               <a class="info-block__link" href="mailto:info@sheffieldbears.com">info@sheffieldbears.com</a>
            </li>
            <li class="info-block__item info-block__item--contact-primary">
               <div class="df-icon df-icon--football-helmet social-links__link" style="font-size: 32px; color: #ffdc11;">
                  <i class="fa fa-twitter"></i>
               </div>
               <h6 class="info-block__heading">Follow us on Twitter</h6>
               <a class="info-block__link" href="http://www.twitter.com/sheffieldbears"  target="_blank">twitter.com/sheffieldbears</a>
            </li>
            <li class="info-block__item info-block__item--contact-primary">
               <div class="df-icon df-icon--football-helmet social-links__link" style="font-size: 32px; color: #ffdc11;">
                  <i class="fa fa-facebook"></i>
               </div>
               <h6 class="info-block__heading">Like us on Facebook</h6>
               <a class="info-block__link" href="http://www.facebook.com/sheffieldbears"  target="_blank">facebook.com/sheffieldbears</a>
            </li>
         </ul>
      </div>
   </div>
   <div class="header__primary">
      <div class="container">
         <div class="header__primary-inner">
            <div class="header-logo">
               <a href="/"><img src="/front/images/hockey/logo.png" srcset="/front/images/hockey/logo@2x.png 2x" alt="Bears" class="header-logo__img"></a>
            </div>
            <nav class="main-nav clearfix">
               <ul class="main-nav__list">
                  <li class=""><a href="/">Home</a></li>


                  <li class=""><a href="/news">News</a></li>
               
                  <li class=""><a href="/fixtures">Fixtures</a></li>
                  <li class=""><a href="/statistics">Statistics</a></li>

                  <li class=""><a href="http://www.buiha.org.uk" target="_blank">BUIHA</a></li>
                  <li class=""><a href="/club">Club</a></li>
               </ul>
            </nav>
         </div>
      </div>
   </div>
</header>


<style>

@media (min-width: 992px) {
   .main-nav {
      position: relative;
      text-align: center;
   }
}

</style>