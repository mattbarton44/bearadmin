
<!-- Favicons
================================================== -->
<link rel="shortcut icon" href="/front/images/football/favicons/favicon.ico">
<link rel="apple-touch-icon" sizes="120x120" href="/front/images/football/favicons/favicon-120.png">
<link rel="apple-touch-icon" sizes="152x152" href="/front/images/football/favicons/favicon-152.png">

<!-- Mobile Specific Metas
================================================== -->
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0">

<!-- Google Web Fonts
================================================== -->
<link href="https://fonts.googleapis.com/css?family=Exo+2:400,700,700i|Roboto:400,400i" rel="stylesheet">

<!-- CSS
================================================== -->
<!-- Preloader CSS -->
<link rel="stylesheet" href="{{ mix('/front/css/preloader.css') }}">

<!-- Vendor CSS -->
<link href="/front/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="/front/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="/front/fonts/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
<link href="/front/vendor/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
<link href="/front/vendor/slick/slick.css" rel="stylesheet">

<!-- Template CSS-->
<link rel="stylesheet" href="{{ mix('/front/css/style.css') }}">

<!-- Custom CSS-->
<link rel="stylesheet" href="{{ mix('/front/css/custom.css') }}">
<link href="/css/bigdog.css" rel="stylesheet">