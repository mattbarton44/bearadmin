<script type="text/javascript">
$( document ).ready(function()
{
    @if (Session::has('error_notification'))
        swal({
          title: "{{Session::get('error_notification')}}",
          type: "error",
          allowEscapeKey: false
        });
    @endif

    @if (Session::has('errors'))
        swal({
          title: "{{Session::get('errors')->first()}}",
          type: "error",
          allowEscapeKey: false
        });
    @endif

    @if (Session::has('success_notification'))
    alert("YAY");
        swal({
          title: "{{Session::get('success_notification')}}",
          type: "success",
          allowEscapeKey: false
        });
    @endif

    @if (Session::has('info_notification'))
        swal({
          title: "{{Session::get('info_notification')}}",
          type: "info",
          allowEscapeKey: false
        });
    @endif
});
</script>