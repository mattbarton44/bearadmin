<!DOCTYPE html>
<html lang="zxx">
<head>
   <title>Sheffield Bears UIHC</title>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="description" content="Sheffield Bears Ice Hockey University">
   <meta name="author" content="Big dog">
   <meta name="keywords" content="Sheffield Bears Ice Hockey University BUIHA Varsity">
   @include('front/scripts_css')
</head>

<body class="template-football">

	<div class="site-wrapper clearfix">
		<div class="site-overlay"></div>

		
		<!-- Content
		================================================== -->
		<div class="site-content">
			<div class="container">
		
				<!-- Error 404 -->
				<div class="error-404">
					<div class="row">
						<div class="col-md-12">
							<figure class="error-404__figure">
								<img src="/images/icon-ghost.svg" alt="">
							</figure>
							<header class="error__header">
								<h2 class="error__title">OOOOPS! Unsupported Browser</h2>
								<h3 class="error__subtitle">Seems that we have a problem!</h3>
							</header>
							<div class="error__description">
								You are using a browser that this website does not support. Try switching to something half decent like Chrome.
							</div>
						</div>
					</div>
				</div>
				<!-- Error 404 / End -->
		
			</div>
		</div>
		
		<!-- Content / End -->
		

	</div>


  <!-- Javascript Files
  ================================================== -->
  <!-- Core JS -->
  <script src="/front/vendor/jquery/jquery.min.js"></script>
  <script src="/front/js/core-min.js"></script>
  
  <!-- Vendor JS -->
  <script src="/front/vendor/twitter/jquery.twitter.js"></script>
  <script src="/front/vendor/jquery-duotone/jquery.duotone.min.js"></script>
  
  <script src="/front/vendor/marquee/jquery.marquee.min.js"></script>
  
  <!-- Template JS -->
  <script src="/front/js/init.js"></script>
  <script src="/front/js/custom.js"></script>
  

</body>
</html>