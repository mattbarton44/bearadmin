@extends('front.layout')

@section('content')
<div class="container">
	<div class="row" id="contentDiv" v-cloak>
		<div class="content col-md-12">

			<div class="card card--clean mb-0">
				<div class="card__content">
					<div class="posts posts--cards post-grid post-grid--2cols post-grid--fitRows row">
						<template v-for="datum in data">
							<div class="post-grid__item col-xs-6">
								<a :href="'news/'+datum.id" class="posts__link-wrapper">
									<div class="posts__item posts__item--card posts__item--category-1 card">
										<div class="posts__inner card__content">
											<div class="posts__cat posts__cat--flow">
												<span :class="'label posts__cat-label label-' + datum.type.colour">@{{datum.type.name}}</span>
											</div>
											<h6 class="posts__title">@{{datum.subject}}</h6>
										</div>
										<footer class="posts__footer card__footer">
											<div class="post-author">
												<figure class="post-author__avatar">
													<img :src="datum.user.avatarPath" alt="">
												</figure>
												<div class="post-author__info">
													<h4 class="post-author__name">@{{datum.user.first_name}} @{{datum.user.last_name}}</h4>
												</div>
											</div>
											<div class="post__meta meta">
												<h4 class="post-author__name">@{{ moment(datum.updated_at, "YYYY-MM-DD HH:mm:ss").calendar() }}</h4>
											</div>
										</footer>
									</div>
								</a>
							</div>
						</template>
					</div>
				</div>
			</div> 

		</div>

	</div>

</div>


@endsection


@section('scripts')

<script>

const contentApp = new Vue ({
	el: '#contentDiv',
	data: {
		data:[]
	},
	methods: {
		loadNews() {
            let app = this;
            axios.get('/dashboard/all_news')
            .then(function (response) { 
            	app.data = response.data; 
            }).catch(function (error) { console.log(error) });
        },
	},
	created: function () {
		this.loadNews();
	},
});

</script>
@endsection
