@extends('front.layout')

@section('content')
<div class="container">

<div class="row" id="contentDiv" v-cloak>

	<div class="content col-md-8">

		<featurednews :data="featured_news" v-if="featured_news.id != null"></featurednews>
		<latestnews :data="latest_news" v-if="latest_news.length > 0"></latestnews>
	</div>

	<div id="sidebar" class="sidebar col-md-4">
    
    	<template v-for="(league, key) in cup_table_data">
			<leaguetable :data="league" :title="key"></leaguetable>
		</template>

	</div>
</div>

</div>
    
  
@endsection



@section('scripts')

<script>

	const contentApp = new Vue ({
		el: '#contentDiv',
		data: {
			cup_table_data:{},
			featured_news:{
				id:null
			},
			latest_news:[]
		},
		methods: {
			loadCupTableData() {
	            let app = this;
	            axios.get('/dashboard/cup_tables_data')
	            .then(function (response) { 
	            	app.cup_table_data = response.data; 
	            }).catch(function (error) { console.log(error) });
	        },
			loadFeaturedNews() {
	            let app = this;
	            axios.get('/dashboard/featured_news')
	            .then(function (response) { 
	            	app.featured_news = response.data; 
	            }).catch(function (error) { console.log(error) });
	        },
			loadLatestNews() {
	            let app = this;
	            axios.get('/dashboard/latest_news')
	            .then(function (response) { 
	            	app.latest_news = response.data; 
	            }).catch(function (error) { console.log(error) });
	        },
		},
		created: function () {
			this.loadCupTableData();
			this.loadFeaturedNews();
			this.loadLatestNews();
		},
	});

</script>
@endsection
