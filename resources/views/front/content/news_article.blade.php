@extends('front.layout')

@section('content')
<div class="container">
	<div class="row"  id="contentDiv" v-cloak v-if="data.id != null">
		<div class="content col-md-12">
			<article class="card post post--single">
				<div class="card__content">
					<div class="post__category">
	                    <span :class="'label posts__cat-label label-' + data.type.colour">@{{data.type.name}}</span>
					</div>
					<header class="post__header">
						<h2 class="post__title">@{{data.subject}}</h2>
						<ul class="post__meta meta">
							<li class="meta__item meta__item--date"><time class="posts__date">@{{ moment(data.updated_at, "YYYY-MM-DD HH:mm:ss").calendar() }}</time></li>
							<li class="meta__item meta__item--date">@{{ data.user.first_name }} @{{data.user.last_name}}</li>
						</ul>
					</header>
					<div class="post__content" v-html="data.body"></div>
				</div>
			</article>
			<div class="post-sharing">
				<a disabled href="#" class="btn btn-default btn-facebook btn-icon btn-block"><i class="fa fa-facebook"></i> <span class="post-sharing__label hidden-xs">Share on Facebook</span></a>
				<a disabled href="#" class="btn btn-default btn-twitter btn-icon btn-block"><i class="fa fa-twitter"></i> <span class="post-sharing__label hidden-xs">Share on Twitter</span></a>
				<a disabled href="#" class="btn btn-default btn-gplus btn-icon btn-block"><i class="fa fa-google-plus"></i> <span class="post-sharing__label hidden-xs">Share on Google+</span></a>
			</div>
		</div>
	</div>
</div>
@endsection



@section('scripts')

<script>
	const contentApp = new Vue ({
		el: '#contentDiv',
		data: {
			data:{},
		},
		methods: {
			loadData() {
	            let app = this;
	            let pathname = window.location.pathname.split("/");
	            let path = "/dashboard/news_article/" + pathname[pathname.length-1]
	            axios.get(path)
	            .then(function (response) { 
	            	app.data = response.data; 
	            }).catch(function (error) { console.log(error) });
	        },
		},
		created: function () {
			this.loadData();
		},
	});

</script>
@endsection
