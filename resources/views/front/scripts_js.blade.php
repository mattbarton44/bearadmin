
<script>
window.Laravel =  <?php echo json_encode([
    'csrfToken' => csrf_token(),
]); ?>

function check() 
{
    "use strict";

    try { eval("var foo = (x)=>x+1"); }
    catch (e) { return false; }
    return true;
}

if (check() == false) 
{
    window.location.href = window.location.origin + "/unsupported";
}


</script>

<script src="{{ mix('/front/js/app.js') }}"></script>

  <!-- Javascript Files
  ================================================== -->
  <!-- Core JS -->
  <script src="/front/vendor/jquery/jquery.min.js"></script>
  <script src="/front/js/core-min.js"></script>
  
  <!-- Vendor JS -->
  <script src="/front/vendor/twitter/jquery.twitter.js"></script>
  <script src="/front/vendor/jquery-duotone/jquery.duotone.min.js"></script>
  
  <script src="/front/vendor/marquee/jquery.marquee.min.js"></script>
  
  <!-- Template JS -->
  <script src="/front/js/init.js"></script>
  <script src="/front/js/custom.js"></script>
  
<script src="{{ mix('/js/app.js') }}"></script>