@extends('front.layout')

@section('content')
<div class="container">
    <div class="row">
        <div class="content col-md-12">
            <div class="card">
                <div class="card__header">
                    <h4>Reset Password</h4>
                </div>
                <div class="card__content">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="login-name">Your Email</label>
                                <input type="email" name="email" id="login-name" class="form-control" placeholder="Enter your email address..."  value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group form-group--sm">
                                <button type="submit" class="btn btn-primary btn-lg btn-block">Send Password Reset Link</button>
                            </div>
                        </div>

                        <div class="row">
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
