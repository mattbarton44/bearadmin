@extends('front.layout')

@section('content')

<div class="container">
<div class="row">
<div class="content col-md-12">
    <div class="card">
        <div class="card__header">
            <h4>Register Now</h4>
        </div>
        <div class="card__content">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                    <label for="register-firstname">First Name</label>
                    <input id="register-firstname" type="text" placeholder="Enter your first name..."  name="first_name" class="form-control" value="{{ old('first_name') }}" required autofocus>
                    @if ($errors->has('first_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                    <label for="register-lastname">Last Name</label>
                    <input id="register-lastname" type="text" placeholder="Enter your last name..."  name="last_name" class="form-control" value="{{ old('last_name') }}" required>
                    @if ($errors->has('last_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="register-name">Your Email</label>
                    <input id="register-name" class="form-control" placeholder="Enter your email address..."  name="email"  type="email"  value="{{ old('email') }}" required>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="register-password">Your Password</label>
                    <input type="password" name="password" id="register-password" class="form-control" placeholder="Enter your password..." required>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="repeat-password">Repeat Password</label>
                    <input type="password" name="password_confirmation" id="repeat-password" class="form-control" placeholder="Repeat your password..." required>
                </div>
                <div class="form-group form-group--submit">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">Create Your Account</abutton>
                </div>
            </form>
        </div>

    </div>
</div>
</div>
</div>


@endsection