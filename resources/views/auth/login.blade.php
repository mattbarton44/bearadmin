@extends('front.layout')

@section('content')

<div class="container">
<div class="row">
<div class="content col-md-12">
    <div class="card">
        <div class="card__header">
            <h4>Login to your Account</h4>
        </div>
        <div class="card__content">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="login-name">Your Email</label>
                        <input type="email" name="email" id="login-name" class="form-control" placeholder="Enter your email address..."  value="{{ old('email') }}" required autofocus>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="login-password">Your Password</label>
                        <input type="password" name="password" id="login-password" class="form-control" placeholder="Enter your password..." required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group form-group--password-forgot">
                        <label class="checkbox checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox1" value="option1" checked> Remember Me
                            <span class="checkbox-indicator"></span>
                        </label>
                        <span class="password-reminder">Forgot your password? <a href="password/reset">Click Here</a></span>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group form-group--sm">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Sign in to your account</button>
                    </div>
                </div>
                <div class="text-center" style="margin-top:20px; margin-bottom:20px">
                   - OR -
                </div>
                <div class="col-md-12">
                    <div class="form-group form-group--sm">
                        <a class="btn btn-success btn-lg btn-block" href="/register">Create a new account</a>
                    </div>
                </div>

                <div class="row">
                </div>

            </form>
        </div>
    </div>
</div>
</div>
</div>
  
@endsection